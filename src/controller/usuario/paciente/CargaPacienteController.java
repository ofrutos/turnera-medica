package controller.usuario.paciente;

import controller.usuario.CargaUsuarioController;
import java.text.ParseException;
import model.pojo.PacientePojo;
import model.pojo.TipoUsuario;
import service.PacienteService;
import service.excpetion.ServiceException;
import view.constants.ViewLabels;
import view.message.MessageView;
import view.usuario.paciente.CargaPacienteView;

/**
 * Clase que administra el controlador de alta y modificación de pacientes.
 */
public class CargaPacienteController extends CargaUsuarioController {

  private final CargaPacienteView cargaPacienteView;
  private final PacienteService pacienteService;
  private PacientePojo pacientePojo;

  /**
   * Crea el controlador de la carga del paciente.
   *
   * @param cargaPacienteView Vista de la carga del paciente.
   * @param messageView       Vista de mensajes.
   * @param pacienteService   Servicio de Paciente.
   */
  public CargaPacienteController(final CargaPacienteView cargaPacienteView,
      final MessageView messageView,
      final PacienteService pacienteService) {
    super(cargaPacienteView,
        messageView);

    this.cargaPacienteView = cargaPacienteView;
    this.pacienteService = pacienteService;
  }

  public void initView() {
    if (this.pacientePojo != null) {
      this.cargaPacienteView.setTitle("Actualizando paciente " + this.pacientePojo.getUsuarioId());
    } else {
      this.cargaPacienteView.setTitle("Creando paciente");
    }

    super.initView();
  }

  /**
   * Actualiza la información del usuario. Si el usuario no existe en el sistema, lo genera.
   */
  @Override
  protected void update() {
    if (super.controlarCarga()) {
      try {
        String mensaje;
        if (this.pacientePojo == null) {
          this.pacienteService.crearPaciente((obtenerPacienteInView()));
          mensaje = "Se ha creado el paciente exitosamente.";
        } else {
          this.pacienteService.actualizarPaciente(obtenerPacienteInView());
          mensaje = "Se ha actualizado el paciente exitosamente.";
        }
        super.showMessage(ViewLabels.MENSAJE_EXITO.getLabel(), mensaje);
        this.cargaPacienteView.dispose();
      } catch (final ServiceException | ParseException e) {
        super.showMessage(ViewLabels.MENSAJE_ERROR.getLabel(), e.getMessage());
      }
    }
  }

  private PacientePojo obtenerPacienteInView() throws ParseException {
    final PacientePojo pacientePojoInView = new PacientePojo(super.getDataInView());
    pacientePojoInView.setTipoUsuario(TipoUsuario.PACIENTE);

    return pacientePojoInView;
  }

  /**
   * Establece el paciente a visualizar en la carga.
   *
   * @param pacientePojo POJO del paciente.
   */
  public void setPacientePojo(final PacientePojo pacientePojo) {
    super.setUsuarioPojo(pacientePojo);
    this.pacientePojo = pacientePojo;
  }

  /**
   * Devuelve la vista de la carga de paciente.
   *
   * @return Vista de la carga de paciente.
   */
  public CargaPacienteView getCargaPacienteView() {
    return this.cargaPacienteView;
  }
}
