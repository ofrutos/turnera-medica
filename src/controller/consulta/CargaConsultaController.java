package controller.consulta;

import controller.CargaController;
import java.awt.event.ItemEvent;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import model.pojo.ConsultaPojo;
import model.pojo.ConsultorioPojo;
import model.pojo.HorarioPojo;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoPojo;
import model.pojo.PacientePojo;
import model.util.DateUtils;
import service.ConsultaService;
import service.MedicoConsultorioService;
import service.MedicoService;
import service.PacienteService;
import service.excpetion.ServiceException;
import view.constants.ViewLabels;
import view.consulta.CargaConsultaView;
import view.message.MessageView;

/**
 * Clase que administra el controlador de la carga de una consulta.
 */
public class CargaConsultaController extends CargaController<ConsultaPojo> {

  private final CargaConsultaView cargaConsultaView;
  private final ConsultaService consultaService;
  private final PacienteService pacienteService;
  private final MedicoService medicoService;
  private final MedicoConsultorioService medicoConsultorioService;
  private ConsultaPojo consultaPojo;
  private List<MedicoConsultorioPojo> medicoConsultorioPojos;

  /**
   * Crea la carga de una consulta.
   *
   * @param cargaConsultaView        Vista de la carga una consulta.
   * @param messageView              Vista de mensajes.
   * @param consultaService          Servicio de Consulta.
   * @param pacienteService          Servicio de Paciente.
   * @param medicoService            Servicio de Medico.
   * @param medicoConsultorioService Servicio de Consultorios del Medico.
   */
  public CargaConsultaController(final CargaConsultaView cargaConsultaView,
      final MessageView messageView,
      final ConsultaService consultaService,
      final PacienteService pacienteService,
      final MedicoService medicoService,
      final MedicoConsultorioService medicoConsultorioService) {
    super(cargaConsultaView,
        messageView);

    this.cargaConsultaView = cargaConsultaView;
    this.consultaService = consultaService;
    this.pacienteService = pacienteService;
    this.medicoService = medicoService;
    this.medicoConsultorioService = medicoConsultorioService;
  }

  @Override
  public void initView() {
    final List<PacientePojo> pacientePojos;
    this.cargaConsultaView.getPacienteComboBox().removeAllItems();
    try {
      pacientePojos = this.pacienteService.obtenerTodosPaciente();
      for (PacientePojo pacientePojo : pacientePojos) {
        this.cargaConsultaView.getPacienteComboBox().addItem(pacientePojo);
      }
    } catch (final ServiceException e) {
      super.showMessage(ViewLabels.MENSAJE_ERROR.getLabel(), e.getMessage());
    }

    final List<MedicoPojo> medicoPojos;
    this.cargaConsultaView.getMedicoComboBox().removeAllItems();
    try {
      medicoPojos = this.medicoService.obtenerTodosMedico();
      for (MedicoPojo medicoPojo : medicoPojos) {
        this.cargaConsultaView.getMedicoComboBox().addItem(medicoPojo);
      }
    } catch (final ServiceException e) {
      super.showMessage(ViewLabels.MENSAJE_ERROR.getLabel(), e.getMessage());
    }

    if (this.consultaPojo != null) {
      this.cargaConsultaView.setTitle("Consulta " + this.consultaPojo.getConsultaId());

      this.cargaConsultaView.getPacienteComboBox().getModel()
          .setSelectedItem(this.consultaPojo.getPacientePojo());
      this.cargaConsultaView.getMedicoComboBox().getModel()
          .setSelectedItem(this.consultaPojo.getMedicoPojo());
      this.cargaConsultaView.getTfFecha()
          .setText(DateUtils.parseDate(this.consultaPojo.getFecha()));

      this.setCargaEnabled(false);
    } else {
      this.cargaConsultaView.setTitle("Creando consulta");

      this.cargaConsultaView.getMedicoComboBox().setSelectedIndex(0);
      this.cargaConsultaView.getTfFecha().setText(null);

      this.setCargaEnabled(true);
    }
    this.cargaConsultaView.getConsultorioComboBox().setEnabled(false);
    this.cargaConsultaView.getHorarioComboBox().setEnabled(false);

    this.cargaConsultaView.setVisible(true);
  }

  public void initController() {
    super.initController();

    this.cargaConsultaView.getMedicoComboBox().addItemListener(this::activateConsultorios);
    this.cargaConsultaView.getConsultorioComboBox().addItemListener(this::activateHorarios);
  }

  @Override
  protected boolean controlarCarga() {
    if (this.cargaConsultaView.getConsultorioComboBox().getItemCount() == 0) {
      super.showMessage(ViewLabels.MENSAJE_ADVERTENCIA.getLabel(),
          "Se debe seleccionar un consultorio.");
      return false;
    }
    if (this.cargaConsultaView.getTfFecha().getText().isEmpty()) {
      super.showMessage(ViewLabels.MENSAJE_ADVERTENCIA.getLabel(),
          "Se debe ingresar una fecha de consulta.");
      return false;
    }
    if (this.cargaConsultaView.getHorarioComboBox().getItemCount() == 0) {
      super.showMessage(ViewLabels.MENSAJE_ADVERTENCIA.getLabel(),
          "Se debe ingresar un horario de consulta.");
      return false;
    }
    return true;
  }

  /**
   * Indica si la carga es permitida o solo se puede actualizar la fecha de fin.
   *
   * @param enabled Si se encuentra permitido cargar todos los datos.
   */
  private void setCargaEnabled(final boolean enabled) {
    this.cargaConsultaView.getPacienteComboBox().setEnabled(enabled);
    this.cargaConsultaView.getMedicoComboBox().setEnabled(enabled);
    this.cargaConsultaView.getTfFecha().setEditable(enabled);
  }

  /**
   * Crea u obtiene una consulta.
   */
  @Override
  protected void update() {
    if (this.controlarCarga()) {
      try {
        if (this.consultaPojo == null) {
          this.consultaService.crearConsulta(this.getDataInView(), this.medicoConsultorioPojos);
          super.showMessage(ViewLabels.MENSAJE_EXITO.getLabel(),
              "Se ha creado la consulta exitosamente.");
        }
        this.cargaConsultaView.dispose();
      } catch (final ServiceException | ParseException e) {
        super.showMessage(ViewLabels.MENSAJE_ERROR.getLabel(), e.getMessage());
      }
    }
  }

  /**
   * Obtiene el POJO de la Consulta en la vista.
   *
   * @return POJO de la consulta en la vista.
   * @throws ParseException En caso de que las fechas no estén bien ingresadas.
   */
  @Override
  protected ConsultaPojo getDataInView() throws ParseException {
    return new ConsultaPojo(
        this.consultaPojo != null ? this.consultaPojo.getConsultaId() : null,
        (PacientePojo) this.cargaConsultaView.getPacienteComboBox().getModel().getSelectedItem(),
        (MedicoPojo) this.cargaConsultaView.getMedicoComboBox().getModel().getSelectedItem(),
        (ConsultorioPojo) this.cargaConsultaView.getConsultorioComboBox().getModel()
            .getSelectedItem(),
        DateUtils.getDateFormat().parse(this.cargaConsultaView.getTfFecha().getText()),
        (HorarioPojo) this.cargaConsultaView.getHorarioComboBox().getModel().getSelectedItem(),
        ((MedicoPojo) this.cargaConsultaView.getMedicoComboBox().getModel().getSelectedItem())
            .getPrecioConsulta(),
        0,
        ((MedicoPojo) this.cargaConsultaView.getMedicoComboBox().getModel().getSelectedItem())
            .getPrecioConsulta());
  }

  /**
   * Establece la consulta a visualizar en la carga.
   *
   * @param consultaPojo POJO de la consulta.
   */
  public void setConsultaPojo(final ConsultaPojo consultaPojo) {
    this.consultaPojo = consultaPojo;
  }

  /**
   * Devuelve la vista de la carga de consulta.
   *
   * @return Vista de la carga de consulta.
   */
  public CargaConsultaView getCargaConsultaView() {
    return this.cargaConsultaView;
  }

  private void activateConsultorios(final ItemEvent event) {
    if (event.getStateChange() == ItemEvent.SELECTED) {
      if (!cargaConsultaView.getConsultorioComboBox().isEnabled()) {
        cargaConsultaView.getConsultorioComboBox().setEnabled(true);
      }

      cargaConsultaView.getConsultorioComboBox().removeAllItems();
      try {
        this.medicoConsultorioPojos = medicoConsultorioService
            .obtenerConsultorios(
                (MedicoPojo) cargaConsultaView.getMedicoComboBox().getSelectedItem());
        final Set<ConsultorioPojo> consultorioPojos = new HashSet<>();

        for (MedicoConsultorioPojo medicoConsultorioPojo : this.medicoConsultorioPojos) {
          consultorioPojos.add(medicoConsultorioPojo.getConsultorioPojo());
        }
        for (ConsultorioPojo consultorioPojo : consultorioPojos) {
          this.cargaConsultaView.getConsultorioComboBox().addItem(consultorioPojo);
        }
      } catch (final ServiceException serviceException) {
        super.showMessage(ViewLabels.MENSAJE_ERROR.getLabel(), serviceException.getMessage());
      }
      if (this.cargaConsultaView.getConsultorioComboBox().getItemCount() > 0) {
        this.cargaConsultaView.getConsultorioComboBox().setSelectedIndex(0);
      }
    }
  }

  private void activateHorarios(final ItemEvent event) {
    if (event.getStateChange() == ItemEvent.SELECTED) {
      if (!this.cargaConsultaView.getHorarioComboBox().isEnabled()) {
        this.cargaConsultaView.getHorarioComboBox().setEnabled(true);
      }

      this.cargaConsultaView.getHorarioComboBox().removeAllItems();
      final Set<HorarioPojo> horarioPojos = new HashSet<>();

      for (MedicoConsultorioPojo medicoConsultorioPojo : this.medicoConsultorioPojos) {
        if (medicoConsultorioPojo.getConsultorioPojo()
            .equals(this.cargaConsultaView.getConsultorioComboBox().getSelectedItem())) {
          horarioPojos.add(medicoConsultorioPojo.getHorarioPojo());
        }
      }
      for (HorarioPojo horarioPojo : horarioPojos) {
        this.cargaConsultaView.getHorarioComboBox().addItem(horarioPojo);
      }
      if (this.cargaConsultaView.getHorarioComboBox().getItemCount() > 0) {
        this.cargaConsultaView.getHorarioComboBox().setSelectedIndex(0);
      }
    }
  }
}
