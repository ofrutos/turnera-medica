package service;

import java.util.List;
import model.pojo.ConsultorioPojo;
import service.excpetion.ServiceException;

/**
 * Interfaz de los servicios de Consultorios.
 */
public interface ConsultorioService {

  /**
   * Crea un consultorio.
   *
   * @param consultorioPojo POJO del consultorio.
   * @throws ServiceException Si ocurre un error al crear el consultorio.
   */
  void crearConsultorio(final ConsultorioPojo consultorioPojo) throws ServiceException;

  /**
   * Actualiza un consultorio.
   *
   * @param consultorioPojo POJO del consultorio.
   * @throws ServiceException Si ocurre un error al actualizar el consultorio.
   */
  void actualizarConsultorio(final ConsultorioPojo consultorioPojo) throws ServiceException;

  /**
   * Elimina un consultorio.
   *
   * @param consultorioPojo POJO del consultorio.
   * @throws ServiceException Si ocurre un error al eliminar el consultorio.
   */
  void eliminarConsultorio(final ConsultorioPojo consultorioPojo) throws ServiceException;

  /**
   * Obtiene un consultorio.
   *
   * @param consultorioId Id del consultorio.
   * @return El consultorio indicado. En caso de no existir devuelve <b>null</b>
   */
  ConsultorioPojo obtenerConsultorio(final int consultorioId);

  /**
   * Obtiene todos los consultorios.
   *
   * @return Devuelve todos los consultorio.
   * @throws ServiceException Si ocurre un error al obtener los consultorio.
   */
  List<ConsultorioPojo> obtenerTodosConsultorio() throws ServiceException;

  /**
   * Obtiene el nombre de las columnas de la tabla de consultorios.
   *
   * @return Nombre de las columnas.
   */
  List<String> obtenerNombreColumnas();
}
