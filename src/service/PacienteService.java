package service;

import java.util.List;
import model.pojo.PacientePojo;
import service.excpetion.ServiceException;

/**
 * Interfaz de los servicios de Pacientes.
 */
public interface PacienteService {

  /**
   * Crea un paciente.
   *
   * @param pacientePojo POJO del paciente.
   * @throws ServiceException Si ocurre un error al crear el paciente.
   */
  void crearPaciente(final PacientePojo pacientePojo) throws ServiceException;

  /**
   * Actualiza un paciente.
   *
   * @param pacientePojo POJO del paciente.
   * @throws ServiceException Si ocurre un error al actualizar el paciente.
   */
  void actualizarPaciente(final PacientePojo pacientePojo) throws ServiceException;

  /**
   * Elimina un paciente.
   *
   * @param pacientePojo POJO del paciente.
   * @throws ServiceException Si ocurre un error al eliminar el paciente.
   */
  void eliminarPaciente(final PacientePojo pacientePojo) throws ServiceException;

  /**
   * Obtiene un paciente.
   *
   * @param pacienteId POJO del paciente.
   * @return El paciente indicado. En caso de no existir devuelve <b>null</b>
   */
  PacientePojo obtenerPaciente(final int pacienteId);

  /**
   * Obtiene un paciente.
   *
   * @return Devuelve todos los pacientes.
   * @throws ServiceException Si ocurre un error al obtener los pacientes.
   */
  List<PacientePojo> obtenerTodosPaciente() throws ServiceException;

  /**
   * Obtiene el nombre de las columnas de la tabla de pacientes.
   *
   * @return Nombre de las columnas.
   */
  List<String> obtenerNombreColumnas();
}
