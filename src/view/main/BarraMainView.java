package view.main;

import view.GeneralBarraView;

/**
 * Clase que administra los componentes visuales de la barra de menú de la ventana principal.
 */
public class BarraMainView extends GeneralBarraView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Crea la barra del menu principal.
   */
  public BarraMainView() {
    super();
  }

  /**
   * Inicializa los componentes visuales.
   */
  public void initView() {
    super.initLabels();
  }
}
