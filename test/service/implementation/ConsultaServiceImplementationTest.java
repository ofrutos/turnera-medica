package service.implementation;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static util.pojo.ConsultaPojoMock.getAllConsultas;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import model.dao.ConsultaDao;
import model.dao.exception.DAOException;
import model.dao.implementation.sqlite.ConsultaDaoImplementation;
import model.pojo.ConsultaPojo;
import model.pojo.ConsultorioPojo;
import model.pojo.HorarioPojo;
import model.pojo.MedicoPojo;
import model.pojo.PacientePojo;
import model.util.DateUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import service.ConsultaService;
import service.excpetion.ServiceException;

public class ConsultaServiceImplementationTest {

  @Rule
  public MockitoRule initRule = MockitoJUnit.rule();

  @Mock
  private final ConsultaDao consultaDao = new ConsultaDaoImplementation();

  private ConsultaService consultaService;

  @Before
  public void setUp() {
    this.consultaService = new ConsultaServiceImplementation(this.consultaDao);
  }

//  @Ignore
//  public void crearConsulta_Ok() throws DAOException, ServiceException {
//    doNothing().when(this.consultaDao).createConsulta(isA(ConsultaPojo.class));
//
//    final ConsultaPojo consultaPojo = new ConsultaPojo();
//
//    this.consultaService.crearConsulta(consultaPojo, this.medicoConsultorioPojos);
//  }
//
//  @Test(expected = ServiceException.class)
//  public void crearConsulta_Error() throws DAOException, ServiceException {
//    doThrow(DAOException.class).when(this.consultaDao).createConsulta(isA(ConsultaPojo.class));
//
//    final ConsultaPojo consultaPojo = new ConsultaPojo();
//
//    this.consultaService.crearConsulta(consultaPojo, this.medicoConsultorioPojos);
//  }

  @Ignore
  public void obtenerConsultaPaciente_Ok() throws DAOException, ServiceException, ParseException {
    when(this.consultaDao.getAllConsultas()).thenReturn(getAllConsultas());

    final List<ConsultaPojo> expectedConsultaPojos = new ArrayList<>();

    final PacientePojo pacientePojo1 = new PacientePojo();
    pacientePojo1.setUsuarioId(9998);

    final MedicoPojo medicoPojo1 = new MedicoPojo();
    medicoPojo1.setUsuarioId(9995);

    final ConsultorioPojo consultorioPojo1 = new ConsultorioPojo();
    consultorioPojo1.setConsultorioId(103);

    final HorarioPojo horarioPojo = new HorarioPojo(36, "", "");

    final ConsultaPojo consultaPojo1 = new ConsultaPojo();
    consultaPojo1.setConsultaId(2998);
    consultaPojo1.setPacientePojo(pacientePojo1);
    consultaPojo1.setMedicoPojo(medicoPojo1);
    consultaPojo1.setConsultorioPojo(consultorioPojo1);
    consultaPojo1.setFecha(DateUtils.getDateFormat().parse("16/09/2019"));
    consultaPojo1.setHorarioPojo(horarioPojo);
    consultaPojo1.setPrecioBase("640");
    consultaPojo1.setDescuento(0);
    consultaPojo1.setPrecioFinal("640");

    expectedConsultaPojos.add(consultaPojo1);

    final PacientePojo pacientePojo2 = new PacientePojo();
    pacientePojo1.setUsuarioId(9998);

    final MedicoPojo medicoPojo2 = new MedicoPojo();
    medicoPojo1.setUsuarioId(9995);

    final ConsultorioPojo consultorioPojo2 = new ConsultorioPojo();
    consultorioPojo1.setConsultorioId(103);

    final ConsultaPojo consultaPojo2 = new ConsultaPojo();
    consultaPojo2.setConsultaId(2997);
    consultaPojo2.setPacientePojo(pacientePojo2);
    consultaPojo2.setMedicoPojo(medicoPojo2);
    consultaPojo2.setConsultorioPojo(consultorioPojo2);
    consultaPojo2.setFecha(DateUtils.getDateFormat().parse("08/11/2019"));
    consultaPojo2.setHorarioPojo(horarioPojo);
    consultaPojo2.setPrecioBase("640");
    consultaPojo2.setDescuento(0);
    consultaPojo2.setPrecioFinal("640");

    expectedConsultaPojos.add(consultaPojo2);

    expectedConsultaPojos.sort(Comparator.comparing(ConsultaPojo::getConsultaId));

    final PacientePojo pacientePojo = new PacientePojo();
    pacientePojo.setUsuarioId(9998);

    final List<ConsultaPojo> actualConsultaPojos = this.consultaService
        .obtenerConsultas(pacientePojo);

    assertEquals(expectedConsultaPojos, actualConsultaPojos);
  }

  @Ignore
  public void obtenerConsultaMedico_Ok() throws ServiceException, DAOException, ParseException {
    when(this.consultaDao.getAllConsultas()).thenReturn(getAllConsultas());

    final List<ConsultaPojo> expectedConsultaPojos = new ArrayList<>();

    final PacientePojo pacientePojo1 = new PacientePojo();
    pacientePojo1.setUsuarioId(9998);

    final MedicoPojo medicoPojo1 = new MedicoPojo();
    medicoPojo1.setUsuarioId(9995);

    final ConsultorioPojo consultorioPojo1 = new ConsultorioPojo();
    consultorioPojo1.setConsultorioId(103);

    final HorarioPojo horarioPojo = new HorarioPojo(36, "", "");

    final ConsultaPojo consultaPojo1 = new ConsultaPojo();
    consultaPojo1.setConsultaId(2998);
    consultaPojo1.setPacientePojo(pacientePojo1);
    consultaPojo1.setMedicoPojo(medicoPojo1);
    consultaPojo1.setConsultorioPojo(consultorioPojo1);
    consultaPojo1.setFecha(DateUtils.getDateFormat().parse("16/09/2019"));
    consultaPojo1.setHorarioPojo(horarioPojo);
    consultaPojo1.setPrecioBase("640");
    consultaPojo1.setDescuento(0);
    consultaPojo1.setPrecioFinal("640");

    expectedConsultaPojos.add(consultaPojo1);

    final PacientePojo pacientePojo2 = new PacientePojo();
    pacientePojo1.setUsuarioId(9998);

    final MedicoPojo medicoPojo2 = new MedicoPojo();
    medicoPojo1.setUsuarioId(9995);

    final ConsultorioPojo consultorioPojo2 = new ConsultorioPojo();
    consultorioPojo1.setConsultorioId(103);

    final ConsultaPojo consultaPojo2 = new ConsultaPojo();
    consultaPojo2.setConsultaId(2997);
    consultaPojo2.setPacientePojo(pacientePojo2);
    consultaPojo2.setMedicoPojo(medicoPojo2);
    consultaPojo2.setConsultorioPojo(consultorioPojo2);
    consultaPojo2.setFecha(DateUtils.getDateFormat().parse("08/11/2019"));
    consultaPojo2.setHorarioPojo(horarioPojo);
    consultaPojo2.setPrecioBase("640");
    consultaPojo2.setDescuento(0);
    consultaPojo2.setPrecioFinal("640");

    expectedConsultaPojos.add(consultaPojo2);

    final PacientePojo pacientePojo3 = new PacientePojo();
    pacientePojo1.setUsuarioId(9997);

    final MedicoPojo medicoPojo3 = new MedicoPojo();
    medicoPojo1.setUsuarioId(9995);

    final ConsultorioPojo consultorioPojo3 = new ConsultorioPojo();
    consultorioPojo1.setConsultorioId(103);

    final ConsultaPojo consultaPojo3 = new ConsultaPojo();
    consultaPojo3.setConsultaId(2996);
    consultaPojo3.setPacientePojo(pacientePojo3);
    consultaPojo3.setMedicoPojo(medicoPojo3);
    consultaPojo3.setConsultorioPojo(consultorioPojo3);
    consultaPojo3.setFecha(DateUtils.getDateFormat().parse("08/09/2019"));
    consultaPojo3.setHorarioPojo(horarioPojo);
    consultaPojo3.setPrecioBase("640");
    consultaPojo3.setDescuento(0);
    consultaPojo3.setPrecioFinal("640");

    expectedConsultaPojos.add(consultaPojo3);

    expectedConsultaPojos.sort(Comparator.comparing(ConsultaPojo::getConsultaId));

    final MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(9995);

    final List<ConsultaPojo> actualConsultaPojos = this.consultaService
        .obtenerConsultas(medicoPojo);

    assertEquals(expectedConsultaPojos, actualConsultaPojos);
  }
}
