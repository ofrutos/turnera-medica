package util.pojo;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import model.pojo.ConsultaPojo;
import model.pojo.ConsultorioPojo;
import model.pojo.HorarioPojo;
import model.pojo.MedicoPojo;
import model.pojo.PacientePojo;
import model.util.DateUtils;

public class ConsultaPojoMock {

  public static ConsultaPojo getConsultaUno() throws ParseException {
    final PacientePojo pacientePojo = new PacientePojo();
    pacientePojo.setUsuarioId(9999);

    final MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(9996);

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();
    consultorioPojo.setConsultorioId(101);

    final HorarioPojo horarioPojo = new HorarioPojo(36, "", "");

    final ConsultaPojo consultaPojo = new ConsultaPojo();
    consultaPojo.setConsultaId(2999);
    consultaPojo.setPacientePojo(pacientePojo);
    consultaPojo.setMedicoPojo(medicoPojo);
    consultaPojo.setConsultorioPojo(consultorioPojo);
    consultaPojo.setFecha(DateUtils.getDateFormat().parse("08/09/2019"));
    consultaPojo.setHorarioPojo(horarioPojo);
    consultaPojo.setPrecioBase("640");
    consultaPojo.setDescuento(0);
    consultaPojo.setPrecioFinal("640");

    return consultaPojo;
  }

  public static ConsultaPojo getConsultaDos() throws ParseException {
    final PacientePojo pacientePojo = new PacientePojo();
    pacientePojo.setUsuarioId(9998);

    final MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(9995);

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();
    consultorioPojo.setConsultorioId(103);

    final HorarioPojo horarioPojo = new HorarioPojo(36, "", "");

    final ConsultaPojo consultaPojo = new ConsultaPojo();
    consultaPojo.setConsultaId(2998);
    consultaPojo.setPacientePojo(pacientePojo);
    consultaPojo.setMedicoPojo(medicoPojo);
    consultaPojo.setConsultorioPojo(consultorioPojo);
    consultaPojo.setFecha(DateUtils.getDateFormat().parse("16/09/2019"));
    consultaPojo.setHorarioPojo(horarioPojo);
    consultaPojo.setPrecioBase("640");
    consultaPojo.setDescuento(0);
    consultaPojo.setPrecioFinal("640");

    return consultaPojo;
  }

  public static ConsultaPojo getConsultaTres() throws ParseException {
    final PacientePojo pacientePojo = new PacientePojo();
    pacientePojo.setUsuarioId(9998);

    final MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(9995);

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();
    consultorioPojo.setConsultorioId(103);

    final HorarioPojo horarioPojo = new HorarioPojo(36, "", "");

    final ConsultaPojo consultaPojo = new ConsultaPojo();
    consultaPojo.setConsultaId(2997);
    consultaPojo.setPacientePojo(pacientePojo);
    consultaPojo.setMedicoPojo(medicoPojo);
    consultaPojo.setConsultorioPojo(consultorioPojo);
    consultaPojo.setFecha(DateUtils.getDateFormat().parse("08/11/2019"));
    consultaPojo.setHorarioPojo(horarioPojo);
    consultaPojo.setPrecioBase("640");
    consultaPojo.setDescuento(0);
    consultaPojo.setPrecioFinal("640");

    return consultaPojo;
  }

  public static ConsultaPojo getConsultaCuatro() throws ParseException {
    final PacientePojo pacientePojo = new PacientePojo();
    pacientePojo.setUsuarioId(9997);

    final MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(9995);

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();
    consultorioPojo.setConsultorioId(103);

    final HorarioPojo horarioPojo = new HorarioPojo(36, "", "");

    final ConsultaPojo consultaPojo = new ConsultaPojo();
    consultaPojo.setConsultaId(2996);
    consultaPojo.setPacientePojo(pacientePojo);
    consultaPojo.setMedicoPojo(medicoPojo);
    consultaPojo.setConsultorioPojo(consultorioPojo);
    consultaPojo.setFecha(DateUtils.getDateFormat().parse("08/09/2019"));
    consultaPojo.setHorarioPojo(horarioPojo);
    consultaPojo.setPrecioBase("640");
    consultaPojo.setDescuento(0);
    consultaPojo.setPrecioFinal("640");

    return consultaPojo;
  }

  public static ConsultaPojo getConsultaCinco() throws ParseException {
    final PacientePojo pacientePojo = new PacientePojo();
    pacientePojo.setUsuarioId(9999);

    final MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(9996);

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();
    consultorioPojo.setConsultorioId(103);

    final HorarioPojo horarioPojo = new HorarioPojo(36, "", "");

    final ConsultaPojo consultaPojo = new ConsultaPojo();
    consultaPojo.setConsultaId(2995);
    consultaPojo.setPacientePojo(pacientePojo);
    consultaPojo.setMedicoPojo(medicoPojo);
    consultaPojo.setConsultorioPojo(consultorioPojo);
    consultaPojo.setFecha(DateUtils.getDateFormat().parse("08/09/2019"));
    consultaPojo.setHorarioPojo(horarioPojo);
    consultaPojo.setPrecioBase("640");
    consultaPojo.setDescuento(0);
    consultaPojo.setPrecioFinal("640");

    return consultaPojo;
  }

  public static Map<Integer, ConsultaPojo> getConsultasPacienteTres() throws ParseException {
    final Map<Integer, ConsultaPojo> consultaPojos = new HashMap<>();

    consultaPojos.put(2998, getConsultaDos());
    consultaPojos.put(2997, getConsultaTres());

    return consultaPojos;
  }

  public static Map<Integer, ConsultaPojo> getConsultasMedicoDos() throws ParseException {
    final Map<Integer, ConsultaPojo> consultaPojos = new HashMap<>();

    consultaPojos.put(2998, getConsultaDos());
    consultaPojos.put(2997, getConsultaTres());
    consultaPojos.put(2996, getConsultaCuatro());

    return consultaPojos;
  }

  public static Map<Integer, ConsultaPojo> getAllConsultas() throws ParseException {
    final Map<Integer, ConsultaPojo> consultaPojos = new TreeMap<>();

    consultaPojos.put(2999, getConsultaUno());
    consultaPojos.put(2998, getConsultaDos());
    consultaPojos.put(2997, getConsultaTres());
    consultaPojos.put(2996, getConsultaCuatro());
    consultaPojos.put(2995, getConsultaCinco());

    return consultaPojos;
  }
}
