package model.dao.utils;

/**
 * Clase con funciones para labels de la BD.
 */
public class LabelUtils {

  private LabelUtils() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Normaliza un label de la BD
   *
   * @param label Nombre de la columna
   * @return Nombre normalizado.
   */
  public static String normalizarLabel(final String label) {
    return label.replace('_', ' ');
  }
}
