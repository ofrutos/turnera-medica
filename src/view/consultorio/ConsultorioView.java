package view.consultorio;

import javax.swing.JTable;
import view.GeneralView;
import view.constants.ViewLabels;

/**
 * Clase de la vista principal de consultorios.
 */
public class ConsultorioView extends GeneralView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private final BarraConsultorioView barraConsultorioView;

  /**
   * Crea la vista principal de consultorios.
   *
   * @param barraConsultorioView Barra de la vista de consultorios.
   */
  public ConsultorioView(final BarraConsultorioView barraConsultorioView) {
    super(barraConsultorioView);

    this.barraConsultorioView = barraConsultorioView;
    this.barraConsultorioView.initView();

    super.jFrame.setTitle(ViewLabels.TITULO_CONSULTORIOS.getLabel());
  }

  /**
   * Devuelve la barra de la vista de consultorios.
   *
   * @return Barra de la vista de consultorios.
   */
  public BarraConsultorioView getBarraConsultorioView() {
    return this.barraConsultorioView;
  }

  /**
   * Devuelve la tabla de los consultorios.
   *
   * @return Tabla de los consultorios.
   */
  public JTable getTablaConsultorio() {
    return super.getjTable();
  }
}
