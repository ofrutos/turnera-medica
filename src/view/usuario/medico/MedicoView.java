package view.usuario.medico;

import javax.swing.JTable;
import view.constants.ViewLabels;
import view.usuario.UsuarioView;

/**
 * Clase de la vista principal de medicos.
 */
public class MedicoView extends UsuarioView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private final BarraMedicoView barraMedicoView;

  /**
   * Crea la vista principal de medicos.
   *
   * @param barraMedicoView Barra de la vista de medicos.
   */
  public MedicoView(final BarraMedicoView barraMedicoView) {
    super(barraMedicoView);

    this.barraMedicoView = barraMedicoView;
    this.barraMedicoView.initView();

    super.jFrame.setTitle(ViewLabels.TITULO_MEDICOS.getLabel());
  }

  /**
   * Devuelve la barra de la vista de medicos.
   *
   * @return Barra de la vista de medicos.
   */
  public BarraMedicoView getBarraMedicoView() {
    return this.barraMedicoView;
  }

  /**
   * Devuelve la tabla de medicos.
   *
   * @return Tabla de medicos.
   */
  public JTable getTablaMedico() {
    return super.getjTable();
  }
}
