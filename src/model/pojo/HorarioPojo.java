package model.pojo;

import java.util.Objects;

public class HorarioPojo {

  private Integer horarioId;
  private String horaInicio;
  private String horaFin;

  public HorarioPojo() {
  }

  public HorarioPojo(final Integer horarioId,
      final String horaInicio,
      final String horaFin) {
    this.horarioId = horarioId;
    this.horaInicio = horaInicio;
    this.horaFin = horaFin;
  }

  public Integer getHorarioId() {
    return this.horarioId;
  }

  public String getHoraInicio() {
    return this.horaInicio;
  }

  public String getHoraFin() {
    return this.horaFin;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HorarioPojo that = (HorarioPojo) o;
    return Objects.equals(horarioId, that.horarioId) &&
        Objects.equals(horaInicio, that.horaInicio) &&
        Objects.equals(horaFin, that.horaFin);
  }

  @Override
  public int hashCode() {
    return Objects.hash(horarioId, horaInicio, horaFin);
  }

  @Override
  public String toString() {
    return this.horaInicio + " - " + this.horaFin;
  }
}
