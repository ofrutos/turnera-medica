package service.implementation;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import model.dao.MedicoConsultorioDao;
import model.dao.exception.DAOException;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoConsultorioPojoKey;
import model.pojo.MedicoPojo;
import model.util.DateUtils;
import service.MedicoConsultorioService;
import service.excpetion.ServiceException;

/**
 * Clase que implementa los servicios de consultorios de médicos.
 */
public class MedicoConsultorioServiceImplementation implements MedicoConsultorioService {

  private final MedicoConsultorioDao medicoConsultorioDao;
  private Map<MedicoConsultorioPojoKey, MedicoConsultorioPojo> medicoConsultorioPojoMap;

  /**
   * Crea una implementación del servicio de consultorios del medico.
   *
   * @param medicoConsultorioDao DAO del consultorio del medico.
   */
  public MedicoConsultorioServiceImplementation(final MedicoConsultorioDao medicoConsultorioDao) {
    this.medicoConsultorioDao = medicoConsultorioDao;
    this.medicoConsultorioPojoMap = new TreeMap<>();
  }

  @Override
  public List<MedicoConsultorioPojo> obtenerConsultorios(final MedicoPojo medicoPojo)
      throws ServiceException {
    if (this.medicoConsultorioPojoMap.isEmpty()) {
      try {
        this.medicoConsultorioPojoMap = this.medicoConsultorioDao.getAllMedicoConsultorio();
      } catch (final DAOException e) {
        throw new ServiceException(e.getMessage());
      }
    }
    return this.medicoConsultorioPojoMap.values()
        .stream()
        .filter(medicoConsultorioPojo -> medicoConsultorioPojo.getMedicoPojo().getUsuarioId()
            .equals(medicoPojo.getUsuarioId()))
        .sorted(Comparator.comparing(MedicoConsultorioPojo::getMedicoConsultorioPojoKey))
        .collect(Collectors.toList());
  }

  @Override
  public void crearConsultorio(final MedicoConsultorioPojo medicoConsultorioPojo)
      throws ServiceException {
    try {
      this.checkMedicoConsultorio(medicoConsultorioPojo);

      this.medicoConsultorioDao.createMedicoConsultorio(medicoConsultorioPojo);
      this.medicoConsultorioPojoMap
          .put(medicoConsultorioPojo.getMedicoConsultorioPojoKey(), medicoConsultorioPojo);
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public void actualizarConsultorio(final MedicoConsultorioPojo medicoConsultorioPojo)
      throws ServiceException {
    try {
      this.checkMedicoConsultorio(medicoConsultorioPojo);

      this.medicoConsultorioDao.updateMedicoConsultorio(medicoConsultorioPojo);
      this.medicoConsultorioPojoMap
          .put(medicoConsultorioPojo.getMedicoConsultorioPojoKey(), medicoConsultorioPojo);
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public List<String> obtenerNombreColumnas() {
    return this.medicoConsultorioDao.getMedicoConsultorioNombreColumna();
  }

  /**
   * Controla que los datos ingresados para el consultorio de un medico sean validos.
   *
   * @param medicoConsultorioPojo POJO del consultorio de un medico.
   * @throws ServiceException Si ocurre un error al momento de validar la información.
   */
  private void checkMedicoConsultorio(final MedicoConsultorioPojo medicoConsultorioPojo)
      throws ServiceException {
    this.checkRangoFechasValido(medicoConsultorioPojo);
    this.checkConsultorioDisponible(medicoConsultorioPojo);
  }

  /**
   * Controla que el rango de las fechas ingresado sea válido.
   *
   * @param medicoConsultorioPojo POJO del consultorio del medico.
   * @throws ServiceException Si el rango de fechas no es válido.
   */
  private void checkRangoFechasValido(final MedicoConsultorioPojo medicoConsultorioPojo)
      throws ServiceException {
    if (!DateUtils.isRangoFechasValida(medicoConsultorioPojo.getFechaInicio(),
        medicoConsultorioPojo.getFechaFin())) {
      throw new ServiceException("Las fechas ingresadas tienen un rango inválido.");
    }
  }

  /**
   * Controla la disponibilidad del consultorio.
   *
   * @param medicoConsultorioPojo POJO del consultorio del medico.
   * @throws ServiceException Si el consultorio se encuentra ocupado en el rango de fechas y hora
   *                          indicados.
   */
  private void checkConsultorioDisponible(final MedicoConsultorioPojo medicoConsultorioPojo)
      throws ServiceException {
    if (this.medicoConsultorioPojoMap.values()
        .stream()
        .anyMatch(actualMedicoConsultorioPojo -> checkDisponibilidad(actualMedicoConsultorioPojo,
            medicoConsultorioPojo))) {
      throw new ServiceException(
          "El consultorio ya se encuentra ocupado dentro del rango de fechas y horario indicado.");
    }
  }

  /**
   * Controla la disponibilidad de un consultorio.
   *
   * @param actualMedicoConsultorio  POJO de un consultorio de un medico.
   * @param newMedicoConsultorioPojo POJO del consultorio de un medico a generar.
   * @return <b>true</b> si el consultorio ya se encuentra ocupado en las fechas indicadas,
   * <b>false</b> de lo contrario.
   */
  private boolean checkDisponibilidad(final MedicoConsultorioPojo actualMedicoConsultorio,
      final MedicoConsultorioPojo newMedicoConsultorioPojo) {
    return actualMedicoConsultorio.getConsultorioPojo().getConsultorioId()
        .equals(newMedicoConsultorioPojo.getConsultorioPojo().getConsultorioId())
        && actualMedicoConsultorio.getHorarioPojo().getHorarioId()
        .equals(newMedicoConsultorioPojo.getHorarioPojo().getHorarioId())
        && (DateUtils.isDateBetween(actualMedicoConsultorio.getFechaInicio(),
        actualMedicoConsultorio.getFechaFin(),
        newMedicoConsultorioPojo.getFechaInicio())
        || DateUtils.isDateBetween(actualMedicoConsultorio.getFechaInicio(),
        actualMedicoConsultorio.getFechaFin(),
        (actualMedicoConsultorio.getFechaFin().equals(DateUtils.getMaximumDate())
            ? newMedicoConsultorioPojo.getFechaFin() : actualMedicoConsultorio.getFechaInicio()))
        && !actualMedicoConsultorio.getMedicoPojo().getUsuarioId()
        .equals(newMedicoConsultorioPojo.getMedicoPojo().getUsuarioId()));
  }
}
