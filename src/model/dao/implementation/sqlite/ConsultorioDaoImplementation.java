package model.dao.implementation.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import model.constants.table.ConsultorioTableConstants;
import model.dao.ConsultorioDao;
import model.dao.exception.DAOException;
import model.dao.utils.LabelUtils;
import model.jdbc.DBManager;
import model.pojo.ConsultorioPojo;
import org.apache.commons.lang3.EnumUtils;

/**
 * Clase que administra los DAO para los consultorios en una base SQLite. <br>
 */
public class ConsultorioDaoImplementation implements ConsultorioDao {

  /**
   * Declaración de SELECT (All) de la tabla consultorio. <br>
   */
  private static final String SELECT_ALL_CONSULTORIOS = "SELECT * FROM Consultorio";
  /**
   * Obtiene un consultorio. <br>
   */
  private static final String SELECT_ONE_CONSULTORIOS = "SELECT * FROM Consultorio WHERE Consultorio_Id = ?";
  /**
   * Declaración de INSERT de la tabla consultorio. <br>
   */
  private static final String INSERT_CONSULTORIO = "INSERT INTO Consultorio (Nombre) VALUES (?)";

  /**
   * Declaración de UPDATE de la tabla consultorio. <br>
   */
  private static final String UPDATE_CONSULTORIO = "UPDATE Consultorio SET Nombre = ? WHERE Consultorio_Id = ?";

  /**
   * Declaración de DELETE de la tabla consultorio. <br>
   */
  private static final String DELETE_CONSULTORIO = "DELETE FROM Consultorio WHERE Consultorio_Id = ?";

  /**
   * Conexión con la base de datos. <br>
   */
  private Connection connection;

  public void createConsultorio(final ConsultorioPojo consultorioPojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(INSERT_CONSULTORIO);
      statement.setString(1, consultorioPojo.getNombre());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows created.");
      }

      ResultSet resultSet = statement.getGeneratedKeys();

      if (resultSet != null && resultSet.next()) {
        consultorioPojo.setConsultorioId(resultSet.getInt(1));
      }
      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public void updateConsultorio(final ConsultorioPojo consultorioPojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(UPDATE_CONSULTORIO);
      statement.setString(1, consultorioPojo.getNombre());
      statement.setInt(2, consultorioPojo.getConsultorioId());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows updated.");
      }

      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public void deleteConsultorio(final ConsultorioPojo consultorioPojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(DELETE_CONSULTORIO);
      statement.setInt(1, consultorioPojo.getConsultorioId());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows deleted.");
      }

      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public Map<Integer, ConsultorioPojo> getAllConsultorio() {
    Map<Integer, ConsultorioPojo> consultorioPojos = new TreeMap<>();
    this.connection = DBManager.connect();
    try {
      ResultSet resultSet = this.connection.createStatement().executeQuery(SELECT_ALL_CONSULTORIOS);
      while (resultSet.next()) {
        consultorioPojos.put(resultSet.getInt(1),
            new ConsultorioPojo(resultSet.getInt(1),
                resultSet.getString(2)));
      }
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
    } finally {
      DBManager.handleClose(this.connection);
    }
    return consultorioPojos;
  }

  @Override
  public List<String> getConsultorioNombreColumna() {
    this.connection = DBManager.connect();
    List<String> cabecera = new ArrayList<>();
    try {
      PreparedStatement statement = this.connection.prepareStatement(SELECT_ONE_CONSULTORIOS);
      // Solo nos interesa conseguir la cabecera.
      statement.setInt(1, 0);

      ResultSetMetaData meta = statement.getMetaData();
      int cantidadColumnas = statement.getMetaData().getColumnCount();

      for (int columna = 1; columna <= cantidadColumnas; columna++) {
        cabecera.add(meta.getColumnName(columna));
      }

      cabecera = cabecera.stream()
          .filter(this::filtrarColumnasPrivadas)
          .map(LabelUtils::normalizarLabel)
          .collect(Collectors.toList());
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
    } finally {
      DBManager.handleClose(this.connection);
    }

    return cabecera;
  }

  @Override
  public ConsultorioPojo getConsultorio(final Integer consultorioId) {
    this.connection = DBManager.connect();
    ConsultorioPojo consultorioPojo = null;
    try {
      PreparedStatement preparedStatement = this.connection
          .prepareStatement(SELECT_ONE_CONSULTORIOS);
      preparedStatement.setInt(1, consultorioId);

      ResultSet resultSet = preparedStatement.executeQuery();
      if (resultSet.next()) {
        consultorioPojo = new ConsultorioPojo();
        consultorioPojo.setConsultorioId(resultSet.getInt(1));
        consultorioPojo.setNombre(resultSet.getString(2));
      }
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
    } finally {
      DBManager.handleClose(this.connection);
    }
    return consultorioPojo;
  }

  /**
   * Indica las columnas que no deben ser visualizadas.
   *
   * @param columna Nombre de la columna.
   * @return <b>true</b> si la columna debe ser visualizada.<br>
   * <b>false</b> si no debe ser visualizada.
   */
  public boolean filtrarColumnasPrivadas(final String columna) {
    return EnumUtils.getEnumIgnoreCase(ConsultorioTableConstants.class, columna) != null;
  }
}
