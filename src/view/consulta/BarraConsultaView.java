package view.consulta;

import javax.swing.JMenuItem;
import view.GeneralBarraView;
import view.constants.ViewLabels;

/**
 * Clase que administra la barra de la vista de consultas.
 */
public class BarraConsultaView extends GeneralBarraView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private JMenuItem consultaLbl;

  /**
   * Crea la vista de la barra de consultas.
   */
  public BarraConsultaView() {
    super();
  }

  /**
   * Inicializa los componentes visuales.
   */
  public void initView() {
    super.initLabels();

    this.consultaLbl = new JMenuItem(ViewLabels.CONSULTA.getLabel());
    super.getNuevoLbl().add(this.consultaLbl);
  }

  public JMenuItem getConsultaLbl() {
    return this.consultaLbl;
  }
}
