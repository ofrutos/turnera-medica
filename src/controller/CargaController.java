package controller;

import view.CargaView;
import view.message.MessageView;

/**
 * Clase que administra un controlador para una carga de datos.
 *
 * @param <T> POJO a la cual se le realizan controles.
 */
public abstract class CargaController<T> implements InitializableController, ViewableController {

  private final CargaView cargaView;
  private final MessageView messageView;

  /**
   * Crea un controlador para la carga de datos.
   *
   * @param cargaView   Vista de la carga de datos.
   * @param messageView Vista de mensajes.
   */
  public CargaController(final CargaView cargaView,
      final MessageView messageView) {
    this.cargaView = cargaView;
    this.messageView = messageView;
  }

  @Override
  public void initController() {
    this.cargaView.getCancelButton().addActionListener(e -> cancel());
    this.cargaView.getOkButton().addActionListener(e -> update());
  }

  private void cancel() {
    this.cargaView.dispose();
  }

  /**
   * Muestra un mensaje.
   *
   * @param titulo  Titulo de la ventana.
   * @param mensaje Mensaje a mostrar.
   */
  public void showMessage(final String titulo,
      final String mensaje) {
    this.messageView.setTitle(titulo);
    this.messageView.setMensaje(mensaje);
    this.messageView.setVisible(true);
  }

  /**
   * Controla que la carga de datos sea valida.
   *
   * @return <b>true</b> si la información esta completa, <b>false</b> de lo contrario.
   */
  protected abstract boolean controlarCarga();

  /**
   * Actualiza con la información indicada.
   */
  protected abstract void update();

  /**
   * Obtiene la información cargada.
   *
   * @return POJO de la información cargada.
   * @throws Exception En caso de que ocurra un error.
   */
  protected abstract T getDataInView() throws Exception;
}
