package controller.main;

import controller.GeneralBarraController;
import controller.InitializableController;
import view.main.BarraMainView;

/**
 * Clase que administra el controlador de la barra de menú de la ventana principal.
 */
public class BarraMainController extends GeneralBarraController implements InitializableController {

  private final BarraMainView barraMainView;

  public BarraMainController(final BarraMainView barraMainView) {
    super(barraMainView);
    this.barraMainView = barraMainView;
  }

  /**
   * Inicializa los controladores.
   */
  public void initController() {
    super.initController();

    this.barraMainView.getNuevoLbl().setEnabled(false);
  }
}
