package view.consulta;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import model.pojo.ConsultorioPojo;
import model.pojo.HorarioPojo;
import model.pojo.MedicoPojo;
import model.pojo.PacientePojo;
import view.CargaView;
import view.util.DateFocusEvent;
import view.validator.TurneraValidator;

/**
 * Clase que administra la vista de la carga de una consulta.
 */
public class CargaConsultaView extends CargaView {

  private final JComboBox<PacientePojo> pacienteComboBox;
  private final JComboBox<MedicoPojo> medicoComboBox;
  private final JComboBox<ConsultorioPojo> consultorioComboBox;
  private final JFormattedTextField tfFecha;
  private final JComboBox<HorarioPojo> horarioComboBox;

  /**
   * Crea la carga de una consulta.
   */
  public CargaConsultaView() {
    setBounds(100, 100, 334, 283);

    this.jPanel = new JPanel();
    this.jPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(this.jPanel, BorderLayout.NORTH);
    GridBagLayout gblPanel = new GridBagLayout();
    gblPanel.columnWidths = new int[]{0, 0, 0};
    gblPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
    gblPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
    gblPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    this.jPanel.setLayout(gblPanel);

    JLabel lblPaciente = new JLabel("Paciente:");
    GridBagConstraints gbcLblPaciente = new GridBagConstraints();
    gbcLblPaciente.fill = GridBagConstraints.VERTICAL;
    gbcLblPaciente.anchor = GridBagConstraints.EAST;
    gbcLblPaciente.insets = new Insets(0, 0, 5, 5);
    gbcLblPaciente.gridx = 0;
    gbcLblPaciente.gridy = 0;
    this.jPanel.add(lblPaciente, gbcLblPaciente);

    this.pacienteComboBox = new JComboBox<>();
    GridBagConstraints gbcPacienteComboBox = new GridBagConstraints();
    gbcPacienteComboBox.insets = new Insets(0, 0, 5, 0);
    gbcPacienteComboBox.fill = GridBagConstraints.HORIZONTAL;
    gbcPacienteComboBox.gridx = 1;
    gbcPacienteComboBox.gridy = 0;
    this.jPanel.add(this.pacienteComboBox, gbcPacienteComboBox);

    JLabel lblMedico = new JLabel("Medico:");
    GridBagConstraints gbcLblMedico = new GridBagConstraints();
    gbcLblMedico.fill = GridBagConstraints.VERTICAL;
    gbcLblMedico.anchor = GridBagConstraints.EAST;
    gbcLblMedico.insets = new Insets(0, 0, 5, 5);
    gbcLblMedico.gridx = 0;
    gbcLblMedico.gridy = 1;
    this.jPanel.add(lblMedico, gbcLblMedico);

    this.medicoComboBox = new JComboBox<>();
    GridBagConstraints gbcMedicoComboBox = new GridBagConstraints();
    gbcMedicoComboBox.insets = new Insets(0, 0, 5, 0);
    gbcMedicoComboBox.fill = GridBagConstraints.HORIZONTAL;
    gbcMedicoComboBox.gridx = 1;
    gbcMedicoComboBox.gridy = 1;
    this.jPanel.add(this.medicoComboBox, gbcMedicoComboBox);

    JLabel lblConsultorio = new JLabel("Consultorio:");
    GridBagConstraints gbcLblConsultorio = new GridBagConstraints();
    gbcLblConsultorio.fill = GridBagConstraints.VERTICAL;
    gbcLblConsultorio.anchor = GridBagConstraints.EAST;
    gbcLblConsultorio.insets = new Insets(0, 0, 5, 5);
    gbcLblConsultorio.gridx = 0;
    gbcLblConsultorio.gridy = 2;
    this.jPanel.add(lblConsultorio, gbcLblConsultorio);

    this.consultorioComboBox = new JComboBox<>();
    GridBagConstraints gbcConsultorioComboBox = new GridBagConstraints();
    gbcConsultorioComboBox.insets = new Insets(0, 0, 5, 0);
    gbcConsultorioComboBox.fill = GridBagConstraints.HORIZONTAL;
    gbcConsultorioComboBox.gridx = 1;
    gbcConsultorioComboBox.gridy = 2;
    this.jPanel.add(this.consultorioComboBox, gbcConsultorioComboBox);

    JLabel lblFecha = new JLabel("Fecha:");
    GridBagConstraints gbcLblFecha = new GridBagConstraints();
    gbcLblFecha.anchor = GridBagConstraints.EAST;
    gbcLblFecha.insets = new Insets(0, 0, 5, 5);
    gbcLblFecha.gridx = 0;
    gbcLblFecha.gridy = 3;
    this.jPanel.add(lblFecha, gbcLblFecha);

    this.tfFecha = new JFormattedTextField(TurneraValidator.dateFormatter());
    this.tfFecha.setFocusLostBehavior(JFormattedTextField.PERSIST);
    this.tfFecha.addFocusListener(new DateFocusEvent());
    GridBagConstraints gbcTfFecha = new GridBagConstraints();
    gbcTfFecha.fill = GridBagConstraints.HORIZONTAL;
    gbcTfFecha.insets = new Insets(0, 0, 5, 0);
    gbcTfFecha.gridx = 1;
    gbcTfFecha.gridy = 3;
    this.jPanel.add(this.tfFecha, gbcTfFecha);

    JLabel lblHorario = new JLabel("Horario:");
    GridBagConstraints gbcLblHorario = new GridBagConstraints();
    gbcLblHorario.fill = GridBagConstraints.VERTICAL;
    gbcLblHorario.anchor = GridBagConstraints.EAST;
    gbcLblHorario.insets = new Insets(0, 0, 5, 5);
    gbcLblHorario.gridx = 0;
    gbcLblHorario.gridy = 4;
    this.jPanel.add(lblHorario, gbcLblHorario);

    this.horarioComboBox = new JComboBox<>();
    GridBagConstraints gbcHorarioComboBox = new GridBagConstraints();
    gbcHorarioComboBox.insets = new Insets(0, 0, 5, 0);
    gbcHorarioComboBox.fill = GridBagConstraints.HORIZONTAL;
    gbcHorarioComboBox.gridx = 1;
    gbcHorarioComboBox.gridy = 4;
    this.jPanel.add(this.horarioComboBox, gbcHorarioComboBox);
  }

  public JComboBox<PacientePojo> getPacienteComboBox() {
    return this.pacienteComboBox;
  }

  public JComboBox<MedicoPojo> getMedicoComboBox() {
    return this.medicoComboBox;
  }

  public JComboBox<ConsultorioPojo> getConsultorioComboBox() {
    return this.consultorioComboBox;
  }

  public JFormattedTextField getTfFecha() {
    return this.tfFecha;
  }

  public JComboBox<HorarioPojo> getHorarioComboBox() {
    return this.horarioComboBox;
  }
}
