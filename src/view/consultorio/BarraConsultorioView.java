package view.consultorio;

import javax.swing.JMenuItem;
import view.GeneralBarraView;
import view.constants.ViewLabels;

/**
 * Clase que administra los componentes visuales de la barra de menú de la ventana de consultorios.
 */
public class BarraConsultorioView extends GeneralBarraView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private JMenuItem consultorioLbl;

  /**
   * Crea la barra del la vista de consultorios.
   */
  public BarraConsultorioView() {
    super();
  }

  /**
   * Inicializa los componentes visuales.
   */
  public void initView() {
    super.initLabels();

    this.consultorioLbl = new JMenuItem(ViewLabels.CONSULTORIO.getLabel());
    super.getNuevoLbl().add(this.consultorioLbl);
  }

  public JMenuItem getConsultorioLbl() {
    return this.consultorioLbl;
  }
}
