package view.reporte;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import view.CargaView;
import view.util.DateFocusEvent;
import view.validator.TurneraValidator;

/**
 * Clase que administra la vista de la carga de reportes.
 */
public class CargaReporteView extends CargaView {

  private final JFormattedTextField tfFechaDesde;
  private final JFormattedTextField tfFechaHasta;

  /**
   * Crea la vista de la carga de reporte.
   */
  public CargaReporteView() {
    setBounds(100, 100, 334, 283);
    setTitle("Rango de fechas");

    this.jPanel = new JPanel();
    this.jPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(this.jPanel, BorderLayout.NORTH);
    GridBagLayout gblPanel = new GridBagLayout();
    gblPanel.columnWidths = new int[]{0, 0, 0};
    gblPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
    gblPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
    gblPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    this.jPanel.setLayout(gblPanel);

    JLabel lblFechaDesde = new JLabel("Fecha desde:");
    GridBagConstraints gbcLblFechaDesde = new GridBagConstraints();
    gbcLblFechaDesde.anchor = GridBagConstraints.EAST;
    gbcLblFechaDesde.insets = new Insets(0, 0, 5, 5);
    gbcLblFechaDesde.gridx = 0;
    gbcLblFechaDesde.gridy = 0;
    this.jPanel.add(lblFechaDesde, gbcLblFechaDesde);

    this.tfFechaDesde = new JFormattedTextField(TurneraValidator.dateFormatter());
    this.tfFechaDesde.setFocusLostBehavior(JFormattedTextField.PERSIST);
    this.tfFechaDesde.addFocusListener(new DateFocusEvent());
    GridBagConstraints gbcTfFechaDesde = new GridBagConstraints();
    gbcTfFechaDesde.insets = new Insets(0, 0, 5, 0);
    gbcTfFechaDesde.fill = GridBagConstraints.HORIZONTAL;
    gbcTfFechaDesde.gridx = 1;
    gbcTfFechaDesde.gridy = 0;
    this.jPanel.add(this.tfFechaDesde, gbcTfFechaDesde);

    JLabel lblFechaHasta = new JLabel("Fecha hasta:");
    GridBagConstraints gbcLblFechaHasta = new GridBagConstraints();
    gbcLblFechaHasta.anchor = GridBagConstraints.EAST;
    gbcLblFechaHasta.insets = new Insets(0, 0, 5, 5);
    gbcLblFechaHasta.gridx = 0;
    gbcLblFechaHasta.gridy = 1;
    this.jPanel.add(lblFechaHasta, gbcLblFechaHasta);

    this.tfFechaHasta = new JFormattedTextField(TurneraValidator.dateFormatter());
    this.tfFechaHasta.setFocusLostBehavior(JFormattedTextField.PERSIST);
    this.tfFechaHasta.addFocusListener(new DateFocusEvent());
    GridBagConstraints gbcTfFechaHasta = new GridBagConstraints();
    gbcTfFechaHasta.insets = new Insets(0, 0, 5, 0);
    gbcTfFechaHasta.fill = GridBagConstraints.HORIZONTAL;
    gbcTfFechaHasta.gridx = 1;
    gbcTfFechaHasta.gridy = 1;
    this.jPanel.add(this.tfFechaHasta, gbcTfFechaHasta);
  }

  public JTextField getTfFechaDesde() {
    return this.tfFechaDesde;
  }

  public JTextField getTfFechaHasta() {
    return this.tfFechaHasta;
  }
}
