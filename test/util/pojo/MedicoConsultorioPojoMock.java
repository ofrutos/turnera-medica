package util.pojo;

import java.text.ParseException;
import java.util.Map;
import java.util.TreeMap;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoConsultorioPojoKey;
import model.util.DateUtils;

public class MedicoConsultorioPojoMock {

  private static MedicoConsultorioPojo getMedicoConsultorioUno() throws ParseException {
    final MedicoConsultorioPojo medicoConsultorioPojo = new MedicoConsultorioPojo();
    medicoConsultorioPojo.getMedicoPojo().setUsuarioId(9995);
    medicoConsultorioPojo.getConsultorioPojo().setConsultorioId(103);
    medicoConsultorioPojo.setFechaInicio(DateUtils.getDateFormat().parse("01/01/2010"));
    medicoConsultorioPojo.setFechaFin(DateUtils.getDateFormat().parse(""));
//    medicoConsultorioPojo.getHorarioPojo().setHorarioId(23);

    return medicoConsultorioPojo;
  }

  public static Map<MedicoConsultorioPojoKey, MedicoConsultorioPojo> getMedicoConsultorioDos()
      throws ParseException {
    final Map<MedicoConsultorioPojoKey, MedicoConsultorioPojo> medicoConsultorioPojos = new TreeMap<>();

    final MedicoConsultorioPojo medicoConsultorioPojo = getMedicoConsultorioUno();

    medicoConsultorioPojos
        .put(medicoConsultorioPojo.getMedicoConsultorioPojoKey(), medicoConsultorioPojo);

    return medicoConsultorioPojos;
  }

  public static Map<MedicoConsultorioPojoKey, MedicoConsultorioPojo> getAllMedicoConsultorio()
      throws ParseException {
    final Map<MedicoConsultorioPojoKey, MedicoConsultorioPojo> medicoConsultorioPojos = new TreeMap<>();

    final MedicoConsultorioPojo medicoConsultorioPojo1 = new MedicoConsultorioPojo();
    medicoConsultorioPojo1.getMedicoPojo().setUsuarioId(9996);
    medicoConsultorioPojo1.getConsultorioPojo().setConsultorioId(101);
    medicoConsultorioPojo1.setFechaInicio(DateUtils.getDateFormat().parse("01/01/2010"));
    medicoConsultorioPojo1.setFechaFin(DateUtils.getDateFormat().parse(""));
//    medicoConsultorioPojo1.getHorarioPojo().setHorarioId(17);

    medicoConsultorioPojos
        .put(medicoConsultorioPojo1.getMedicoConsultorioPojoKey(), medicoConsultorioPojo1);

    final MedicoConsultorioPojo medicoConsultorioPojo2 = new MedicoConsultorioPojo();
    medicoConsultorioPojo2.getMedicoPojo().setUsuarioId(9996);
    medicoConsultorioPojo2.getConsultorioPojo().setConsultorioId(101);
    medicoConsultorioPojo2.setFechaInicio(DateUtils.getDateFormat().parse("01/01/2010"));
    medicoConsultorioPojo2.setFechaFin(DateUtils.getDateFormat().parse(""));
//    medicoConsultorioPojo2.setHorarioId(18);

    medicoConsultorioPojos
        .put(medicoConsultorioPojo2.getMedicoConsultorioPojoKey(), medicoConsultorioPojo2);

    final MedicoConsultorioPojo medicoConsultorioPojo3 = getMedicoConsultorioUno();

    medicoConsultorioPojos
        .put(medicoConsultorioPojo3.getMedicoConsultorioPojoKey(), medicoConsultorioPojo3);

    return medicoConsultorioPojos;
  }
}
