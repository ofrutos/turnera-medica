package model.pojo;

import java.util.Date;
import model.constants.usuario.TipoDocumento;

/**
 * Clase que administra la información de un paciente.
 */
public class PacientePojo extends UsuarioPojo {

  /**
   * Crea un paciente.
   */
  public PacientePojo() {
    super();
  }

  /**
   * Crea un paciente a partir de un usuario.
   *
   * @param usuarioPojo POJO del usuario.
   */
  public PacientePojo(final UsuarioPojo usuarioPojo) {
    super(usuarioPojo);
  }

  /**
   * Crea un paciente.
   *
   * @param usuarioId       ID del usuario.
   * @param tipoDocumento   Tipo de documento.
   * @param numeroDocumento Numero del documento.
   * @param nombre          Nombre del usuario.
   * @param apellido        Apellido del usuario.
   * @param fechaNacimiento Fecha de nacimiento.
   * @param tipoUsuario     Tipo de usuario.
   */
  public PacientePojo(final Integer usuarioId,
      final TipoDocumento tipoDocumento,
      final String numeroDocumento,
      final String nombre,
      final String apellido,
      final Date fechaNacimiento,
      final TipoUsuario tipoUsuario) {
    super(usuarioId,
        tipoDocumento,
        numeroDocumento,
        nombre, apellido,
        fechaNacimiento,
        tipoUsuario);
  }

  @Override
  public boolean equals(Object o) {
    return super.equals(o);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }
}
