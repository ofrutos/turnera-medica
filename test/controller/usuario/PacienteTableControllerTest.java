package controller.usuario;

import static org.junit.Assert.assertEquals;

import controller.usuario.paciente.CargaPacienteController;
import controller.usuario.paciente.PacienteTableController;
import java.util.Arrays;
import java.util.List;
import javax.swing.JTable;
import model.dao.implementation.sqlite.PacienteDaoImplementation;
import org.junit.Before;
import org.junit.Test;
import service.PacienteService;
import service.implementation.PacienteServiceImplementation;
import view.message.MessageView;
import view.usuario.paciente.CargaPacienteView;

public class PacienteTableControllerTest {

  private PacienteTableController pacienteTableController;

  @Before
  public void setUp() {
    final PacienteService pacienteService = new PacienteServiceImplementation(
        new PacienteDaoImplementation());
    final CargaPacienteController cargaPacienteController = new CargaPacienteController(
        new CargaPacienteView(),
        new MessageView(),
        pacienteService);

    this.pacienteTableController = new PacienteTableController(new JTable(),
        pacienteService,
        cargaPacienteController,
        null,
        new MessageView());

    this.pacienteTableController.initController();
  }

  @Test
  public void getColumnCountTest() {
    assertEquals(6, this.pacienteTableController.getColumnCount());
  }

  @Test
  public void getColumnNameTest() {
    List<String> nombreColumnas = Arrays
        .asList("Usuario Id",
            "Tipo Documento",
            "Numero Documento",
            "Nombre",
            "Apellido",
            "Fecha Nacimiento");

    for (int columna = 0; columna < 6; columna++) {
      assertEquals(nombreColumnas.get(columna),
          this.pacienteTableController.getColumnName(columna));
    }
  }
}
