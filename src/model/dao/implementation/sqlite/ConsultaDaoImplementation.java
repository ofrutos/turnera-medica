package model.dao.implementation.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import model.constants.table.ConsultaTableConstants;
import model.dao.ConsultaDao;
import model.dao.exception.DAOException;
import model.dao.utils.LabelUtils;
import model.jdbc.DBManager;
import model.pojo.ConsultaPojo;
import model.pojo.ConsultorioPojo;
import model.pojo.HorarioPojo;
import model.pojo.MedicoPojo;
import model.pojo.PacientePojo;
import model.util.DateUtils;
import org.apache.commons.lang3.EnumUtils;

public class ConsultaDaoImplementation implements ConsultaDao {

  /**
   * Obtiene una consulta.
   */
  private static final String SELECT_ONE_CONSULTA = "SELECT C.Consulta_Id, C.Paciente_Id Paciente, P.Nombre, P.Apellido, C.Medico_Id Medico, M.Nombre, M.Apellido, C.Consultorio_Id Consultorio, CC.Nombre, C.Fecha, C.Horario_Id Horario, H.Hora_Inicio, H.Hora_Fin, C.Precio_Base, C.Descuento, C.Precio_Final FROM Consulta C, Usuario P, Usuario M, Consultorio CC, Horarios H WHERE C.Paciente_Id = P.Usuario_Id AND C.Medico_Id = M.Usuario_Id AND C.Consultorio_Id = CC.Consultorio_Id AND C.Horario_Id = H.Horario_Id AND Consulta_Id = ?";
  /**
   * Declaración de SELECT (All) de la tabla consulta.
   */
  private static final String SELECT_ALL_CONSULTAS = "SELECT C.Consulta_Id, C.Paciente_Id Paciente, P.Nombre, P.Apellido, C.Medico_Id Medico, M.Nombre, M.Apellido, C.Consultorio_Id Consultorio, CC.Nombre, C.Fecha, C.Horario_Id Horario, H.Hora_Inicio, H.Hora_Fin, C.Precio_Base, C.Descuento, C.Precio_Final FROM Consulta C, Usuario P, Usuario M, Consultorio CC, Horarios H WHERE C.Paciente_Id = P.Usuario_Id AND C.Medico_Id = M.Usuario_Id AND C.Consultorio_Id = CC.Consultorio_Id AND C.Horario_Id = H.Horario_Id";
  /**
   * Declaración de INSERT de la tabla consulta.
   */
  private static final String INSERT_CONSULTA = "INSERT INTO Consulta (Paciente_Id, Medico_Id, Consultorio_Id, Fecha, Horario_Id, Precio_Base, Descuento, Precio_Final) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

  /**
   * Declaración de UPDATE de la tabla consulta.
   */
  private static final String UPDATE_CONSULTA = "UPDATE Consulta SET Paciente_Id = ?, Medico_Id = ?, Consultorio_Id = ?, Fecha = ?, Horario_Id = ?, Precio_Base = ?, Descuento = ?, Precio_Final = ? WHERE Consulta_Id = ?";

  /**
   * Declaración de DELETE de la tabla consulta.
   */
  private static final String DELETE_CONSULTA = "DELETE FROM Consulta WHERE Consulta_Id = ?";
  /**
   * Conexión con la base de datos.
   */
  private Connection connection;

  public void createConsulta(final ConsultaPojo consultaPojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(INSERT_CONSULTA);
      statement.setInt(1, consultaPojo.getPacientePojo().getUsuarioId());
      statement.setInt(2, consultaPojo.getMedicoPojo().getUsuarioId());
      statement.setInt(3, consultaPojo.getConsultorioPojo().getConsultorioId());
      statement.setString(4, DateUtils.parseDate(consultaPojo.getFecha()));
      statement.setInt(5, consultaPojo.getHorarioPojo().getHorarioId());
      statement.setString(6, consultaPojo.getPrecioBase());
      statement.setInt(7, consultaPojo.getDescuento());
      statement.setString(8, consultaPojo.getPrecioFinal());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows created.");
      }

      ResultSet resultSet = statement.getGeneratedKeys();

      if (resultSet != null && resultSet.next()) {
        consultaPojo.setConsultaId(resultSet.getInt(1));
      }
      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public void updateConsulta(final ConsultaPojo consultaPojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(UPDATE_CONSULTA);
      statement.setInt(1, consultaPojo.getPacientePojo().getUsuarioId());
      statement.setInt(2, consultaPojo.getMedicoPojo().getUsuarioId());
      statement.setInt(3, consultaPojo.getConsultorioPojo().getConsultorioId());
      statement.setString(4, DateUtils.parseDate(consultaPojo.getFecha()));
      statement.setInt(5, consultaPojo.getHorarioPojo().getHorarioId());
      statement.setString(6, consultaPojo.getPrecioBase());
      statement.setInt(7, consultaPojo.getDescuento());
      statement.setString(8, consultaPojo.getPrecioFinal());
      statement.setInt(9, consultaPojo.getConsultaId());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows updated.");
      }

      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public void deleteConsulta(final ConsultaPojo consultaPojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(DELETE_CONSULTA);
      statement.setInt(1, consultaPojo.getConsultaId());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows deleted.");
      }

      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public Map<Integer, ConsultaPojo> getAllConsultas() throws DAOException {
    Map<Integer, ConsultaPojo> consultaPojos = new HashMap<>();
    this.connection = DBManager.connect();
    try {
      ResultSet resultSet = this.connection.createStatement().executeQuery(SELECT_ALL_CONSULTAS);
      while (resultSet.next()) {
        final ConsultaPojo consultaPojo = this.generateConsultaPojo(resultSet);

        consultaPojos.put(resultSet.getInt(1), consultaPojo);
      }
    } catch (final SQLException | ParseException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
    return consultaPojos;
  }

  @Override
  public ConsultaPojo getConsulta(final int consultaId) {
    this.connection = DBManager.connect();
    ConsultaPojo consultaPojo = null;
    try {
      PreparedStatement preparedStatement = this.connection
          .prepareStatement(SELECT_ONE_CONSULTA);
      preparedStatement.setInt(1, consultaId);

      ResultSet resultSet = preparedStatement.executeQuery();
      if (resultSet.next()) {
        consultaPojo = this.generateConsultaPojo(resultSet);
      }
    } catch (final SQLException | ParseException e) {
      DBManager.handleRollback(this.connection);
    } finally {
      DBManager.handleClose(this.connection);
    }
    return consultaPojo;
  }

  @Override
  public List<String> getConsultaNombreColumna() {
    this.connection = DBManager.connect();
    List<String> cabecera = new ArrayList<>();
    try {
      PreparedStatement statement = this.connection.prepareStatement(SELECT_ONE_CONSULTA);
      // Solo nos interesa conseguir la cabecera.
      statement.setInt(1, 0);

      ResultSetMetaData meta = statement.getMetaData();
      int cantidadColumnas = statement.getMetaData().getColumnCount();

      for (int columna = 1; columna <= cantidadColumnas; columna++) {
        cabecera.add(meta.getColumnName(columna));
      }

      cabecera = cabecera.stream()
          .filter(this::filtrarColumnasPrivadas)
          .map(LabelUtils::normalizarLabel)
          .collect(Collectors.toList());
    } catch (
        final SQLException e) {
      DBManager.handleRollback(this.connection);
    } finally {
      DBManager.handleClose(this.connection);
    }
    return cabecera;
  }

  /**
   * Indica las columnas que no deben ser visualizadas.
   *
   * @param columna Nombre de la columna.
   * @return <b>true</b> si la columna debe ser visualizada.<br><b>false</b> si no debe ser
   * visualizada.
   */
  public boolean filtrarColumnasPrivadas(final String columna) {
    return EnumUtils.getEnumIgnoreCase(ConsultaTableConstants.class, columna) != null;
  }

  private ConsultaPojo generateConsultaPojo(final ResultSet resultSet)
      throws SQLException, ParseException {
    final PacientePojo pacientePojo = new PacientePojo();
    pacientePojo.setUsuarioId(resultSet.getInt(2));
    pacientePojo.setNombre(resultSet.getString(3));
    pacientePojo.setApellido(resultSet.getString(4));

    final MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(resultSet.getInt(5));
    medicoPojo.setNombre(resultSet.getString(6));
    medicoPojo.setApellido(resultSet.getString(7));
    medicoPojo.setPrecioConsulta(resultSet.getString(14));

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();
    consultorioPojo.setConsultorioId(resultSet.getInt(8));
    consultorioPojo.setNombre(resultSet.getString(9));

    final HorarioPojo horarioPojo = new HorarioPojo(
        resultSet.getInt(11),
        resultSet.getString(12),
        resultSet.getString(13)
    );

    return new ConsultaPojo(resultSet.getInt(1),
        pacientePojo,
        medicoPojo,
        consultorioPojo,
        DateUtils.getDateFormat().parse(resultSet.getString(10)),
        horarioPojo,
        resultSet.getString(14),
        resultSet.getInt(15),
        resultSet.getString(16));
  }
}
