package controller.medicoconsultorio;

import controller.GeneralTableController;
import controller.InitializableController;
import controller.ViewableController;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTable;
import model.constants.table.MedicoConsultorioTableConstants;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoPojo;
import model.util.DateUtils;
import service.MedicoConsultorioService;
import service.excpetion.ServiceException;
import view.message.MessageView;

/**
 * Clase que administra el controlador de la tabla de consultorios del medico.
 */
public class MedicoConsultorioTableController extends GeneralTableController implements
    InitializableController, ViewableController {

  private final JTable tablaMedicoConsultorio;
  /**
   * Servicio de Consultorios de Medicos.
   */
  private final MedicoConsultorioService medicoConsultorioService;
  private final CargaMedicoConsultorioController cargaMedicoConsultorioController;
  private MedicoPojo medicoPojo;

  /**
   * Crea el controlador de la tabla de consultorios del medico.
   *
   * @param tablaMedicoConsultorio           Tabla de consultorios del medico.
   * @param medicoConsultorioService         Servicio de Consultorios de Medicos.
   * @param cargaMedicoConsultorioController Controlador de la carga de consultorio del medico.
   * @param messageView                      Vista de mensajes.
   */
  public MedicoConsultorioTableController(final JTable tablaMedicoConsultorio,
      final MedicoConsultorioService medicoConsultorioService,
      final CargaMedicoConsultorioController cargaMedicoConsultorioController,
      final MessageView messageView) {
    super(tablaMedicoConsultorio,
        messageView);

    this.tablaMedicoConsultorio = tablaMedicoConsultorio;
    this.medicoConsultorioService = medicoConsultorioService;
    this.cargaMedicoConsultorioController = cargaMedicoConsultorioController;
  }

  public void initController() {
    super.setNombreColumnas(this.medicoConsultorioService.obtenerNombreColumnas());

    super.initController();

    this.tablaMedicoConsultorio.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(final MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2) {
          final JTable jTable = (JTable) mouseEvent.getSource();
          cargaMedicoConsultorioController
              .setMedicoConsultorioPojo(getMedicoConsultorioPojoInRow(jTable.getSelectedRow()));
          cargaMedicoConsultorioController.initView();
        }
      }
    });

    this.cargaMedicoConsultorioController.getCargaMedicoConsultorioView()
        .addWindowListener(new WindowAdapter() {
          @Override
          public void windowClosed(final WindowEvent windowEvent) {
            try {
              setFilas(medicoConsultorioService.obtenerConsultorios(medicoPojo).toArray());
            } catch (final ServiceException e) {
              handleExceptionMessage(e, "los consultorios del medico");
            }
            fireTableDataChanged();
            windowEvent.getWindow().dispose();
          }
        });
  }

  @Override
  public void initView() {
    try {
      super.setFilas(this.medicoConsultorioService.obtenerConsultorios(this.medicoPojo).toArray());
    } catch (final ServiceException e) {
      handleExceptionMessage(e, "los consultorios del medico");
    }
  }

  @Override
  public Object getValueAt(final int rowIndex, final int columnIndex) {
    return getValueAtColumn(getMedicoConsultorioPojoInRow(rowIndex), columnIndex + 1);
  }

  /**
   * Obtiene el valor de cada columna a partir de su posición en la BD.
   *
   * @param medicoConsultorioPojo Consultorio del medico actual.
   * @param column                Numero de la columna.
   * @return Valor en la columna indicada.
   */
  private Object getValueAtColumn(final MedicoConsultorioPojo medicoConsultorioPojo,
      final int column) {
    if (column == MedicoConsultorioTableConstants.CONSULTORIO.getPosition()) {
      return medicoConsultorioPojo.getConsultorioPojo().toString();
    }
    if (column == MedicoConsultorioTableConstants.FECHA_INICIO.getPosition()) {
      return DateUtils.parseDate(medicoConsultorioPojo.getFechaInicio());
    }
    if (column == MedicoConsultorioTableConstants.FECHA_FIN.getPosition()) {
      return DateUtils.parseDate(medicoConsultorioPojo.getFechaFin());
    }
    if (column == MedicoConsultorioTableConstants.HORARIO.getPosition()) {
      return medicoConsultorioPojo.getHorarioPojo().toString();
    }
    return null;
  }

  private MedicoConsultorioPojo getMedicoConsultorioPojoInRow(final int row) {
    return (MedicoConsultorioPojo) super.getFilas()[row];
  }

  /**
   * Establece el medico al cual visualizar sus consultorios.
   *
   * @param medicoPojo POJO del medico.
   */
  public void setMedicoPojo(final MedicoPojo medicoPojo) {
    this.medicoPojo = medicoPojo;
  }
}
