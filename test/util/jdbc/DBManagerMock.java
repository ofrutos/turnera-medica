package util.jdbc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import model.jdbc.SQLiteConstants;
import org.apache.ibatis.jdbc.ScriptRunner;

public class DBManagerMock {

  private static final String DB_DRIVER = SQLiteConstants.DB_DRIVER.getConstant();
  private static final String DB_PATH = SQLiteConstants.DB_PATH_TEST.getConstant();
  private static final String CMD_INITIALIZE = "./test/util/jdbc/loader/";
  private static final String CMD_DELETE = "./test/util/jdbc/deleter/";

  public static Connection connect() {
    Connection connection = null;
    try {
      connection = DriverManager.getConnection(DB_DRIVER + ":" + DB_PATH);
      connection.setAutoCommit(false);
    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(0);
    }
    return connection;
  }

  /**
   * Crea pacientes en la base de datos.
   */
  public static void crearPacientes() {
    crearDatos("pacientes.sql");
  }

  /**
   * Borra pacientes en la base de datos.
   */
  public static void borrarPacientes() {
    borrarDatos("pacientes.sql");
  }

  /**
   * Crea medicos en la base de datos.
   */
  public static void crearMedicos() {
    crearDatos("medicos.sql");
  }

  /**
   * Borra medicos en la base de datos.
   */
  public static void borrarMedicos() {
    borrarDatos("medicos.sql");
  }

  /**
   * Crea consultorios en la base de datos.
   */
  public static void crearConsultorios() {
    crearDatos("consultorios.sql");
  }

  /**
   * Borra consultorios en la base de datos.
   */
  public static void borrarConsultorios() {
    borrarDatos("consultorios.sql");
  }

  /**
   * Crea consultorios en la base de datos.
   */
  public static void crearConsultas() {
    crearDatos("consultas.sql");
  }

  /**
   * Borra consultorios en la base de datos.
   */
  public static void borrarConsultas() {
    borrarDatos("consultas.sql");
  }

  /**
   * Crea los consultorios de los medicos en la base de datos.
   */
  public static void crearMedicoConsultorios() {
    crearDatos("medico-consultorios.sql");
  }

  /**
   * Borra los consultorios de los medicos en la base de datos.
   */
  public static void borrarMedicoConsultorios() {
    borrarDatos("medico-consultorios.sql");
  }

  /**
   * Crea datos a partir de un script.
   *
   * @param archivo Script con los datos a generar.
   */
  private static void crearDatos(final String archivo) {
    executeScripts(CMD_INITIALIZE + archivo);
  }

  /**
   * Elimina datos a partir de un script.
   *
   * @param archivo Script con los datos a eliminar.
   */
  private static void borrarDatos(final String archivo) {
    executeScripts(CMD_DELETE + archivo);
  }

  /**
   * Ejecuta un script contra la base de datos.
   *
   * @param fileName Path y nombre del script a generar.
   */
  private static void executeScripts(final String fileName) {
    try {
      Class.forName("org.sqlite.JDBC");
    } catch (ClassNotFoundException e) {
      System.exit(10);
    }

    Connection connection = null;
    try {
      connection = DriverManager.getConnection(DB_DRIVER + ":" + DB_PATH);
      ScriptRunner scriptRunner = new ScriptRunner(connection);
      scriptRunner.setEscapeProcessing(false);
      Reader reader = new BufferedReader(new FileReader(fileName));
      scriptRunner.runScript(reader);
    } catch (IOException | SQLException e) {
      System.out.println(e.getMessage());
    } finally {
      try {
        if (connection != null) {
          connection.close();
        }
      } catch (SQLException e) {
        e.printStackTrace();
      }
    }
  }
}
