package controller.medicoconsultorio;

import controller.CargaController;
import java.text.ParseException;
import java.util.List;
import model.pojo.ConsultorioPojo;
import model.pojo.HorarioPojo;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoPojo;
import model.util.DateUtils;
import service.ConsultorioService;
import service.HorarioService;
import service.MedicoConsultorioService;
import service.excpetion.ServiceException;
import view.constants.ViewLabels;
import view.medicoconsultorio.CargaMedicoConsultorioView;
import view.message.MessageView;

/**
 * Clase que administra la carga de consultorios de medicos.
 */
public class CargaMedicoConsultorioController extends CargaController<MedicoConsultorioPojo> {

  private final CargaMedicoConsultorioView cargaMedicoConsultorioView;
  private final MedicoConsultorioService medicoConsultorioService;
  private final ConsultorioService consultorioService;
  private final HorarioService horarioService;
  private MedicoConsultorioPojo medicoConsultorioPojo;
  private MedicoPojo medicoPojo;

  /**
   * Crea el controlador de la carga del consultorio del medico.
   *
   * @param cargaMedicoConsultorioView Vista de la carga de consultorios de medicos.
   * @param messageView                Vista de mensaje.
   * @param medicoConsultorioService   Servicio de Consultorios de Medicos.
   * @param consultorioService         Servicio de Consultorios.
   * @param horarioService             Servicio de Horarios.
   */
  public CargaMedicoConsultorioController(
      final CargaMedicoConsultorioView cargaMedicoConsultorioView,
      final MessageView messageView,
      final MedicoConsultorioService medicoConsultorioService,
      final ConsultorioService consultorioService,
      final HorarioService horarioService) {
    super(cargaMedicoConsultorioView,
        messageView);

    this.cargaMedicoConsultorioView = cargaMedicoConsultorioView;
    this.medicoConsultorioService = medicoConsultorioService;
    this.consultorioService = consultorioService;
    this.horarioService = horarioService;
  }

  @Override
  protected boolean controlarCarga() {
    if (this.cargaMedicoConsultorioView.getTfFechaInicio().getText().isEmpty()) {
      super.showMessage(ViewLabels.MENSAJE_ADVERTENCIA.getLabel(),
          "Se debe indicar la fecha de inicio de uso del consultorio.");
      return false;
    }
    return true;
  }

  @Override
  public void initView() {
    final List<ConsultorioPojo> consultorioPojos;
    this.cargaMedicoConsultorioView.getConsultorioComboBox().removeAllItems();
    try {
      consultorioPojos = this.consultorioService.obtenerTodosConsultorio();
      for (ConsultorioPojo consultorioPojo : consultorioPojos) {
        this.cargaMedicoConsultorioView.getConsultorioComboBox().addItem(consultorioPojo);
      }
    } catch (final ServiceException e) {
      super.showMessage(ViewLabels.MENSAJE_ERROR.getLabel(), e.getMessage());
    }

    final List<HorarioPojo> horarioPojos;
    this.cargaMedicoConsultorioView.getHorarioComboBox().removeAllItems();
    try {
      horarioPojos = this.horarioService.obtenerTodosHorarios();
      for (HorarioPojo horarioPojo : horarioPojos) {
        this.cargaMedicoConsultorioView.getHorarioComboBox().addItem(horarioPojo);
      }
    } catch (final ServiceException e) {
      super.showMessage(ViewLabels.MENSAJE_ERROR.getLabel(), e.getMessage());
    }

    if (this.medicoConsultorioPojo != null) {
      this.cargaMedicoConsultorioView.getConsultorioComboBox().getModel()
          .setSelectedItem(this.medicoConsultorioPojo.getConsultorioPojo());
      this.cargaMedicoConsultorioView.getTfFechaInicio()
          .setText(DateUtils.parseDate(this.medicoConsultorioPojo.getFechaInicio()));
      this.cargaMedicoConsultorioView.getTfFechaFin()
          .setText(DateUtils.parseDate(this.medicoConsultorioPojo.getFechaFin()));
      this.cargaMedicoConsultorioView.getHorarioComboBox().getModel()
          .setSelectedItem(this.medicoConsultorioPojo.getHorarioPojo());

      this.setCargaEnabled(false);
    } else {
      this.cargaMedicoConsultorioView.getTfFechaInicio().setText(null);
      this.cargaMedicoConsultorioView.getTfFechaFin().setText(null);

      this.setCargaEnabled(true);
    }

    this.cargaMedicoConsultorioView.setVisible(true);
  }

  /**
   * Indica si la carga es permitida o solo se puede actualizar la fecha de fin.
   *
   * @param enabled Si se encuentra permitido cargar todos los datos.
   */
  private void setCargaEnabled(final boolean enabled) {
    this.cargaMedicoConsultorioView.getConsultorioComboBox().setEnabled(enabled);
    this.cargaMedicoConsultorioView.getTfFechaInicio().setEditable(enabled);
    this.cargaMedicoConsultorioView.getHorarioComboBox().setEnabled(enabled);
  }

  /**
   * Actualiza la información del consultorio. Si el consultorio no existe en el sistema, lo
   * genera.
   */
  @Override
  protected void update() {
    if (this.controlarCarga()) {
      try {
        String mensaje;
        if (this.medicoConsultorioPojo == null) {
          this.medicoConsultorioService.crearConsultorio(this.getDataInView());
          mensaje = "Se ha creado el consultorio exitosamente.";
        } else {
          this.medicoConsultorioService.actualizarConsultorio(this.getDataInView());
          mensaje = "Se ha actualizado el consultorio exitosamente.";
        }
        super.showMessage(ViewLabels.MENSAJE_EXITO.getLabel(), mensaje);
        this.cargaMedicoConsultorioView.dispose();
      } catch (final ServiceException | ParseException e) {
        super.showMessage(ViewLabels.MENSAJE_ERROR.getLabel(), e.getMessage());
      }
    }
  }

  @Override
  protected MedicoConsultorioPojo getDataInView() throws ParseException {
    return new MedicoConsultorioPojo(
        this.medicoConsultorioPojo != null ? this.medicoConsultorioPojo
            .getMedicoConsultorioPojoKey().getUsuarioId() : this.medicoPojo.getUsuarioId(),
        ((ConsultorioPojo) this.cargaMedicoConsultorioView.getConsultorioComboBox().getModel()
            .getSelectedItem()).getConsultorioId(),
        ((ConsultorioPojo) this.cargaMedicoConsultorioView.getConsultorioComboBox().getModel()
            .getSelectedItem()).getNombre(),
        DateUtils.getDateFormat()
            .parse(this.cargaMedicoConsultorioView.getTfFechaInicio().getText()),
        DateUtils.getDateFormat().parse(this.cargaMedicoConsultorioView.getTfFechaFin().getText()),
        ((HorarioPojo) this.cargaMedicoConsultorioView.getHorarioComboBox().getModel()
            .getSelectedItem()).getHorarioId(),
        ((HorarioPojo) this.cargaMedicoConsultorioView.getHorarioComboBox().getModel()
            .getSelectedItem()).getHoraInicio(),
        ((HorarioPojo) this.cargaMedicoConsultorioView.getHorarioComboBox().getModel()
            .getSelectedItem()).getHoraFin());
  }

  /**
   * Establece el consultorio del medico a visualizar en la carga.
   *
   * @param medicoConsultorioPojo POJO del consultorio del medico.
   */
  public void setMedicoConsultorioPojo(final MedicoConsultorioPojo medicoConsultorioPojo) {
    this.medicoConsultorioPojo = medicoConsultorioPojo;
  }

  /**
   * Devuelve la vista de la carga del consultorio del paciente.
   *
   * @return Vista de la carga del consultorio del paciente.
   */
  public CargaMedicoConsultorioView getCargaMedicoConsultorioView() {
    return this.cargaMedicoConsultorioView;
  }

  /**
   * Establece el medico actual.
   *
   * @param medicoPojo POJO del medico.
   */
  public void setMedicoPojo(final MedicoPojo medicoPojo) {
    this.medicoPojo = medicoPojo;
  }
}
