package controller.consulta;

import controller.GeneralTableController;
import controller.InitializableController;
import controller.ViewableController;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTable;
import model.constants.table.ConsultaTableConstants;
import model.pojo.ConsultaPojo;
import model.pojo.MedicoPojo;
import model.pojo.PacientePojo;
import model.util.DateUtils;
import service.ConsultaService;
import service.excpetion.ServiceException;
import view.message.MessageView;

public class ConsultaTableController extends GeneralTableController implements
    InitializableController, ViewableController {

  private final JTable tablaConsulta;
  private final ConsultaService consultaService;
  private final CargaConsultaController cargaConsultaController;
  private MedicoPojo medicoPojo;
  private PacientePojo pacientePojo;

  /**
   * Crea un controlador general de las tablas.
   *
   * @param tablaConsulta           Tabla de consultas.
   * @param consultaService         Servicio de Consultas.
   * @param cargaConsultaController Controlador de la carga de consultas.
   * @param messageView             Vista de mensajes.
   */
  public ConsultaTableController(final JTable tablaConsulta,
      final ConsultaService consultaService,
      final CargaConsultaController cargaConsultaController,
      final MessageView messageView) {
    super(tablaConsulta,
        messageView);

    this.tablaConsulta = tablaConsulta;
    this.consultaService = consultaService;
    this.cargaConsultaController = cargaConsultaController;
  }

  public void initController() {
    super.setNombreColumnas(this.consultaService.obtenerNombreColumnasConsulta());

    super.initController();

    this.tablaConsulta.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(final MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2) {
          final JTable jTable = (JTable) mouseEvent.getSource();
          cargaConsultaController.setConsultaPojo(getConsultaPojoInRow(jTable.getSelectedRow()));
          cargaConsultaController.initView();
        }
      }
    });

    this.cargaConsultaController.getCargaConsultaView().addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(final WindowEvent windowEvent) {
        try {
          setFilas(consultaService.obtenerConsultas().toArray());
        } catch (final ServiceException e) {
          handleExceptionMessage(e, "las consultas");
        }
        fireTableDataChanged();
        windowEvent.getWindow().dispose();
      }
    });
  }

  @Override
  public Object getValueAt(final int rowIndex, final int columnIndex) {
    return getValueAtColumn(getConsultaPojoInRow(rowIndex), columnIndex + 1);
  }

  /**
   * Obtiene el valor de cada columna a partir de su posición en la BD.
   *
   * @param consulta Consulta actual.
   * @param column   Numero de la columna.
   * @return Valor en la columna indicada.
   */
  private Object getValueAtColumn(final ConsultaPojo consulta, final int column) {
    if (column == ConsultaTableConstants.CONSULTA_ID.getPosition()) {
      return consulta.getConsultaId();
    }
    if (column == ConsultaTableConstants.PACIENTE.getPosition()) {
      return consulta.getPacientePojo().toString();
    }
    if (column == ConsultaTableConstants.MEDICO.getPosition()) {
      return consulta.getMedicoPojo().toString()
          .substring(0, consulta.getMedicoPojo().toString().indexOf("("));
    }
    if (column == ConsultaTableConstants.CONSULTORIO.getPosition()) {
      return consulta.getConsultorioPojo().toString();
    }
    if (column == ConsultaTableConstants.FECHA.getPosition()) {
      return DateUtils.parseDate(consulta.getFecha());
    }
    if (column == ConsultaTableConstants.HORARIO.getPosition()) {
      return consulta.getHorarioPojo().toString();
    }
    if (column == ConsultaTableConstants.PRECIO_FINAL.getPosition()) {
      return consulta.getPrecioFinal();
    }

    return null;
  }

  private ConsultaPojo getConsultaPojoInRow(final int row) {
    return (ConsultaPojo) super.getFilas()[row];
  }

  /**
   * Establece el medico de las consultas a visualizar.
   *
   * @param medicoPojo POJO del medico.
   */
  public void setMedicoPojo(final MedicoPojo medicoPojo) {
    this.medicoPojo = medicoPojo;
  }

  /**
   * Establece el paciente de las consultas a visualizar.
   *
   * @param pacientePojo POJO del paciente.
   */
  public void setPacientePojo(final PacientePojo pacientePojo) {
    this.pacientePojo = pacientePojo;
  }

  @Override
  public void initView() {
    if (this.medicoPojo != null) {
      try {
        super.setFilas(this.consultaService.obtenerConsultas(this.medicoPojo).toArray());
      } catch (final ServiceException e) {
        super.handleExceptionMessage(e,
            "las consultas del medico " + this.medicoPojo.getUsuarioId());
      }
    } else {
      if (this.pacientePojo != null) {
        try {
          super.setFilas(this.consultaService.obtenerConsultas(this.pacientePojo).toArray());
        } catch (final ServiceException e) {
          super.handleExceptionMessage(e,
              "las consultas del paciente " + this.pacientePojo.getUsuarioId());
        }
      } else {
        try {
          super.setFilas(this.consultaService.obtenerConsultas().toArray());
        } catch (final ServiceException e) {
          super.handleExceptionMessage(e, "las consultas");
        }
      }
      fireTableDataChanged();
    }
  }
}
