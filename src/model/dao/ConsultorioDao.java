package model.dao;

import java.util.List;
import java.util.Map;
import model.dao.exception.DAOException;
import model.pojo.ConsultorioPojo;

/**
 * Interfaz que administra todas las declaraciones para el DAO de un consultorio.
 */
public interface ConsultorioDao {

  /**
   * Crea un consultorio.
   *
   * @param consultorioPojo Consultorio a crear.
   * @throws DAOException Si no se pudo crear un consultorio u ocurrió un error en la base de
   *                      datos.
   */
  void createConsultorio(final ConsultorioPojo consultorioPojo) throws DAOException;

  /**
   * Actualiza un consultorio.
   *
   * @param consultorioPojo Consultorio a actualizar.
   * @throws DAOException Si no se pudo actualizar un consultorio u ocurrió un error en la base de
   *                      datos.
   */
  void updateConsultorio(final ConsultorioPojo consultorioPojo) throws DAOException;

  /**
   * Elimina un consultorio.
   *
   * @param consultorioPojo Consultorio a eliminar.
   * @throws DAOException Si no se pudo eliminar un consultorio u ocurrió un error en la base de
   *                      datos.
   */
  void deleteConsultorio(final ConsultorioPojo consultorioPojo) throws DAOException;

  /**
   * Lista todos los consultorios. <br>
   *
   * @return Todos los consultorios. <br>
   */
  Map<Integer, ConsultorioPojo> getAllConsultorio() throws DAOException;

  /**
   * Obtiene los labels de la tabla de los consultorios.
   *
   * @return Label de la tabla de los consultorios.
   */
  List<String> getConsultorioNombreColumna();

  /**
   * Obtiene un consultorio.
   *
   * @param consultorioId Id del consultorio.
   * @return Consultorio.
   */
  ConsultorioPojo getConsultorio(final Integer consultorioId) throws DAOException;
}
