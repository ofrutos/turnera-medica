package controller.reporte;

import controller.GeneralBarraController;
import controller.InitializableController;
import view.reporte.BarraReporteView;

/**
 * Clase que administra el controlador de la barra de la vista de reportes.
 */
public class BarraReporteController extends GeneralBarraController implements
    InitializableController {

  private final BarraReporteView barraReporteView;
  private final CargaReporteController cargaReporteController;

  /**
   * Crea el controlador de la vista de reportes.
   *
   * @param barraReporteView Vista de la barra de la ventana de reportes.
   */
  public BarraReporteController(final BarraReporteView barraReporteView,
      final CargaReporteController cargaReporteController) {
    super(barraReporteView);

    this.barraReporteView = barraReporteView;
    this.cargaReporteController = cargaReporteController;
  }

  public void initController() {
    super.initController();

    this.barraReporteView.getRangoLbl().addActionListener(e -> crearRango());
  }

  /**
   * Carga el formulario para crear un nuevo rango.
   */
  private void crearRango() {
    this.cargaReporteController.initView();
  }
}
