package model.dao;

import java.util.List;
import model.dao.exception.DAOException;
import model.pojo.HorarioPojo;

/**
 * Interfaz que administra todas las declaraciones para el DAO de los horarios.
 */
public interface HorarioDao {

  /**
   * Lista todos los horarios.
   *
   * @return Todos los horarios.
   * @throws DAOException Si ocurre un error al obtener los horarios.
   */
  List<HorarioPojo> getAllHorarios() throws DAOException;
}
