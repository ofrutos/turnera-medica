package controller.consultorio;

import controller.GeneralBarraController;
import controller.InitializableController;
import view.consultorio.BarraConsultorioView;

/**
 * Clase que administra el controlador de la barra de menú de la ventana principal de consultorios.
 */
public class BarraConsultorioController extends GeneralBarraController implements
    InitializableController {

  private final BarraConsultorioView barraConsultorioView;
  private final CargaConsultorioController cargaConsultorioController;

  /**
   * Crea el controlador de la barra de menu de la ventana principal de consultorios.
   *
   * @param barraConsultorioView       Vista general de los consultorios.
   * @param cargaConsultorioController Controlador de la carga de consultorios.
   */
  public BarraConsultorioController(final BarraConsultorioView barraConsultorioView,
      final CargaConsultorioController cargaConsultorioController) {
    super(barraConsultorioView);
    this.barraConsultorioView = barraConsultorioView;
    this.cargaConsultorioController = cargaConsultorioController;
  }

  public void initController() {
    super.initController();
    this.barraConsultorioView.getConsultorioLbl().addActionListener(e -> this.crearConsultorio());
  }

  /**
   * Carga el formulario para crear un nuevo consultorio. <br>
   */
  private void crearConsultorio() {
    this.cargaConsultorioController.setConsultorioPojo(null);
    this.cargaConsultorioController.initView();
  }
}
