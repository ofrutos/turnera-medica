package view.consultorio;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import view.CargaView;

/**
 * Clase que administra la carga de consultorio.
 */
public class CargaConsultorioView extends CargaView {

  private final JTextField tfNombre;

  /**
   * Crea una vista para la carga de consultorio.
   */
  public CargaConsultorioView() {
    setBounds(100, 100, 334, 283);

    JPanel jPanel = new JPanel();
    jPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(jPanel, BorderLayout.NORTH);
    GridBagLayout gblPanel = new GridBagLayout();
    gblPanel.columnWidths = new int[]{0, 0, 0};
    gblPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
    gblPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
    gblPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    jPanel.setLayout(gblPanel);

    JLabel lblNombre = new JLabel("Nombre:");
    GridBagConstraints gbcLblNombre = new GridBagConstraints();
    gbcLblNombre.fill = GridBagConstraints.VERTICAL;
    gbcLblNombre.anchor = GridBagConstraints.EAST;
    gbcLblNombre.insets = new Insets(0, 0, 5, 5);
    gbcLblNombre.gridx = 0;
    gbcLblNombre.gridy = 0;
    jPanel.add(lblNombre, gbcLblNombre);

    tfNombre = new JTextField(null);
    GridBagConstraints gbcTfNombre = new GridBagConstraints();
    gbcTfNombre.fill = GridBagConstraints.HORIZONTAL;
    gbcTfNombre.insets = new Insets(0, 0, 5, 0);
    gbcTfNombre.gridx = 1;
    gbcTfNombre.gridy = 0;
    jPanel.add(tfNombre, gbcTfNombre);
  }

  public JTextField getTfNombre() {
    return tfNombre;
  }
}
