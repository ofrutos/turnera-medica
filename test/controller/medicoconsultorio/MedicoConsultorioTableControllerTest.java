package controller.medicoconsultorio;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import javax.swing.JTable;
import model.dao.implementation.sqlite.ConsultorioDaoImplementation;
import model.dao.implementation.sqlite.HorarioDaoImplementation;
import model.dao.implementation.sqlite.MedicoConsultorioDaoImplementation;
import model.pojo.MedicoPojo;
import org.junit.Before;
import org.junit.Test;
import service.ConsultorioService;
import service.HorarioService;
import service.MedicoConsultorioService;
import service.implementation.ConsultorioServiceImplementation;
import service.implementation.HorarioServiceImplementation;
import service.implementation.MedicoConsultorioServiceImplementation;
import view.medicoconsultorio.CargaMedicoConsultorioView;
import view.message.MessageView;

public class MedicoConsultorioTableControllerTest {

  private MedicoConsultorioTableController medicoConsultorioTableController;

  @Before
  public void setUp() {
    MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(1);

    final MedicoConsultorioService medicoConsultorioService = new MedicoConsultorioServiceImplementation(
        new MedicoConsultorioDaoImplementation());
    final ConsultorioService consultorioService = new ConsultorioServiceImplementation(
        new ConsultorioDaoImplementation());
    final HorarioService horarioService = new HorarioServiceImplementation(
        new HorarioDaoImplementation());
    final CargaMedicoConsultorioController cargaMedicoConsultorioController = new CargaMedicoConsultorioController(
        new CargaMedicoConsultorioView(),
        new MessageView(),
        medicoConsultorioService,
        consultorioService,
        horarioService);
    this.medicoConsultorioTableController = new MedicoConsultorioTableController(new JTable(),
        medicoConsultorioService,
        cargaMedicoConsultorioController,
        new MessageView());

    this.medicoConsultorioTableController.initController();
  }

  @Test
  public void getColumnCountTest() {
    assertEquals(6, this.medicoConsultorioTableController.getColumnCount());
  }

  @Test
  public void getColumnNameTest() {
    List<String> nombreColumnas = Arrays
        .asList("Consultorio Id",
            "Nombre",
            "Fecha Inicio",
            "Fecha Fin",
            "Hora Inicio",
            "Hora Fin");

    for (int columna = 0; columna < nombreColumnas.size(); columna++) {
      assertEquals(nombreColumnas.get(columna),
          this.medicoConsultorioTableController.getColumnName(columna));
    }
  }
}
