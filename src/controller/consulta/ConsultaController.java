package controller.consulta;

import controller.InitializableController;
import controller.ViewableController;
import model.pojo.MedicoPojo;
import model.pojo.PacientePojo;
import service.ConsultaService;
import service.MedicoConsultorioService;
import service.MedicoService;
import service.PacienteService;
import view.constants.ViewLabels;
import view.consulta.CargaConsultaView;
import view.consulta.ConsultaView;
import view.message.MessageView;

/**
 * Clase que administra los controladores de la vista de Consultas.
 */
public class ConsultaController implements InitializableController, ViewableController {

  private final ConsultaView consultaView;
  private final ConsultaTableController consultaTableController;
  private final BarraConsultaController barraConsultaController;
  private final CargaConsultaController cargaConsultaController;
  private MedicoPojo medicoPojo;
  private PacientePojo pacientePojo;

  public ConsultaController(final ConsultaView consultaView,
      final MessageView messageView,
      final ConsultaService consultaService,
      final PacienteService pacienteService,
      final MedicoService medicoService,
      final MedicoConsultorioService medicoConsultorioService) {
    this.consultaView = consultaView;
    this.cargaConsultaController = new CargaConsultaController(new CargaConsultaView(),
        messageView,
        consultaService,
        pacienteService,
        medicoService,
        medicoConsultorioService);
    this.barraConsultaController = new BarraConsultaController(
        this.consultaView.getConsultaBarraView(),
        this.cargaConsultaController);
    this.consultaTableController = new ConsultaTableController(this.consultaView.getTablaConsulta(),
        consultaService,
        this.cargaConsultaController,
        messageView);
  }

  @Override
  public void initController() {
    consultaTableController.initController();
    barraConsultaController.initController();
    cargaConsultaController.initController();
  }

  @Override
  public void initView() {
    if (this.medicoPojo != null) {
      this.consultaView.getjFrame()
          .setTitle("Consultas del Medico " + this.medicoPojo.getUsuarioId());
      this.consultaView.getConsultaBarraView().getConsultaLbl().setEnabled(false);
    } else {
      if (this.pacientePojo != null) {
        this.consultaView.getjFrame()
            .setTitle("Consultas del Paciente " + this.pacientePojo.getUsuarioId());
        this.consultaView.getConsultaBarraView().getConsultaLbl().setEnabled(false);
      } else {
        this.consultaView.getjFrame()
            .setTitle(ViewLabels.TITULO_CONSULTAS.getLabel());
        this.consultaView.getConsultaBarraView().getConsultaLbl().setEnabled(true);
      }
    }
    this.consultaTableController.setMedicoPojo(this.medicoPojo);
    this.consultaTableController.setPacientePojo(this.pacientePojo);
    this.consultaTableController.initView();
    this.consultaView.getjFrame().setVisible(true);
  }

  /**
   * Establece el medico de las consultas a visualizar.
   *
   * @param medicoPojo POJO del medico.
   */
  public void setMedicoPojo(final MedicoPojo medicoPojo) {
    this.medicoPojo = medicoPojo;
  }

  /**
   * Establece el paciente de las consultas a visualizar.
   *
   * @param pacientePojo POJO del paciente.
   */
  public void setPacientePojo(final PacientePojo pacientePojo) {
    this.pacientePojo = pacientePojo;
  }
}
