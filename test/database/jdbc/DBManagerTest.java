package database.jdbc;

import java.sql.Connection;
import model.jdbc.DBManager;
import org.junit.Test;

public class DBManagerTest {
	private Connection connection;

	@Test
	public void testConnection() {
		System.out.println("Inicio de test");
		try {
			this.connection = DBManager.connect();
			System.out.println("Conexión exitosa");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DBManager.handleClose(this.connection);
			System.out.println("Fin de conexión");
		}
		System.out.println("Fin de test");
	}

}
