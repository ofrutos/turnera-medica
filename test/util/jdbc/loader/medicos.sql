INSERT INTO main.Usuario
(Usuario_Id,
 Tipo_Documento,
 Numero_Documento,
 Nombre,
 Apellido,
 Fecha_Nacimiento,
 Tipo_Usuario,
 Precio_Consulta)
VALUES (9996,
        'DNI',
        '24567190',
        'Miguel',
        'Saenz',
        '01/01/1990',
        'MEDICO',
        640);

INSERT INTO main.Usuario
(Usuario_Id,
 Tipo_Documento,
 Numero_Documento,
 Nombre,
 Apellido,
 Fecha_Nacimiento,
 Tipo_Usuario,
 Precio_Consulta)
VALUES (9995,
        'DNI',
        '25513987',
        'Jorge',
        'Velazquez',
        '20/01/1995',
        'MEDICO',
        720);

INSERT INTO main.Usuario
(Usuario_Id,
 Tipo_Documento,
 Numero_Documento,
 Nombre,
 Apellido,
 Fecha_Nacimiento,
 Tipo_Usuario,
 Precio_Consulta)
VALUES (9994,
        'DNI',
        '12567123',
        'Laura',
        'Mlaker',
        '10/10/1985',
        'MEDICO',
        850);
