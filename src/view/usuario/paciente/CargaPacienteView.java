package view.usuario.paciente;

import view.usuario.CargaUsuarioView;

/**
 * Clase que administra la carga de paciente.
 */
public class CargaPacienteView extends CargaUsuarioView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  /**
   * Crea la vista de un paciente.
   */
  public CargaPacienteView() {
    super();
  }
}
