package view.message;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import view.constants.ViewLabels;

/**
 * Clase que administra una ventana de mensaje.
 */
public class MessageView extends JDialog {

  private static final long serialVersionUID = 1L;
  private final JLabel lblMensaje;

  /**
   * Carga la ventana con el mensaje a mostrar.
   */
  public MessageView() {
    setResizable(false);
    setModal(true);
    setAlwaysOnTop(true);

    setBounds(100, 100, 323, 148);

    getContentPane().setLayout(new BorderLayout());
    JPanel contentPanel = new JPanel();
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    contentPanel.setLayout(null);

    this.lblMensaje = new JLabel();
    this.lblMensaje.setVerticalAlignment(SwingConstants.CENTER);
    this.lblMensaje.setHorizontalAlignment(SwingConstants.CENTER);
    this.lblMensaje.setBounds(10, 5, 297, 81);
    contentPanel.add(this.lblMensaje);

    JPanel buttonPane = new JPanel();
    getContentPane().add(buttonPane, BorderLayout.SOUTH);
    buttonPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

    JButton btnOk = new JButton(ViewLabels.OK.getLabel());
    btnOk.addActionListener(e -> dispose());
    buttonPane.add(btnOk);
    getRootPane().setDefaultButton(btnOk);
  }

  /**
   * Establece el mensaje a mostrar.
   *
   * @param mensaje Mensaje.
   */
  public void setMensaje(final String mensaje) {
    this.lblMensaje.setText("<html><center>" + mensaje + "</center></html>");
  }
}
