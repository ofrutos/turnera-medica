package view.medicoconsultorio;

import javax.swing.JMenuItem;
import view.GeneralBarraView;
import view.constants.ViewLabels;

/**
 * Clase que administra los componentes visuales de la barra de menú de la ventana de los medicos.
 */
public class BarraMedicoConsultorioView extends GeneralBarraView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private JMenuItem consultorioLbl;

  /**
   * Crea la barra de la vista de consultorios del medico.
   */
  public BarraMedicoConsultorioView() {
    super();
  }

  /**
   * Inicializa los componentes visuales.
   */
  public void initView() {
    super.initLabels();

    this.consultorioLbl = new JMenuItem(ViewLabels.CONSULTORIO.getLabel());
    super.getNuevoLbl().add(this.consultorioLbl);
  }

  /**
   * Devuelve el label de Consultorios.
   *
   * @return Label de consultorios.
   */
  public JMenuItem getConsultorioLbl() {
    return this.consultorioLbl;
  }
}
