package view.usuario.medico;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Clase que administra el popup menu de la tabla de medicos.
 */
public class MedicoTableControllerPopupMenuView extends JPopupMenu {

  private final JMenuItem consultoriosJMenuItem;
  private final JMenuItem consultasJMenuItem;


  /**
   * Crea el popup menu de la tabla de medicos.
   */
  public MedicoTableControllerPopupMenuView() {
    super();

    this.consultoriosJMenuItem = new JMenuItem("Consultorios");
    add(this.consultoriosJMenuItem);

    this.consultasJMenuItem = new JMenuItem("Consultas");
    add(this.consultasJMenuItem);
  }

  public JMenuItem getConsultoriosJMenuItem() {
    return consultoriosJMenuItem;
  }

  public JMenuItem getConsultasJMenuItem() {
    return consultasJMenuItem;
  }
}
