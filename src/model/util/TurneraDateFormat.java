package model.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Clase que administra el formato a utilizar en la aplicación.
 */
public class TurneraDateFormat extends SimpleDateFormat {

  /**
   * Crea un formato de fecha de la aplicación.
   *
   * @param dateFormat Formato de fecha.
   */
  public TurneraDateFormat(final String dateFormat) {
    super(dateFormat);
  }

  @Override
  public Date parse(String text) {
    try {
      return super.parse(text);
    } catch (final ParseException e) {
      return DateUtils.getMaximumDate();
    }
  }
}
