package model.constants.table;

public enum ConsultorioTableConstants {
  CONSULTORIO_ID(1),
  NOMBRE(2);

  private final Integer position;

  ConsultorioTableConstants(Integer position) {
    this.position = position;
  }

  public Integer getPosition() {
    return this.position;
  }
}
