package controller.medicoconsultorio;

import controller.InitializableController;
import controller.ViewableController;
import model.pojo.MedicoPojo;
import service.ConsultorioService;
import service.HorarioService;
import service.MedicoConsultorioService;
import view.medicoconsultorio.CargaMedicoConsultorioView;
import view.medicoconsultorio.MedicoConsultorioView;
import view.message.MessageView;

/**
 * Clase que administra los controladores de la ventana principal de Consultorios de Medicos.
 */
public class MedicoConsultorioController implements InitializableController, ViewableController {

  private final MedicoConsultorioView medicoConsultorioView;
  private final MedicoConsultorioTableController medicoConsultorioTableController;
  private final CargaMedicoConsultorioController cargaMedicoConsultorioController;
  private final BarraMedicoConsultorioController barraMedicoConsultorioController;
  private MedicoPojo medicoPojo;

  /**
   * Crea un controlador general de consultorios de medicos.
   *
   * @param medicoConsultorioView    Vista general de los consultorios de un medico.
   * @param messageView              Vista de mensajes.
   * @param medicoConsultorioService Servicio de Consultorios de Medicos.
   * @param consultorioService       Servicio de Consultorios.
   * @param horarioService           Servicio de Horarios.
   */
  public MedicoConsultorioController(final MedicoConsultorioView medicoConsultorioView,
      final MessageView messageView,
      final MedicoConsultorioService medicoConsultorioService,
      final ConsultorioService consultorioService,
      final HorarioService horarioService) {
    this.medicoConsultorioView = medicoConsultorioView;
    this.cargaMedicoConsultorioController = new CargaMedicoConsultorioController(
        new CargaMedicoConsultorioView(),
        messageView,
        medicoConsultorioService,
        consultorioService,
        horarioService);
    this.barraMedicoConsultorioController = new BarraMedicoConsultorioController(
        this.medicoConsultorioView.getBarraMedicoView(),
        this.cargaMedicoConsultorioController,
        this);
    this.medicoConsultorioTableController = new MedicoConsultorioTableController(
        this.medicoConsultorioView.getTablaMedicoConsultorio(),
        medicoConsultorioService,
        this.cargaMedicoConsultorioController,
        messageView);
  }

  @Override
  public void initController() {
    this.barraMedicoConsultorioController.initController();
    this.cargaMedicoConsultorioController.initController();
    this.medicoConsultorioTableController.initController();
  }

  @Override
  public void initView() {
    this.medicoConsultorioTableController.setMedicoPojo(this.medicoPojo);
    this.medicoConsultorioTableController.initView();

    this.medicoConsultorioView.getjFrame()
        .setTitle("Consultorios del Medico " + this.medicoPojo.getUsuarioId());
    this.medicoConsultorioView.getjFrame().setVisible(true);
  }

  /**
   * Devuelve el medico actual.
   *
   * @return POJO del medico.
   */
  public MedicoPojo getMedicoPojo() {
    return this.medicoPojo;
  }

  /**
   * Establece el Medico a visualizar sus Consultorios.
   *
   * @param medicoPojo POJO del medico.
   */
  public void setMedicoPojo(final MedicoPojo medicoPojo) {
    this.medicoPojo = medicoPojo;
  }
}
