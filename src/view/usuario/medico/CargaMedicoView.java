package view.usuario.medico;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;
import view.usuario.CargaUsuarioView;
import view.validator.TurneraValidator;

/**
 * Clase que administra la carga de Medicos.
 */
public class CargaMedicoView extends CargaUsuarioView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;

  private final JFormattedTextField tfPrecioConsulta;

  /**
   * Crea una vista para la carga de medicos.
   */
  public CargaMedicoView() {
    super();

    JLabel lblPrecioConsulta = new JLabel("Precio consulta:");
    GridBagConstraints gbcLblConsultaPaciente = new GridBagConstraints();
    gbcLblConsultaPaciente.fill = GridBagConstraints.VERTICAL;
    gbcLblConsultaPaciente.anchor = GridBagConstraints.EAST;
    gbcLblConsultaPaciente.insets = new Insets(0, 0, 5, 5);
    gbcLblConsultaPaciente.gridx = 0;
    gbcLblConsultaPaciente.gridy = 7;
    this.jPanel.add(lblPrecioConsulta, gbcLblConsultaPaciente);

    tfPrecioConsulta = new JFormattedTextField(TurneraValidator.numberFormatter(0));
    GridBagConstraints gbcTfConsultaPaciente = new GridBagConstraints();
    gbcTfConsultaPaciente.fill = GridBagConstraints.HORIZONTAL;
    gbcTfConsultaPaciente.insets = new Insets(0, 0, 5, 0);
    gbcTfConsultaPaciente.gridx = 1;
    gbcTfConsultaPaciente.gridy = 7;
    this.jPanel.add(tfPrecioConsulta, gbcTfConsultaPaciente);
  }

  public JTextField getTfPrecioConsulta() {
    return tfPrecioConsulta;
  }
}
