package view.consulta;

import javax.swing.JTable;
import view.GeneralView;

/**
 * Clase de la vista general de consultas.
 */
public class ConsultaView extends GeneralView {

  private final BarraConsultaView consultaBarraView;

  /**
   * Crea la vista general de consultas.
   *
   * @param barraConsultaView Barra de la vista de consultas.
   */
  public ConsultaView(final BarraConsultaView barraConsultaView) {
    super(barraConsultaView);

    this.consultaBarraView = barraConsultaView;
    this.consultaBarraView.initView();
  }

  /**
   * Devuelve la barra de la vista de las consultas.
   *
   * @return Barra de la vista de las consultas.
   */
  public BarraConsultaView getConsultaBarraView() {
    return this.consultaBarraView;
  }

  /**
   * Devuelve la tabla de las consultas.
   *
   * @return Tabla de las consultas.
   */
  public JTable getTablaConsulta() {
    return super.getjTable();
  }
}
