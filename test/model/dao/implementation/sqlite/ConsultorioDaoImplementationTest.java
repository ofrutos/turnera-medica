package model.dao.implementation.sqlite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;
import static util.jdbc.DBManagerMock.borrarConsultorios;
import static util.jdbc.DBManagerMock.crearConsultorios;
import static util.pojo.ConsultorioPojoMock.getConsultorioDos;
import static util.pojo.ConsultorioPojoMock.getConsultorioTres;
import static util.pojo.ConsultorioPojoMock.getConsultorioUno;

import java.sql.Connection;
import model.jdbc.DBManager;
import model.pojo.ConsultorioPojo;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import model.dao.exception.DAOException;
import util.jdbc.DBManagerMock;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DBManager.class)
public class ConsultorioDaoImplementationTest {

  private ConsultorioDaoImplementation consultorioDaoImplementation;

  @BeforeClass
  public static void preTests() {
    spy(DBManager.class);
    when(DBManager.connect())
        .thenAnswer((Answer<Connection>) invocationOnMock -> DBManagerMock.connect());

    crearConsultorios();
  }

  @Before
  public void setup() {
    this.consultorioDaoImplementation = new ConsultorioDaoImplementation();
  }

  @AfterClass
  public static void postTests() {
    borrarConsultorios();
  }

  @Test
  public void consultarConsultorio_Ok() {
    final ConsultorioPojo expectedConsultorioPojo = getConsultorioUno();
    final ConsultorioPojo actualConsultorioPojo = this.consultorioDaoImplementation
        .getConsultorio(101);

    assertEquals(expectedConsultorioPojo, actualConsultorioPojo);
  }

  @Test
  public void consultarConsultorio_Error() {
    final ConsultorioPojo actualConsultorioPojo = this.consultorioDaoImplementation
        .getConsultorio(100);
    assertNull(actualConsultorioPojo);
  }

  @Test
  public void crearConsultorio_Ok() throws DAOException {
    final ConsultorioPojo expectedConsultorioPojo = new ConsultorioPojo();
    expectedConsultorioPojo.setNombre("Consultorio 4");

    this.consultorioDaoImplementation.createConsultorio(expectedConsultorioPojo);
    final ConsultorioPojo actualConsultorioPojo = this.consultorioDaoImplementation
        .getConsultorio(expectedConsultorioPojo.getConsultorioId());
    assertEquals(expectedConsultorioPojo, actualConsultorioPojo);
  }

  @Test
  public void actualizarConsultorio_Ok() throws DAOException {
    final ConsultorioPojo expectedConsultorioPojo = getConsultorioDos();
    expectedConsultorioPojo.setNombre("Consultorio 2.1");

    final ConsultorioPojo currentConsultorioPojo = this.consultorioDaoImplementation
        .getConsultorio(102);
    currentConsultorioPojo.setNombre("Consultorio 2.1");

    this.consultorioDaoImplementation.updateConsultorio(currentConsultorioPojo);
    final ConsultorioPojo actualConsultorioPojo = this.consultorioDaoImplementation
        .getConsultorio(expectedConsultorioPojo.getConsultorioId());

    assertEquals(expectedConsultorioPojo, actualConsultorioPojo);
  }

  @Test(expected = DAOException.class)
  public void actualizarConsultorio_Error() throws DAOException {
    final ConsultorioPojo expectedConsultorioPojo = new ConsultorioPojo();
    expectedConsultorioPojo.setConsultorioId(1001);
    expectedConsultorioPojo.setNombre("Consultorio 3");

    this.consultorioDaoImplementation.updateConsultorio(expectedConsultorioPojo);
  }

  @Test
  public void eliminarConsultorio_Ok() throws DAOException {
    final ConsultorioPojo expectedConsultorioPojo = getConsultorioTres();

    this.consultorioDaoImplementation.deleteConsultorio(expectedConsultorioPojo);
    final ConsultorioPojo actualConsultorioPojo = this.consultorioDaoImplementation
        .getConsultorio(103);

    assertNull(actualConsultorioPojo);
  }

  @Test(expected = DAOException.class)
  public void eliminarConsultorio_Error() throws DAOException {
    final ConsultorioPojo expectedConsultorioPojo = new ConsultorioPojo();
    expectedConsultorioPojo.setConsultorioId(1001);
    expectedConsultorioPojo.setNombre("Consultorio 3");

    this.consultorioDaoImplementation.deleteConsultorio(expectedConsultorioPojo);
  }
}

