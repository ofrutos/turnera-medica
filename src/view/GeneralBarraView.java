package view;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import view.constants.ViewLabels;

/**
 * Clase que administra la barra general del sistema.
 */
public class GeneralBarraView extends JMenuBar {

  private JMenuItem salirLbl;
  private JMenu nuevoLbl;

  /**
   * Crea la barra general del programa.
   */
  public GeneralBarraView() {
    super();
  }

  /**
   * Inicializa los labels de la barra.
   */
  public void initLabels() {
    JMenu archivo = new JMenu(ViewLabels.ITEM_ARCHIVO.getLabel());
    this.salirLbl = new JMenuItem(ViewLabels.SALIR.getLabel());

    this.nuevoLbl = new JMenu(ViewLabels.ITEM_NUEVO.getLabel());

    archivo.add(this.nuevoLbl);
    archivo.add(this.salirLbl);

    this.add(archivo);
  }

  /**
   * Devuelve el label de salir del sistema.
   *
   * @return Label de "Salir".
   */
  public JMenuItem getSalirLbl() {
    return this.salirLbl;
  }

  /**
   * Devuelve el label de "Nuevo"
   *
   * @return Label de "Nuevo".
   */
  public JMenuItem getNuevoLbl() {
    return this.nuevoLbl;
  }
}
