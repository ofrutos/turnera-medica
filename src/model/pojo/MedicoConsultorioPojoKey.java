package model.pojo;

import java.util.Date;
import java.util.Objects;

/**
 * Clase que administra el POJO de la clave del consultorio de un medico.
 */
public class MedicoConsultorioPojoKey implements Comparable<MedicoConsultorioPojoKey> {

  private Integer usuarioId;
  private Integer consultorioId;
  private Date fechaInicio;
  private Integer horarioId;

  /**
   * Crea una clave de consultorio de medico vacía.
   */
  public MedicoConsultorioPojoKey() {
  }

  /**
   * Crea la clave del consultorio de un medico.
   *
   * @param usuarioId     ID del usuario.
   * @param consultorioId ID del consultorio.
   * @param fechaInicio   Fecha de inicio de atención.
   * @param horarioId     ID del horario.
   */
  public MedicoConsultorioPojoKey(final Integer usuarioId,
      final Integer consultorioId,
      final Date fechaInicio,
      final Integer horarioId) {
    this.usuarioId = usuarioId;
    this.consultorioId = consultorioId;
    this.fechaInicio = fechaInicio;
    this.horarioId = horarioId;
  }

  public Integer getUsuarioId() {
    return usuarioId;
  }

  public void setUsuarioId(Integer usuarioId) {
    this.usuarioId = usuarioId;
  }

  public Integer getConsultorioId() {
    return consultorioId;
  }

  public void setConsultorioId(Integer consultorioId) {
    this.consultorioId = consultorioId;
  }

  public Date getFechaInicio() {
    return fechaInicio;
  }

  public void setFechaInicio(Date fechaInicio) {
    this.fechaInicio = fechaInicio;
  }

  public Integer getHorarioId() {
    return horarioId;
  }

  public void setHorarioId(Integer horarioId) {
    this.horarioId = horarioId;
  }

  @Override
  public int compareTo(final MedicoConsultorioPojoKey medicoConsultorioPojoKey) {
    int comparedUsuarioId = this.usuarioId.compareTo(medicoConsultorioPojoKey.getUsuarioId());

    if (comparedUsuarioId == 0) {
      int comparedConsultorioId = this.consultorioId
          .compareTo(medicoConsultorioPojoKey.getConsultorioId());

      if (comparedConsultorioId == 0) {
        int comparedFechaInicio = this.fechaInicio
            .compareTo(medicoConsultorioPojoKey.getFechaInicio());

        if (comparedFechaInicio == 0) {
          return this.horarioId.compareTo(medicoConsultorioPojoKey.getHorarioId());
        } else {
          return comparedFechaInicio;
        }
      } else {
        return comparedUsuarioId;
      }
    } else {
      return comparedUsuarioId;
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MedicoConsultorioPojoKey that = (MedicoConsultorioPojoKey) o;
    return usuarioId.equals(that.usuarioId) &&
        consultorioId.equals(that.consultorioId) &&
        fechaInicio.equals(that.fechaInicio) &&
        horarioId.equals(that.horarioId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(usuarioId, consultorioId, fechaInicio, horarioId);
  }
}
