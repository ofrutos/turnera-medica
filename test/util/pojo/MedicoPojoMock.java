package util.pojo;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import model.constants.usuario.TipoDocumento;
import model.pojo.MedicoPojo;
import model.pojo.TipoUsuario;
import model.util.DateUtils;

public class MedicoPojoMock {

  public static MedicoPojo getMedicoUno() throws ParseException {
    final MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(9996);
    medicoPojo.setTipoDocumento(TipoDocumento.DNI);
    medicoPojo.setNumeroDocumento("24567190");
    medicoPojo.setNombre("Miguel");
    medicoPojo.setApellido("Saenz");
    medicoPojo.setFechaNacimiento(DateUtils.getDateFormat().parse("01/01/1990"));
    medicoPojo.setTipoUsuario(TipoUsuario.MEDICO);
    medicoPojo.setPrecioConsulta("640");
    return medicoPojo;
  }

  public static MedicoPojo getMedicoDos() throws ParseException {
    final MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(9995);
    medicoPojo.setTipoDocumento(TipoDocumento.DNI);
    medicoPojo.setNumeroDocumento("25513987");
    medicoPojo.setNombre("Jorge");
    medicoPojo.setApellido("Velazquez");
    medicoPojo.setFechaNacimiento(DateUtils.getDateFormat().parse("20/01/1995"));
    medicoPojo.setTipoUsuario(TipoUsuario.MEDICO);
    medicoPojo.setPrecioConsulta("720");
    return medicoPojo;
  }

  public static MedicoPojo getMedicoTres() throws ParseException {
    final MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(9994);
    medicoPojo.setTipoDocumento(TipoDocumento.DNI);
    medicoPojo.setNumeroDocumento("12567123");
    medicoPojo.setNombre("Laura");
    medicoPojo.setApellido("Mlaker");
    medicoPojo.setFechaNacimiento(DateUtils.getDateFormat().parse("10/10/1985"));
    medicoPojo.setTipoUsuario(TipoUsuario.MEDICO);
    medicoPojo.setPrecioConsulta("850");
    return medicoPojo;
  }

  public static Map<Integer, MedicoPojo> getAllMedicos() throws ParseException {
    final Map<Integer, MedicoPojo> medicoPojos = new HashMap<>();

    medicoPojos.put(9996, getMedicoUno());
    medicoPojos.put(9995, getMedicoDos());

    return medicoPojos;
  }
}
