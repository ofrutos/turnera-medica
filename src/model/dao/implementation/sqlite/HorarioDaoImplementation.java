package model.dao.implementation.sqlite;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.dao.HorarioDao;
import model.dao.exception.DAOException;
import model.jdbc.DBManager;
import model.pojo.HorarioPojo;

/**
 * Clase que administra los DAO para los horarios en una base SQLite. <br>
 */
public class HorarioDaoImplementation implements HorarioDao {

  /**
   * Declaración de SELECT (All) de la tabla de horarios. <br>
   */
  private static final String SELECT_ALL_HORARIOS = "SELECT * FROM Horarios";

  /**
   * Conexión con la base de datos. <br>
   */
  private Connection connection;

  public List<HorarioPojo> getAllHorarios() throws DAOException {
    List<HorarioPojo> horariosPojos = new ArrayList<>();
    this.connection = DBManager.connect();
    try {
      ResultSet resultSet = this.connection.createStatement().executeQuery(SELECT_ALL_HORARIOS);
      while (resultSet.next()) {
        horariosPojos.add(new HorarioPojo(resultSet.getInt(1),
            resultSet.getString(2),
            resultSet.getString(3)));
      }
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
    return horariosPojos;
  }
}
