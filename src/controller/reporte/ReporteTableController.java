package controller.reporte;

import controller.GeneralTableController;
import controller.InitializableController;
import controller.ViewableController;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTable;
import model.constants.table.ReporteTableConstants;
import model.pojo.MedicoPojo;
import service.ConsultaService;
import service.excpetion.ServiceException;
import view.message.MessageView;

/**
 * Clase que administra el controlador de la tabla de reportes.
 */
public class ReporteTableController extends GeneralTableController implements
    InitializableController, ViewableController {

  private final ConsultaService consultaService;
  private final CargaReporteController cargaReporteController;

  /**
   * Crea el controlador de la tabla de reportes.
   *
   * @param tablaReporte           Tabla de reportes
   * @param consultaService        Servicio de Consulta.
   * @param cargaReporteController Controlador de la carga de reportes.
   * @param messageView            Vista de mensajes.
   */
  public ReporteTableController(final JTable tablaReporte,
      final ConsultaService consultaService,
      final CargaReporteController cargaReporteController,
      final MessageView messageView) {
    super(tablaReporte,
        messageView);

    this.consultaService = consultaService;
    this.cargaReporteController = cargaReporteController;
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    return getValueAtColumn(getMedicoPojoInRow(rowIndex), columnIndex + 1);
  }

  @Override
  public void initController() {
    super.setNombreColumnas(this.consultaService.obtenerNombreColumnasReporte());

    super.initController();

    this.cargaReporteController.getCargaReporteView().addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(final WindowEvent windowEvent) {
        try {
          setFilas(consultaService.obtenerReportes(cargaReporteController.getFechaDesde(),
              cargaReporteController.getFechaHasta()).toArray());
        } catch (final ServiceException e) {
          handleExceptionMessage(e, "los reportes");
        }
        fireTableDataChanged();
        windowEvent.getWindow().dispose();
      }
    });
  }

  /**
   * Obtiene el valor de cada columna a partir de su posición en la BD.
   *
   * @param medicoPojo Medico actual.
   * @param column     Numero de la columna.
   * @return Valor en la columna indicada.
   */
  private Object getValueAtColumn(final MedicoPojo medicoPojo, final int column) {
    if (column == ReporteTableConstants.MEDICO.getPosition()) {
      return medicoPojo.toString().substring(0, medicoPojo.toString().indexOf("("));
    }
    if (column == ReporteTableConstants.CANTIDAD_CONSULTAS.getPosition()) {
      return medicoPojo.getCantidadConsultas();
    }
    if (column == ReporteTableConstants.TOTAL_RECAUDADO.getPosition()) {
      return medicoPojo.getTotalRecaudado();
    }

    return null;
  }

  private MedicoPojo getMedicoPojoInRow(final int row) {
    return (MedicoPojo) super.getFilas()[row];
  }

  public void initView() {
    try {
      super.setFilas(this.consultaService
          .obtenerReportes(this.cargaReporteController.getFechaDesde(),
              this.cargaReporteController.getFechaHasta()).toArray());
    } catch (final ServiceException e) {
      super.handleExceptionMessage(e, "los reportes");
    }
  }
}
