package service.implementation;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import model.dao.MedicoDao;
import model.dao.exception.DAOException;
import model.pojo.MedicoPojo;
import model.pojo.UsuarioPojo;
import service.MedicoService;
import service.excpetion.ServiceException;

/**
 * Clase que implementa los servicios de médicos.
 */
public class MedicoServiceImplementation implements MedicoService {

  private final MedicoDao medicoDao;
  private Map<Integer, MedicoPojo> medicoPojoMap;

  /**
   * Crea una implementación del servicio de Medicos.
   *
   * @param medicoDao DAO de medico.
   */
  public MedicoServiceImplementation(final MedicoDao medicoDao) {
    this.medicoDao = medicoDao;
    this.medicoPojoMap = new TreeMap<>();
  }

  @Override
  public void crearMedico(final MedicoPojo medicoPojo) throws ServiceException {
    try {
      this.checkUnicoDocumento(medicoPojo);

      this.medicoDao.createMedico(medicoPojo);
      this.medicoPojoMap.put(medicoPojo.getUsuarioId(), medicoPojo);
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public void actualizarMedico(final MedicoPojo medicoPojo) throws ServiceException {
    try {
      this.checkUnicoDocumento(medicoPojo);

      this.medicoDao.updateMedico(medicoPojo);
      this.medicoPojoMap.put(medicoPojo.getUsuarioId(), medicoPojo);
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public void eliminarMedico(final MedicoPojo medicoPojo) throws ServiceException {
    try {
      this.medicoDao.deleteMedico(medicoPojo);
      this.medicoPojoMap.remove(medicoPojo.getUsuarioId());
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public MedicoPojo obtenerMedico(final int medicoId) {
    return this.medicoPojoMap.get(medicoId);
  }

  @Override
  public List<MedicoPojo> obtenerTodosMedico() throws ServiceException {
    if (this.medicoPojoMap.isEmpty()) {
      try {
        this.medicoPojoMap = this.medicoDao.getAllMedicos();
      } catch (final DAOException e) {
        throw new ServiceException(e.getMessage());
      }
    }
    return this.medicoPojoMap.values()
        .stream()
        .sorted(Comparator.comparing(UsuarioPojo::getUsuarioId))
        .collect(Collectors.toList());
  }

  @Override
  public List<String> obtenerNombreColumnas() {
    return this.medicoDao.getMedicoNombreColumna();
  }

  /**
   * Controla si el medico no posee el mismo documento que otro medico.
   *
   * @param medicoPojo POJO del medico.
   * @throws ServiceException Si el medico posee el mismo documento que otro medico.
   */
  private void checkUnicoDocumento(final MedicoPojo medicoPojo) throws ServiceException {
    if (this.medicoPojoMap.values()
        .stream()
        .anyMatch(currentMedicoPojo ->
            !currentMedicoPojo.getUsuarioId().equals(medicoPojo.getUsuarioId()) &&
                currentMedicoPojo.getTipoDocumento().equals(medicoPojo.getTipoDocumento())
                && currentMedicoPojo.getNumeroDocumento()
                .equals(medicoPojo.getNumeroDocumento()))) {
      throw new ServiceException("El DNI ingresado ya se encuentra registrado para un medico.");
    }
  }
}
