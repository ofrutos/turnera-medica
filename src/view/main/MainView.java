package view.main;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JFrame;
import view.constants.ViewLabels;

/**
 * Clase que administra la ventana principal del gestor. <br>
 */
public class MainView extends JFrame {

  private static final long serialVersionUID = 1L;

  private final JFrame jFrame;
  private final BarraMainView barraMainView;
  private final JButton btnMedico;
  private final JButton btnPaciente;
  private final JButton btnConsultorio;
  private final JButton btnConsulta;
  private final JButton btnReporte;

  public MainView() {
    setDefaultCloseOperation(EXIT_ON_CLOSE);
    this.jFrame = new JFrame();
    this.barraMainView = new BarraMainView();

    this.barraMainView.initView();

    this.jFrame.setBounds(100, 100, 327, 213);
    this.jFrame.setJMenuBar(this.barraMainView);

    this.jFrame.setTitle(ViewLabels.TITULO_PRINCIPAL.getLabel());

    GridBagLayout gblPanel = new GridBagLayout();
    gblPanel.columnWidths = new int[]{0, 0, 0};
    gblPanel.rowHeights = new int[]{0, 0, 0};
    gblPanel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
    gblPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
    this.jFrame.getContentPane().setLayout(gblPanel);

    this.btnMedico = new JButton(ViewLabels.MEDICO.getLabel());
    GridBagConstraints gbcBtnMedico = new GridBagConstraints();
    gbcBtnMedico.fill = GridBagConstraints.HORIZONTAL;
    gbcBtnMedico.insets = new Insets(0, 0, 5, 0);
    gbcBtnMedico.gridx = 1;
    gbcBtnMedico.gridy = 0;
    this.jFrame.getContentPane().add(this.btnMedico, gbcBtnMedico);

    this.btnPaciente = new JButton(ViewLabels.PACIENTE.getLabel());
    GridBagConstraints gbcBtnPaciente = new GridBagConstraints();
    gbcBtnPaciente.fill = GridBagConstraints.HORIZONTAL;
    gbcBtnPaciente.insets = new Insets(0, 0, 5, 0);
    gbcBtnPaciente.gridx = 1;
    gbcBtnPaciente.gridy = 1;
    this.jFrame.getContentPane().add(this.btnPaciente, gbcBtnPaciente);

    this.btnConsultorio = new JButton(ViewLabels.CONSULTORIO.getLabel());
    GridBagConstraints gbcBtnConsultorio = new GridBagConstraints();
    gbcBtnConsultorio.fill = GridBagConstraints.HORIZONTAL;
    gbcBtnConsultorio.insets = new Insets(0, 0, 5, 0);
    gbcBtnConsultorio.gridx = 1;
    gbcBtnConsultorio.gridy = 2;
    this.jFrame.getContentPane().add(this.btnConsultorio, gbcBtnConsultorio);

    this.btnConsulta = new JButton(ViewLabels.CONSULTA.getLabel());
    GridBagConstraints gbcBtnConsulta = new GridBagConstraints();
    gbcBtnConsulta.fill = GridBagConstraints.HORIZONTAL;
    gbcBtnConsulta.insets = new Insets(0, 0, 5, 0);
    gbcBtnConsulta.gridx = 1;
    gbcBtnConsulta.gridy = 3;
    this.jFrame.getContentPane().add(this.btnConsulta, gbcBtnConsulta);

    this.btnReporte = new JButton(ViewLabels.REPORTE.getLabel());
    GridBagConstraints gbcBtnReporte = new GridBagConstraints();
    gbcBtnReporte.fill = GridBagConstraints.HORIZONTAL;
    gbcBtnReporte.insets = new Insets(0, 0, 5, 0);
    gbcBtnReporte.gridx = 1;
    gbcBtnReporte.gridy = 4;
    this.jFrame.getContentPane().add(this.btnReporte, gbcBtnReporte);

    this.jFrame.setVisible(true);
  }

  public BarraMainView getBarraMainView() {
    return this.barraMainView;
  }

  public JFrame getjFrame() {
    return this.jFrame;
  }

  public JButton getBtnMedico() {
    return this.btnMedico;
  }

  public JButton getBtnPaciente() {
    return this.btnPaciente;
  }

  public JButton getBtnConsultorio() {
    return this.btnConsultorio;
  }

  public JButton getBtnConsulta() {
    return this.btnConsulta;
  }

  public JButton getBtnReporte() {
    return this.btnReporte;
  }
}
