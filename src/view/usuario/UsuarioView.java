package view.usuario;

import view.GeneralBarraView;
import view.GeneralView;

/**
 * Clase que administra la vista de un usuario.
 */
public class UsuarioView extends GeneralView {

  /**
   * Crea la vista de un usuario.
   *
   * @param generalBarraView Barra general de la vista.
   */
  public UsuarioView(final GeneralBarraView generalBarraView) {
    super(generalBarraView);
  }
}
