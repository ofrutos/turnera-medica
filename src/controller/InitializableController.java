package controller;

/**
 * Interfaz general de controladores.
 */
public interface InitializableController {

  /**
   * Inicializa el controlador.
   */
  void initController();
}
