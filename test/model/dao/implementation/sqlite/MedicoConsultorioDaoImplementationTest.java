package model.dao.implementation.sqlite;

import static org.junit.Assert.assertEquals;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;
import static util.jdbc.DBManagerMock.borrarConsultorios;
import static util.jdbc.DBManagerMock.borrarMedicoConsultorios;
import static util.jdbc.DBManagerMock.borrarMedicos;
import static util.jdbc.DBManagerMock.crearConsultorios;
import static util.jdbc.DBManagerMock.crearMedicoConsultorios;
import static util.jdbc.DBManagerMock.crearMedicos;
import static util.pojo.MedicoConsultorioPojoMock.getAllMedicoConsultorio;

import java.sql.Connection;
import java.text.ParseException;
import java.util.Map;
import model.dao.exception.DAOException;
import model.jdbc.DBManager;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoConsultorioPojoKey;
import model.util.DateUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import util.jdbc.DBManagerMock;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DBManager.class)
public class MedicoConsultorioDaoImplementationTest {

  private MedicoConsultorioDaoImplementation medicoConsultorioDaoImplementation;

  @BeforeClass
  public static void preTests() {
    spy(DBManager.class);
    when(DBManager.connect())
        .thenAnswer((Answer<Connection>) invocationOnMock -> DBManagerMock.connect());

    crearMedicos();
    crearConsultorios();
    crearMedicoConsultorios();
  }

  @Before
  public void setup() {
    this.medicoConsultorioDaoImplementation = new MedicoConsultorioDaoImplementation();
  }

  @AfterClass
  public static void postTests() {
    borrarMedicoConsultorios();
    borrarMedicos();
    borrarConsultorios();
  }

  @Test
  public void consultarMedicoConsultorio_Ok() throws DAOException, ParseException {
    final Map<MedicoConsultorioPojoKey, MedicoConsultorioPojo> expectedMedicoConsultorioPojos = getAllMedicoConsultorio();

    final Map<MedicoConsultorioPojoKey, MedicoConsultorioPojo> actualMedicoConsultorioPojos = this.medicoConsultorioDaoImplementation
        .getAllMedicoConsultorio();

    assertEquals(expectedMedicoConsultorioPojos, actualMedicoConsultorioPojos);
  }

  @Test
  public void crearMedicoConsultorio_Ok() throws DAOException, ParseException {
    final MedicoConsultorioPojo actualMedicoConsultorioPojo = new MedicoConsultorioPojo();
    actualMedicoConsultorioPojo.getMedicoPojo().setUsuarioId(9996);
    actualMedicoConsultorioPojo.getConsultorioPojo().setConsultorioId(102);
    actualMedicoConsultorioPojo.setFechaInicio(DateUtils.getDateFormat().parse("01/01/2012"));
    actualMedicoConsultorioPojo.setFechaFin(DateUtils.getDateFormat().parse(""));
//    actualMedicoConsultorioPojo.setHorarioId(23);

    this.medicoConsultorioDaoImplementation.createMedicoConsultorio(actualMedicoConsultorioPojo);
  }

  @Test(expected = DAOException.class)
  public void crearMedicoConsultorio_Error() throws DAOException, ParseException {
    final MedicoConsultorioPojo actualMedicoConsultorioPojo = new MedicoConsultorioPojo();
    actualMedicoConsultorioPojo.getMedicoPojo().setUsuarioId(9996);
    actualMedicoConsultorioPojo.getConsultorioPojo().setConsultorioId(101);
    actualMedicoConsultorioPojo.setFechaInicio(DateUtils.getDateFormat().parse("01/01/2010"));
    actualMedicoConsultorioPojo.setFechaFin(DateUtils.getDateFormat().parse(""));
//    actualMedicoConsultorioPojo.setHorarioId(17);

    this.medicoConsultorioDaoImplementation.createMedicoConsultorio(actualMedicoConsultorioPojo);
  }

  @Test
  public void updateMedicoConsultorio_Ok() throws DAOException, ParseException {
    final MedicoConsultorioPojo actualMedicoConsultorioPojo = new MedicoConsultorioPojo();
    actualMedicoConsultorioPojo.getMedicoPojo().setUsuarioId(9995);
    actualMedicoConsultorioPojo.getConsultorioPojo().setConsultorioId(103);
    actualMedicoConsultorioPojo.setFechaInicio(DateUtils.getDateFormat().parse("01/01/2010"));
    actualMedicoConsultorioPojo.setFechaFin(DateUtils.getDateFormat().parse("02/01/2010"));
//    actualMedicoConsultorioPojo.setHorarioId(23);

    this.medicoConsultorioDaoImplementation.updateMedicoConsultorio(actualMedicoConsultorioPojo);
  }

  @Test(expected = DAOException.class)
  public void updateMedicoConsultorio_Error() throws DAOException, ParseException {
    final MedicoConsultorioPojo actualMedicoConsultorioPojo = new MedicoConsultorioPojo();
    actualMedicoConsultorioPojo.getMedicoPojo().setUsuarioId(9990);
    actualMedicoConsultorioPojo.getConsultorioPojo().setConsultorioId(103);
    actualMedicoConsultorioPojo.setFechaInicio(DateUtils.getDateFormat().parse("01/01/2010"));
    actualMedicoConsultorioPojo.setFechaFin(DateUtils.getDateFormat().parse("02/01/2010"));
//    actualMedicoConsultorioPojo.getHorarioPojo().setHorarioId(23);

    this.medicoConsultorioDaoImplementation.updateMedicoConsultorio(actualMedicoConsultorioPojo);
  }
}
