package controller.medicoconsultorio;

import controller.GeneralBarraController;
import controller.InitializableController;
import view.medicoconsultorio.BarraMedicoConsultorioView;

/**
 * Clase que administra el controlador de la barra de menú de la ventana principal de consultorios
 * de medicos.
 */
public class BarraMedicoConsultorioController extends GeneralBarraController implements
    InitializableController {

  private final BarraMedicoConsultorioView barraMedicoConsultorioView;
  private final MedicoConsultorioController medicoConsultorioController;
  private final CargaMedicoConsultorioController cargaMedicoConsultorioController;

  /**
   * Crea el controlador de la barra de menu de la ventana principal de medicos.
   *
   * @param barraMedicoConsultorioView       Vista de la barra de los consultorios del medico.
   * @param cargaMedicoConsultorioController Controlador de la carga de consultorios del medico.
   */
  public BarraMedicoConsultorioController(
      final BarraMedicoConsultorioView barraMedicoConsultorioView,
      final CargaMedicoConsultorioController cargaMedicoConsultorioController,
      final MedicoConsultorioController medicoConsultorioController) {
    super(barraMedicoConsultorioView);
    this.barraMedicoConsultorioView = barraMedicoConsultorioView;
    this.cargaMedicoConsultorioController = cargaMedicoConsultorioController;
    this.medicoConsultorioController = medicoConsultorioController;
  }

  @Override
  public void initController() {
    super.initController();
    this.barraMedicoConsultorioView.getConsultorioLbl().addActionListener(e -> this.crearMedico());
  }

  /**
   * Carga el formulario para crear un nuevo médico.
   */
  private void crearMedico() {
    this.cargaMedicoConsultorioController.setMedicoConsultorioPojo(null);
    this.cargaMedicoConsultorioController
        .setMedicoPojo(this.medicoConsultorioController.getMedicoPojo());
    this.cargaMedicoConsultorioController.initView();
  }
}
