package controller.consultorio;

import controller.CargaController;
import model.pojo.ConsultorioPojo;
import service.ConsultorioService;
import service.excpetion.ServiceException;
import view.constants.ViewLabels;
import view.consultorio.CargaConsultorioView;
import view.message.MessageView;

/**
 * Clase que administra el controlador de alta y modificación de consultorios.
 */
public class CargaConsultorioController extends CargaController<ConsultorioPojo> {

  private final CargaConsultorioView cargaConsultorioView;
  private final ConsultorioService consultorioService;
  private ConsultorioPojo consultorioPojo;

  /**
   * Crea el controlador de la carga de consultorios.
   *
   * @param cargaConsultorioView Vista de la carga de consultorios.
   * @param messageView          Vista de mensajes.
   * @param consultorioService   Servicio de Consultorios.
   */
  public CargaConsultorioController(final CargaConsultorioView cargaConsultorioView,
      final MessageView messageView,
      final ConsultorioService consultorioService) {
    super(cargaConsultorioView,
        messageView);

    this.cargaConsultorioView = cargaConsultorioView;
    this.consultorioService = consultorioService;
  }

  public void initView() {
    if (this.consultorioPojo != null) {
      this.cargaConsultorioView
          .setTitle("Actualizando consultorio " + this.consultorioPojo.getConsultorioId());

      this.cargaConsultorioView.getTfNombre().setText(this.consultorioPojo.getNombre());
    } else {
      this.cargaConsultorioView.setTitle("Creando consultorio");

      this.cargaConsultorioView.getTfNombre().setText(null);
    }
    this.cargaConsultorioView.setVisible(true);
  }

  @Override
  public boolean controlarCarga() {
    if (this.cargaConsultorioView.getTfNombre().getText().isEmpty()) {
      super.showMessage(ViewLabels.MENSAJE_ADVERTENCIA.getLabel(),
          "Se debe indicar el nombre del consultorio.");
      return false;
    }
    return true;
  }

  /**
   * Actualiza la información del consultorio. Si el consultorio no existe en el sistema, lo
   * genera.
   */
  @Override
  protected void update() {
    if (this.controlarCarga()) {
      try {
        String mensaje;
        if (this.consultorioPojo == null) {
          this.consultorioService.crearConsultorio(getDataInView());
          mensaje = "Se ha creado el consultorio exitosamente.";
        } else {
          this.consultorioService.actualizarConsultorio(getDataInView());
          mensaje = "Se ha actualizado el consultorio exitosamente.";
        }
        super.showMessage(ViewLabels.MENSAJE_EXITO.getLabel(), mensaje);
        this.cargaConsultorioView.dispose();
      } catch (final ServiceException e) {
        super.showMessage(ViewLabels.MENSAJE_ERROR.getLabel(), e.getMessage());
      }
    }
  }

  @Override
  protected ConsultorioPojo getDataInView() {
    return new ConsultorioPojo(
        this.consultorioPojo == null ? null : this.consultorioPojo.getConsultorioId(),
        this.cargaConsultorioView.getTfNombre().getText());
  }

  /**
   * Establece el consultorio a visualizar en la carga.
   *
   * @param consultorioPojo POJO del consultorio.
   */
  public void setConsultorioPojo(final ConsultorioPojo consultorioPojo) {
    this.consultorioPojo = consultorioPojo;
  }

  /**
   * Devuelve la vista de la carga de consultorio.
   *
   * @return Vista de la carga de consultorio.
   */
  public CargaConsultorioView getCargaConsultorioView() {
    return this.cargaConsultorioView;
  }
}
