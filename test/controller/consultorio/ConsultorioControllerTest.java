package controller.consultorio;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import javax.swing.JTable;
import model.dao.implementation.sqlite.ConsultorioDaoImplementation;
import org.junit.Before;
import org.junit.Test;
import service.ConsultorioService;
import service.implementation.ConsultorioServiceImplementation;
import view.consultorio.CargaConsultorioView;
import view.message.MessageView;

public class ConsultorioControllerTest {

  private ConsultorioTableController consultorioTableController;

  @Before
  public void setUp() {
    final ConsultorioService consultorioService = new ConsultorioServiceImplementation(
        new ConsultorioDaoImplementation());
    final CargaConsultorioController cargaConsultorioController = new CargaConsultorioController(
        new CargaConsultorioView(),
        new MessageView(),
        consultorioService);

    this.consultorioTableController = new ConsultorioTableController(new JTable(),
        consultorioService,
        cargaConsultorioController,
        new MessageView());

    this.consultorioTableController.initController();
  }

  @Test
  public void getColumnCountTest() {
    assertEquals(2, this.consultorioTableController.getColumnCount());
  }

  @Test
  public void getColumnNameTest() {
    List<String> nombreColumnas = Arrays
        .asList("Consultorio Id",
            "Nombre");

    for (int columna = 0; columna < 2; columna++) {
      assertEquals(nombreColumnas.get(columna),
          this.consultorioTableController.getColumnName(columna));
    }
  }
}
