package model.dao.implementation.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import model.constants.table.PacienteTableConstants;
import model.constants.usuario.TipoDocumento;
import model.dao.PacienteDao;
import model.dao.exception.DAOException;
import model.dao.utils.LabelUtils;
import model.jdbc.DBManager;
import model.pojo.PacientePojo;
import model.pojo.TipoUsuario;
import model.util.DateUtils;
import org.apache.commons.lang3.EnumUtils;

/**
 * Clase que administra los DAO para los pacientes en una base SQLite. <br>
 */
public class PacienteDaoImplementation implements PacienteDao {

  /**
   * Declaración de SELECT (All) de la tabla de pacientes. <br>
   */
  private static final String SELECT_ALL_PACIENTES = "SELECT * FROM Usuario WHERE Tipo_Usuario = ?";

  /**
   * Obtiene un único paciente.
   */
  private static final String SELECT_ONE_PACIENTE = "SELECT * FROM Usuario WHERE Usuario_Id = ? AND Tipo_Usuario = ?";

  /**
   * Declaración de INSERT de la tabla de pacientes. <br>
   */
  private static final String INSERT_PACIENTE = "INSERT INTO Usuario (Tipo_Documento, Numero_Documento, Nombre, Apellido, Fecha_Nacimiento, Tipo_Usuario) VALUES (?, ?, ?, ?, ?, ?)";

  /**
   * Declaración de UPDATE de la tabla de pacientes. <br>
   */
  private static final String UPDATE_PACIENTE = "UPDATE Usuario SET Tipo_Documento = ?, Numero_Documento = ?, Nombre = ?, Apellido = ?, Fecha_Nacimiento = ?, Tipo_Usuario = ? WHERE Usuario_Id = ?";

  /**
   * Declaración de DELETE de la tabla de pacientes. <br>
   */
  private static final String DELETE_PACIENTE = "DELETE FROM Usuario WHERE Usuario_Id = ?";

  /**
   * Conexión con la base de datos. <br>
   */
  private Connection connection;

  public void createPaciente(PacientePojo pacientePojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(INSERT_PACIENTE);
      statement.setString(1, pacientePojo.getTipoDocumento().name());
      statement.setString(2, pacientePojo.getNumeroDocumento());
      statement.setString(3, pacientePojo.getNombre());
      statement.setString(4, pacientePojo.getApellido());
      statement.setString(5, DateUtils.parseDate(pacientePojo.getFechaNacimiento()));
      statement.setString(6, pacientePojo.getTipoUsuario().name());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows created.");
      }

      ResultSet resultSet = statement.getGeneratedKeys();

      if (resultSet != null && resultSet.next()) {
        pacientePojo.setUsuarioId(resultSet.getInt(1));
      }
      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public void updatePaciente(PacientePojo pacientePojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(UPDATE_PACIENTE);
      statement.setString(1, pacientePojo.getTipoDocumento().name());
      statement.setString(2, pacientePojo.getNumeroDocumento());
      statement.setString(3, pacientePojo.getNombre());
      statement.setString(4, pacientePojo.getApellido());
      statement.setString(5, DateUtils.parseDate(pacientePojo.getFechaNacimiento()));
      statement.setString(6, pacientePojo.getTipoUsuario().name());
      statement.setInt(7, pacientePojo.getUsuarioId());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows updated.");
      }

      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public Map<Integer, PacientePojo> getAllPacientes() throws DAOException {
    Map<Integer, PacientePojo> pacientePojos = new TreeMap<>();
    this.connection = DBManager.connect();
    try {
      PreparedStatement preparedStatement = this.connection.prepareStatement(SELECT_ALL_PACIENTES);
      preparedStatement.setString(1, TipoUsuario.PACIENTE.toString());

      ResultSet resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        pacientePojos.put(resultSet.getInt(1),
            new PacientePojo(resultSet.getInt(1),
                TipoDocumento.valueOf(resultSet.getString(2)),
                resultSet.getString(3),
                resultSet.getString(4),
                resultSet.getString(5),
                DateUtils.getDateFormat().parse(resultSet.getString(6)),
                TipoUsuario.valueOf(resultSet.getString(7))));
      }
    } catch (final SQLException | ParseException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
    return pacientePojos;
  }

  @Override
  public PacientePojo getPaciente(final Integer pacienteId) throws DAOException {
    this.connection = DBManager.connect();
    PacientePojo pacientePojo = null;
    try {
      PreparedStatement preparedStatement = this.connection.prepareStatement(SELECT_ONE_PACIENTE);
      preparedStatement.setInt(1, pacienteId);
      preparedStatement.setString(2, TipoUsuario.PACIENTE.name());

      ResultSet resultSet = preparedStatement.executeQuery();
      if (resultSet.next()) {
        pacientePojo = new PacientePojo();
        pacientePojo.setUsuarioId(resultSet.getInt(1));
        pacientePojo.setTipoDocumento(TipoDocumento.valueOf(resultSet.getString(2)));
        pacientePojo.setNumeroDocumento(resultSet.getString(3));
        pacientePojo.setNombre(resultSet.getString(4));
        pacientePojo.setApellido(resultSet.getString(5));
        pacientePojo.setFechaNacimiento(DateUtils.getDateFormat().parse(resultSet.getString(6)));
        pacientePojo.setTipoUsuario(TipoUsuario.valueOf(resultSet.getString(7)));
      }
    } catch (final SQLException | ParseException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
    return pacientePojo;
  }

  public void deletePaciente(PacientePojo pacientePojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(DELETE_PACIENTE);
      statement.setInt(1, pacientePojo.getUsuarioId());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows deleted.");
      }

      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public List<String> getPacienteNombreColumna() {
    this.connection = DBManager.connect();
    List<String> cabecera = new ArrayList<>();
    try {
      PreparedStatement statement = this.connection.prepareStatement(SELECT_ONE_PACIENTE);
      // Solo nos interesa conseguir la cabecera.
      statement.setInt(1, 0);
      statement.setString(1, TipoUsuario.PACIENTE.toString());

      ResultSetMetaData meta = statement.getMetaData();
      int cantidadColumnas = statement.getMetaData().getColumnCount();

      for (int columna = 1; columna <= cantidadColumnas; columna++) {
        cabecera.add(meta.getColumnName(columna));
      }

      cabecera = cabecera.stream()
          .filter(this::filtrarColumnasPrivadas)
          .map(LabelUtils::normalizarLabel)
          .collect(Collectors.toList());
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
    } finally {
      DBManager.handleClose(this.connection);
    }

    return cabecera;
  }

  /**
   * Indica las columnas que no deben ser visualizadas.
   *
   * @param columna Nombre de la columna.
   * @return <b>true</b> si la columna debe ser visualizada.<br><b>false</b> si no debe ser
   * visualizada.
   */
  private boolean filtrarColumnasPrivadas(final String columna) {
    return EnumUtils.getEnumIgnoreCase(PacienteTableConstants.class, columna) != null;
  }
}
