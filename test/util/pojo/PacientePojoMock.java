package util.pojo;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import model.constants.usuario.TipoDocumento;
import model.pojo.PacientePojo;
import model.pojo.TipoUsuario;
import model.util.DateUtils;

public class PacientePojoMock {

  public static PacientePojo getPacienteUno() throws ParseException {
    final PacientePojo pacientePojo = new PacientePojo();
    pacientePojo.setUsuarioId(9999);
    pacientePojo.setTipoDocumento(TipoDocumento.DNI);
    pacientePojo.setNumeroDocumento("17346910");
    pacientePojo.setNombre("Juan");
    pacientePojo.setApellido("Perez");
    pacientePojo.setFechaNacimiento(DateUtils.getDateFormat().parse("01/01/1990"));
    pacientePojo.setTipoUsuario(TipoUsuario.PACIENTE);
    return pacientePojo;
  }

  public static PacientePojo getPacienteDos() throws ParseException {
    final PacientePojo pacientePojo = new PacientePojo();
    pacientePojo.setUsuarioId(9997);
    pacientePojo.setTipoDocumento(TipoDocumento.DNI);
    pacientePojo.setNumeroDocumento("26145124");
    pacientePojo.setNombre("Romina");
    pacientePojo.setApellido("Molina");
    pacientePojo.setFechaNacimiento(DateUtils.getDateFormat().parse("10/10/1985"));
    pacientePojo.setTipoUsuario(TipoUsuario.PACIENTE);
    return pacientePojo;
  }

  public static PacientePojo getPacienteTres() throws ParseException {
    final PacientePojo pacientePojo = new PacientePojo();
    pacientePojo.setUsuarioId(9998);
    pacientePojo.setTipoDocumento(TipoDocumento.DNI);
    pacientePojo.setNumeroDocumento("35513987");
    pacientePojo.setNombre("Julian");
    pacientePojo.setApellido("Velazquez");
    pacientePojo.setFechaNacimiento(DateUtils.getDateFormat().parse("20/01/1995"));
    pacientePojo.setTipoUsuario(TipoUsuario.PACIENTE);
    return pacientePojo;
  }

  public static Map<Integer, PacientePojo> getAllPacientes() throws ParseException {
    final Map<Integer, PacientePojo> pacientePojos = new HashMap<>();

    pacientePojos.put(9999, getPacienteUno());
    pacientePojos.put(9997, getPacienteDos());

    return pacientePojos;
  }
}
