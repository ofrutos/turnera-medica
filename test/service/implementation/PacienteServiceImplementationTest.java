package service.implementation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static util.pojo.PacientePojoMock.getAllPacientes;
import static util.pojo.PacientePojoMock.getPacienteUno;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import model.constants.usuario.TipoDocumento;
import model.dao.PacienteDao;
import model.dao.exception.DAOException;
import model.dao.implementation.sqlite.PacienteDaoImplementation;
import model.pojo.PacientePojo;
import model.pojo.TipoUsuario;
import model.pojo.UsuarioPojo;
import model.util.DateUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import service.PacienteService;
import service.excpetion.ServiceException;

public class PacienteServiceImplementationTest {

  @Rule
  public MockitoRule initRule = MockitoJUnit.rule();

  @Mock
  private final PacienteDao pacienteDao = new PacienteDaoImplementation();

  private PacienteService pacienteService;

  @Before
  public void setup() {
    this.pacienteService = new PacienteServiceImplementation(this.pacienteDao);
  }

  @Ignore
  public void crearPaciente_Ok() throws DAOException, ServiceException {
    doNothing().when(this.pacienteDao).createPaciente(isA(PacientePojo.class));

    final PacientePojo pacientePojo = new PacientePojo();

    this.pacienteService.crearPaciente(pacientePojo);
  }

  @Test(expected = ServiceException.class)
  public void crearPaciente_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.pacienteDao).createPaciente(isA(PacientePojo.class));

    final PacientePojo pacientePojo = new PacientePojo();

    this.pacienteService.crearPaciente(pacientePojo);
  }

  @Ignore
  public void actualizarPaciente_Ok() throws DAOException, ServiceException {
    doNothing().when(this.pacienteDao).updatePaciente(isA(PacientePojo.class));

    final PacientePojo pacientePojo = new PacientePojo();

    this.pacienteService.actualizarPaciente(pacientePojo);
  }

  @Test(expected = ServiceException.class)
  public void actualizarPaciente_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.pacienteDao).updatePaciente(isA(PacientePojo.class));

    final PacientePojo pacientePojo = new PacientePojo();

    this.pacienteService.actualizarPaciente(pacientePojo);
  }

  @Ignore
  public void eliminarPaciente_Ok() throws DAOException, ServiceException {
    doNothing().when(this.pacienteDao).deletePaciente(isA(PacientePojo.class));

    final PacientePojo pacientePojo = new PacientePojo();

    this.pacienteService.eliminarPaciente(pacientePojo);
  }

  @Test(expected = ServiceException.class)
  public void eliminarPaciente_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.pacienteDao).deletePaciente(isA(PacientePojo.class));

    final PacientePojo pacientePojo = new PacientePojo();

    this.pacienteService.eliminarPaciente(pacientePojo);
  }

  @Ignore
  public void consultarPaciente_Ok() throws DAOException, ParseException {
    when(this.pacienteDao.getPaciente(isA(Integer.class))).thenReturn(getPacienteUno());

    final PacientePojo expectedPacientePojo = new PacientePojo();
    expectedPacientePojo.setUsuarioId(9999);
    expectedPacientePojo.setTipoDocumento(TipoDocumento.DNI);
    expectedPacientePojo.setNumeroDocumento("17346910");
    expectedPacientePojo.setNombre("Juan");
    expectedPacientePojo.setApellido("Perez");
    expectedPacientePojo.setFechaNacimiento(DateUtils.getDateFormat().parse("01/01/1990"));
    expectedPacientePojo.setTipoUsuario(TipoUsuario.PACIENTE);

    final int pacienteId = 9999;
    final PacientePojo actualPacientePojo = this.pacienteService.obtenerPaciente(pacienteId);

    assertEquals(expectedPacientePojo, actualPacientePojo);
  }

  @Test
  public void consultarPaciente_Error() throws DAOException {
    when(this.pacienteDao.getPaciente(isA(Integer.class))).thenThrow(DAOException.class);

    final int pacienteId = 1;
    final PacientePojo actualPacientePojo = this.pacienteService.obtenerPaciente(pacienteId);

    assertNull(actualPacientePojo);
  }

  @Test
  public void consultarAllPacientes_Ok() throws DAOException, ServiceException, ParseException {
    when(this.pacienteDao.getAllPacientes()).thenReturn(getAllPacientes());

    final List<PacientePojo> expectedPacientesPojo = new ArrayList<>();

    final PacientePojo expectedPacientePojo1 = new PacientePojo();
    expectedPacientePojo1.setUsuarioId(9999);
    expectedPacientePojo1.setTipoDocumento(TipoDocumento.DNI);
    expectedPacientePojo1.setNumeroDocumento("17346910");
    expectedPacientePojo1.setNombre("Juan");
    expectedPacientePojo1.setApellido("Perez");
    expectedPacientePojo1.setFechaNacimiento(DateUtils.getDateFormat().parse("01/01/1990"));
    expectedPacientePojo1.setTipoUsuario(TipoUsuario.PACIENTE);

    expectedPacientesPojo.add(expectedPacientePojo1);

    final PacientePojo expectedPacientePojo2 = new PacientePojo();
    expectedPacientePojo2.setUsuarioId(9997);
    expectedPacientePojo2.setTipoDocumento(TipoDocumento.DNI);
    expectedPacientePojo2.setNumeroDocumento("26145124");
    expectedPacientePojo2.setNombre("Romina");
    expectedPacientePojo2.setApellido("Molina");
    expectedPacientePojo2.setFechaNacimiento(DateUtils.getDateFormat().parse("10/10/1985"));
    expectedPacientePojo2.setTipoUsuario(TipoUsuario.PACIENTE);

    expectedPacientesPojo.add(expectedPacientePojo2);

    expectedPacientesPojo.sort(Comparator.comparing(UsuarioPojo::getUsuarioId));

    final List<PacientePojo> actualPacientesPojo = this.pacienteService
        .obtenerTodosPaciente();

    assertEquals(expectedPacientesPojo, actualPacientesPojo);
  }

  @Test(expected = ServiceException.class)
  public void consultarAllPacientes_Error() throws DAOException, ServiceException {
    when(this.pacienteDao.getAllPacientes()).thenThrow(DAOException.class);

    final List<PacientePojo> actualPacientesPojo = this.pacienteService.obtenerTodosPaciente();
  }
}
