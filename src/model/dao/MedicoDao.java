package model.dao;

import java.util.List;
import java.util.Map;
import model.dao.exception.DAOException;
import model.pojo.MedicoPojo;

/**
 * Interfaz que administra todas las declaraciones para el DAO de un médico.
 */
public interface MedicoDao {

  /**
   * Crea un médico.
   *
   * @param medicoPojo Médico a crear.
   * @throws DAOException Si no se pudo crear un medico u ocurrió un error en la base de datos.
   */
  void createMedico(final MedicoPojo medicoPojo) throws DAOException;

  /**
   * Actualiza un médico.
   *
   * @param medicoPojo Médico a actualizar.
   * @throws DAOException Si no se pudo actualizar un medico u ocurrió un error en la base de
   *                      datos.
   */
  void updateMedico(final MedicoPojo medicoPojo) throws DAOException;

  /**
   * Elimina un médico.
   *
   * @param medicoPojo Médico a eliminar.
   * @throws DAOException Si no se pudo eliminar un medico u ocurrió un error en la base de datos.
   */
  void deleteMedico(final MedicoPojo medicoPojo) throws DAOException;

  /**
   * Lista todos los médicos.
   *
   * @return Todos los médicos.
   */
  Map<Integer, MedicoPojo> getAllMedicos() throws DAOException;

  /**
   * Obtiene los labels de la tabla de médicos.
   *
   * @return Label de la tabla de médicos.
   */
  List<String> getMedicoNombreColumna();

  /**
   * Obtiene un medico.
   *
   * @param medicoId Id del medico.
   * @return Medico.
   */
  MedicoPojo getMedico(final Integer medicoId) throws DAOException;
}
