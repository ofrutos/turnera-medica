package view.constants;

/**
 * Constantes para la los labels de distintas funcionalidades.
 */
public enum ViewLabels {
  ITEM_NUEVO("Nuevo"),
  ITEM_ARCHIVO("Archivo"),
  SALIR("Salir"),
  OK("Ok"),
  CANCELAR("Cancelar"),
  CONSULTORIO("Consultorio"),
  PACIENTE("Paciente"),
  MEDICO("Medico"),
  CONSULTA("Consulta"),
  REPORTE("Reporte"),
  RANGO_FECHAS("Rango Fechas"),
  TITULO_PRINCIPAL("Turnera Medica"),
  TITULO_PACIENTES("Pacientes"),
  TITULO_MEDICOS("Medicos"),
  TITULO_CONSULTORIOS("Consultorios"),
  TITULO_CONSULTAS("Consultas"),
  TITULO_REPORTE("Reportes"),
  MENSAJE_EXITO("Exito"),
  MENSAJE_ADVERTENCIA("Advertencia"),
  MENSAJE_ERROR("Error");

  private final String label;

  ViewLabels(final String label) {
    this.label = label;
  }

  public String getLabel() {
    return this.label;
  }
}
