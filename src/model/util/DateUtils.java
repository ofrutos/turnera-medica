package model.util;

import java.text.DateFormat;
import java.util.Date;
import org.sqlite.date.DateFormatUtils;

/**
 * Clase utilitaria de fechas.
 */
public class DateUtils {

  private static final String DATE_FORMAT = "dd/MM/yyyy";
  private static final Date MAX_DATE = new Date(Long.MAX_VALUE);
  private static final Date MIN_DATE = new Date(Long.MIN_VALUE);

  private DateUtils() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Parsea una fecha al formato usado en la aplicación.
   *
   * @param date Fecha.
   * @return String de la fecha.
   */
  public static String parseDate(final Date date) {
    if (date.equals(MAX_DATE)) {
      return "";
    } else {
      return DateFormatUtils.format(date, DATE_FORMAT);
    }
  }

  /**
   * Devuelve el formato de fecha utilizado en la aplicación.
   *
   * @return Formato de fecha.
   */
  public static DateFormat getDateFormat() {
    return new TurneraDateFormat(DATE_FORMAT);
  }

  /**
   * Devuelve la máxima fecha posible.
   *
   * @return Máxima fecha posible.
   */
  public static Date getMaximumDate() {
    return MAX_DATE;
  }

  /**
   * Devuelve la mínima fecha posible.
   *
   * @return Mínima fecha posible.
   */
  public static Date getMinimumDate() {
    return MIN_DATE;
  }

  /**
   * Controla si una fecha se encuentra en el medio de un rango de fechas.
   *
   * @param fechaInicio   Fecha de inicio.
   * @param fechaFin      Fecha de fin.
   * @param fechaCuestion Fecha en cuestión.
   * @return <b>true</b> si la fecha se encuentra dentro del rango, <b>false</b> de lo contrario.
   */
  public static boolean isDateBetween(final Date fechaInicio,
      final Date fechaFin,
      final Date fechaCuestion) {
    return fechaInicio.getTime() < fechaCuestion.getTime()
        && fechaCuestion.getTime() < fechaFin.getTime();
  }

  /**
   * Controla si el rango de fechas ingresado es válido.
   *
   * @param fechaDesde Fecha desde.
   * @param fechaHasta Fecha hasta.
   * @return <b>true</b> si el rango de fechas es valido, <b>false</b> de lo contrario.
   */
  public static boolean isRangoFechasValida(final Date fechaDesde,
      final Date fechaHasta) {
    return fechaDesde.getTime() < fechaHasta.getTime();
  }
}
