package service.implementation;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import model.dao.ConsultorioDao;
import model.dao.exception.DAOException;
import model.pojo.ConsultorioPojo;
import service.ConsultorioService;
import service.excpetion.ServiceException;

/**
 * Clase que implementa los servicios de consultorios.
 */
public class ConsultorioServiceImplementation implements ConsultorioService {

  private final ConsultorioDao consultorioDao;
  private Map<Integer, ConsultorioPojo> consultorioPojoMap;

  /**
   * Crea una implementación del servicio de Consultorios.
   *
   * @param consultorioDao DAO de consultorio.
   */
  public ConsultorioServiceImplementation(final ConsultorioDao consultorioDao) {
    this.consultorioDao = consultorioDao;
    this.consultorioPojoMap = new TreeMap<>();
  }

  @Override
  public void crearConsultorio(final ConsultorioPojo consultorioPojo) throws ServiceException {
    try {
      this.consultorioDao.createConsultorio(consultorioPojo);
      this.consultorioPojoMap.put(consultorioPojo.getConsultorioId(), consultorioPojo);
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public void actualizarConsultorio(final ConsultorioPojo consultorioPojo)
      throws ServiceException {
    try {
      this.consultorioDao.updateConsultorio(consultorioPojo);
      this.consultorioPojoMap.put(consultorioPojo.getConsultorioId(), consultorioPojo);
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public void eliminarConsultorio(final ConsultorioPojo consultorioPojo) throws ServiceException {
    try {
      this.consultorioDao.deleteConsultorio(consultorioPojo);
      this.consultorioPojoMap.remove(consultorioPojo.getConsultorioId());
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public ConsultorioPojo obtenerConsultorio(final int consultorioId) {
    return this.consultorioPojoMap.get(consultorioId);
  }

  @Override
  public List<ConsultorioPojo> obtenerTodosConsultorio() throws ServiceException {
    if (this.consultorioPojoMap.isEmpty()) {
      try {
        this.consultorioPojoMap = this.consultorioDao.getAllConsultorio();
      } catch (final DAOException e) {
        throw new ServiceException(e.getMessage());
      }
    }
    return this.consultorioPojoMap.values()
        .stream()
        .sorted(Comparator.comparing(ConsultorioPojo::getConsultorioId))
        .collect(Collectors.toList());
  }

  @Override
  public List<String> obtenerNombreColumnas() {
    return this.consultorioDao.getConsultorioNombreColumna();
  }
}
