INSERT INTO main.Usuario
(Usuario_Id,
 Tipo_Documento,
 Numero_Documento,
 Nombre,
 Apellido,
 Fecha_Nacimiento,
 Tipo_Usuario)
VALUES (9999,
        'DNI',
        '17346910',
        'Juan',
        'Perez',
        '01/01/1990',
        'PACIENTE');

INSERT INTO main.Usuario
(Usuario_Id,
 Tipo_Documento,
 Numero_Documento,
 Nombre,
 Apellido,
 Fecha_Nacimiento,
 Tipo_Usuario)
VALUES (9998,
        'DNI',
        '35513987',
        'Julian',
        'Velazquez',
        '20/01/1995',
        'PACIENTE');

INSERT INTO main.Usuario
(Usuario_Id,
 Tipo_Documento,
 Numero_Documento,
 Nombre,
 Apellido,
 Fecha_Nacimiento,
 Tipo_Usuario)
VALUES (9997,
        'DNI',
        '26145124',
        'Romina',
        'Molina',
        '10/10/1985',
        'PACIENTE');
