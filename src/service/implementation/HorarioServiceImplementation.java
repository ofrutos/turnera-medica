package service.implementation;

import java.util.ArrayList;
import java.util.List;
import model.dao.HorarioDao;
import model.dao.exception.DAOException;
import model.pojo.HorarioPojo;
import service.HorarioService;
import service.excpetion.ServiceException;

/**
 * Clase que administra los servicios de Horarios.
 */
public class HorarioServiceImplementation implements HorarioService {

  private final HorarioDao horarioDao;
  private List<HorarioPojo> horarioPojoList;

  /**
   * Crea un servicio de Horarios.
   *
   * @param horarioDao DAO de Horarios.
   */
  public HorarioServiceImplementation(final HorarioDao horarioDao) {
    this.horarioDao = horarioDao;
    this.horarioPojoList = new ArrayList<>();
  }

  @Override
  public List<HorarioPojo> obtenerTodosHorarios() throws ServiceException {
    try {
      if (this.horarioPojoList.isEmpty()) {
        this.horarioPojoList = this.horarioDao.getAllHorarios();
      }
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
    return this.horarioPojoList;
  }
}
