package model.constants.table;

public enum ReporteTableConstants {

  MEDICO(1),
  CANTIDAD_CONSULTAS(2),
  TOTAL_RECAUDADO(3);

  private final Integer position;

  ReporteTableConstants(final Integer position) {
    this.position = position;
  }

  public Integer getPosition() {
    return this.position;
  }
}
