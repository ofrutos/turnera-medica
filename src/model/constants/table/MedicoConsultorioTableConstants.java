package model.constants.table;

public enum MedicoConsultorioTableConstants {
  CONSULTORIO(1),
  FECHA_INICIO(2),
  FECHA_FIN(3),
  HORARIO(4);

  private final Integer position;

  MedicoConsultorioTableConstants(Integer position) {
    this.position = position;
  }

  public Integer getPosition() {
    return this.position;
  }
}
