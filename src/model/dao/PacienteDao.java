package model.dao;

import java.util.List;
import java.util.Map;
import model.dao.exception.DAOException;
import model.pojo.PacientePojo;

/**
 * Interfaz que administra todas las declaraciones para el DAO de un paciente.
 */
public interface PacienteDao {

  /**
   * Crea un paciente.
   *
   * @param pacientePojo Paciente a crear.
   * @throws DAOException Si no se pudo crear un paciente u ocurrió un error en la base de datos.
   */
  void createPaciente(final PacientePojo pacientePojo) throws DAOException;

  /**
   * Actualiza un paciente.
   *
   * @param pacientePojo Paciente a actualizar.
   * @throws DAOException Si no se pudo actualizar un paciente u ocurrió un error en la base de
   *                      datos.
   */
  void updatePaciente(final PacientePojo pacientePojo) throws DAOException;

  /**
   * Elimina un paciente.
   *
   * @param pacientePojo Paciente a eliminar.
   * @throws DAOException Si no se pudo eliminar un paciente u ocurrió un error en la base de
   *                      datos.
   */
  void deletePaciente(final PacientePojo pacientePojo) throws DAOException;

  /**
   * Lista todos los pacientes.
   *
   * @return Todos los pacientes.
   */
  Map<Integer, PacientePojo> getAllPacientes() throws DAOException;

  /**
   * Obtiene un paciente.
   *
   * @param pacienteId Id del paciente.
   * @return Paciente.
   */
  PacientePojo getPaciente(final Integer pacienteId) throws DAOException;

  /**
   * Obtiene el nombre de las columnas de la tabla de Pacientes.
   *
   * @return Nombre de las columnas.
   */
  List<String> getPacienteNombreColumna();
}
