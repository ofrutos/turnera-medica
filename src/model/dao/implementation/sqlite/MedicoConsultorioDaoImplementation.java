package model.dao.implementation.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import model.constants.table.MedicoConsultorioTableConstants;
import model.dao.MedicoConsultorioDao;
import model.dao.exception.DAOException;
import model.dao.utils.LabelUtils;
import model.jdbc.DBManager;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoConsultorioPojoKey;
import model.util.DateUtils;
import org.apache.commons.lang3.EnumUtils;

/**
 * Clase que administra los DAO para los consultorios de los medicos en una base SQLite.
 */
public class MedicoConsultorioDaoImplementation implements MedicoConsultorioDao {

  /**
   * Declaración de INSERT de la tabla de consultorios de medicos.
   */
  private static final String INSERT_MEDICO_CONSULTORIO = "INSERT INTO Medico_Consultorio (Usuario_Id, Consultorio_Id, Fecha_Inicio, Fecha_Fin, Horario_Id) VALUES (?, ?, ?, ?, ?)";

  /**
   * Declaración de UPDATE de la tabla de consultorios de medicos.
   */
  private static final String UPDATE_MEDICO_CONSULTORIO = "UPDATE Medico_Consultorio SET Fecha_Fin = ? WHERE Usuario_Id = ? AND Consultorio_Id = ? AND Fecha_Inicio = ? AND Horario_Id = ?";

  /**
   * Declaración de SELECT de la tabla de consultorios de medicos.
   */
  private static final String SELECT_ALL_MEDICO_CONSULTORIOS = "SELECT Mc.Usuario_Id, Mc.Consultorio_Id Consultorio, C.Nombre, Mc.Fecha_Inicio, Mc.Fecha_Fin, Mc.Horario_Id Horario, H.Hora_Inicio, H.Hora_Fin FROM Medico_Consultorio Mc, Consultorio C, Horarios H WHERE Mc.Consultorio_Id = C.Consultorio_Id AND Mc.Horario_Id = H.Horario_Id";

  /**
   * Conexión con la base de datos.
   */
  private Connection connection;

  @Override
  public void createMedicoConsultorio(final MedicoConsultorioPojo medicoConsultorioPojo)
      throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(INSERT_MEDICO_CONSULTORIO);
      statement.setInt(1, medicoConsultorioPojo.getMedicoConsultorioPojoKey().getUsuarioId());
      statement.setInt(2, medicoConsultorioPojo.getMedicoConsultorioPojoKey().getConsultorioId());
      statement.setString(3, DateUtils.parseDate(medicoConsultorioPojo.getFechaInicio()));
      statement.setString(4, DateUtils.parseDate(medicoConsultorioPojo.getFechaFin()));
      statement.setInt(5, medicoConsultorioPojo.getMedicoConsultorioPojoKey().getHorarioId());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows created.");
      }

      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  @Override
  public void updateMedicoConsultorio(final MedicoConsultorioPojo medicoConsultorioPojo)
      throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(UPDATE_MEDICO_CONSULTORIO);
      statement.setString(1, DateUtils.parseDate(medicoConsultorioPojo.getFechaFin()));
      statement.setInt(2, medicoConsultorioPojo.getMedicoConsultorioPojoKey().getUsuarioId());
      statement.setInt(3, medicoConsultorioPojo.getMedicoConsultorioPojoKey().getConsultorioId());
      statement.setString(4, DateUtils.parseDate(medicoConsultorioPojo.getFechaInicio()));
      statement.setInt(5, medicoConsultorioPojo.getMedicoConsultorioPojoKey().getHorarioId());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows updated.");
      }

      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  @Override
  public Map<MedicoConsultorioPojoKey, MedicoConsultorioPojo> getAllMedicoConsultorio()
      throws DAOException {
    Map<MedicoConsultorioPojoKey, MedicoConsultorioPojo> medicoConsultorioPojos = new HashMap<>();
    this.connection = DBManager.connect();
    try {
      PreparedStatement preparedStatement = this.connection
          .prepareStatement(SELECT_ALL_MEDICO_CONSULTORIOS);

      ResultSet resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        medicoConsultorioPojos.put(new MedicoConsultorioPojoKey(resultSet.getInt(1),
                resultSet.getInt(2),
                DateUtils.getDateFormat().parse(resultSet.getString(4)),
                resultSet.getInt(6)),
            new MedicoConsultorioPojo(resultSet.getInt(1),
                resultSet.getInt(2),
                resultSet.getString(3),
                DateUtils.getDateFormat().parse(resultSet.getString(4)),
                DateUtils.getDateFormat().parse(resultSet.getString(5)),
                resultSet.getInt(6),
                resultSet.getString(7),
                resultSet.getString(8)));
      }
    } catch (final SQLException | ParseException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
    return medicoConsultorioPojos;
  }

  @Override
  public List<String> getMedicoConsultorioNombreColumna() {
    this.connection = DBManager.connect();
    List<String> cabecera = new ArrayList<>();
    try {
      PreparedStatement statement = this.connection
          .prepareStatement(SELECT_ALL_MEDICO_CONSULTORIOS);

      ResultSetMetaData meta = statement.getMetaData();
      int cantidadColumnas = statement.getMetaData().getColumnCount();

      for (int columna = 1; columna <= cantidadColumnas; columna++) {
        cabecera.add(meta.getColumnName(columna));
      }

      cabecera = cabecera.stream()
          .filter(this::filtrarColumnasPrivadas)
          .map(LabelUtils::normalizarLabel)
          .collect(Collectors.toList());
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
    } finally {
      DBManager.handleClose(this.connection);
    }

    return cabecera;
  }

  /**
   * Indica las columnas que no deben ser visualizadas.
   *
   * @param columna Nombre de la columna.
   * @return <b>true</b> si la columna debe ser visualizada.<br><b>false</b> si no debe ser
   * visualizada.
   */
  private boolean filtrarColumnasPrivadas(final String columna) {
    return EnumUtils.getEnumIgnoreCase(MedicoConsultorioTableConstants.class, columna) != null;
  }
}
