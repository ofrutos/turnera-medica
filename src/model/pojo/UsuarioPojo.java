package model.pojo;

import java.util.Date;
import java.util.Objects;
import model.constants.usuario.TipoDocumento;

/**
 * Clase que administra un usuario.
 */
public class UsuarioPojo {

  /**
   * Id del usuario.
   */
  private Integer usuarioId;
  /**
   * Tipo de documento.
   */
  private TipoDocumento tipoDocumento;
  /**
   * Número del documento.
   */
  private String numeroDocumento;
  /**
   * Nombre del usuario.
   */
  private String nombre;
  /**
   * Apellido del usuario.
   */
  private String apellido;
  /**
   * Fecha de nacimiento del usuario.
   */
  private Date fechaNacimiento;
  /**
   * Tipo de usuario.
   */
  private TipoUsuario tipoUsuario;

  /**
   * Crea un usuario.
   */
  public UsuarioPojo() {

  }

  /**
   * Crea un usuario.
   *
   * @param usuarioId       ID del usuario.
   * @param tipoDocumento   Tipo de documento.
   * @param numeroDocumento Numero del documento.
   * @param nombre          Nombre del usuario.
   * @param apellido        Apellido del usuario.
   * @param fechaNacimiento Fecha de nacimiento.
   * @param tipoUsuario     Tipo de usuario.
   */
  public UsuarioPojo(final Integer usuarioId,
      final TipoDocumento tipoDocumento,
      final String numeroDocumento,
      final String nombre,
      final String apellido,
      final Date fechaNacimiento,
      final TipoUsuario tipoUsuario) {
    this.usuarioId = usuarioId;
    this.tipoDocumento = tipoDocumento;
    this.numeroDocumento = numeroDocumento;
    this.nombre = nombre;
    this.apellido = apellido;
    this.fechaNacimiento = fechaNacimiento;
    this.tipoUsuario = tipoUsuario;
  }

  /**
   * Crea un usuario a partir de otro usuario.
   *
   * @param usuarioPojo POJO del usuario.
   */
  public UsuarioPojo(final UsuarioPojo usuarioPojo) {
    this.usuarioId = usuarioPojo.getUsuarioId();
    this.tipoDocumento = usuarioPojo.getTipoDocumento();
    this.numeroDocumento = usuarioPojo.getNumeroDocumento();
    this.nombre = usuarioPojo.getNombre();
    this.apellido = usuarioPojo.getApellido();
    this.fechaNacimiento = usuarioPojo.getFechaNacimiento();
    this.tipoUsuario = usuarioPojo.getTipoUsuario();
  }

  public Integer getUsuarioId() {
    return usuarioId;
  }

  public void setUsuarioId(Integer usuarioId) {
    this.usuarioId = usuarioId;
  }

  public TipoDocumento getTipoDocumento() {
    return tipoDocumento;
  }

  public void setTipoDocumento(TipoDocumento tipoDocumento) {
    this.tipoDocumento = tipoDocumento;
  }

  public String getNumeroDocumento() {
    return numeroDocumento;
  }

  public void setNumeroDocumento(String numeroDocumento) {
    this.numeroDocumento = numeroDocumento;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public String getApellido() {
    return apellido;
  }

  public void setApellido(String apellido) {
    this.apellido = apellido;
  }

  public Date getFechaNacimiento() {
    return fechaNacimiento;
  }

  public void setFechaNacimiento(Date fechaNacimiento) {
    this.fechaNacimiento = fechaNacimiento;
  }

  public TipoUsuario getTipoUsuario() {
    return tipoUsuario;
  }

  public void setTipoUsuario(TipoUsuario tipoUsuario) {
    this.tipoUsuario = tipoUsuario;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UsuarioPojo that = (UsuarioPojo) o;
    return usuarioId.equals(that.usuarioId) &&
        tipoDocumento == that.tipoDocumento &&
        Objects.equals(numeroDocumento, that.numeroDocumento) &&
        nombre.equals(that.nombre) &&
        apellido.equals(that.apellido) &&
        Objects.equals(fechaNacimiento, that.fechaNacimiento) &&
        tipoUsuario == that.tipoUsuario;
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(usuarioId, tipoDocumento, numeroDocumento, nombre, apellido, fechaNacimiento,
            tipoUsuario);
  }

  @Override
  public String toString() {
    return this.usuarioId + " - " + this.nombre + " " + this.apellido;
  }
}
