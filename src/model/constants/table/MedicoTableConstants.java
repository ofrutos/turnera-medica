package model.constants.table;

public enum MedicoTableConstants {
  USUARIO_ID(1),
  TIPO_DOCUMENTO(2),
  NUMERO_DOCUMENTO(3),
  NOMBRE(4),
  APELLIDO(5),
  FECHA_NACIMIENTO(6),
  PRECIO_CONSULTA(7);

  private final Integer position;

  MedicoTableConstants(Integer position) {
    this.position = position;
  }

  public Integer getPosition() {
    return this.position;
  }
}
