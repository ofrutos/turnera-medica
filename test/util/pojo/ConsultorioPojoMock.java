package util.pojo;

import java.util.Map;
import java.util.TreeMap;
import model.pojo.ConsultorioPojo;

public class ConsultorioPojoMock {

  public static ConsultorioPojo getConsultorioUno() {
    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();
    consultorioPojo.setConsultorioId(101);
    consultorioPojo.setNombre("Consultorio 1");
    return consultorioPojo;
  }

  public static ConsultorioPojo getConsultorioDos() {
    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();
    consultorioPojo.setConsultorioId(102);
    consultorioPojo.setNombre("Consultorio 2");
    return consultorioPojo;
  }

  public static ConsultorioPojo getConsultorioTres() {
    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();
    consultorioPojo.setConsultorioId(103);
    consultorioPojo.setNombre("Consultorio 3");
    return consultorioPojo;
  }

  public static Map<Integer, ConsultorioPojo> getAllConsultorios() {
    final Map<Integer, ConsultorioPojo> consultorioPojos = new TreeMap<>();

    consultorioPojos.put(101, getConsultorioUno());
    consultorioPojos.put(102, getConsultorioDos());

    return consultorioPojos;
  }
}
