package service.implementation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import model.dao.ConsultaDao;
import model.dao.exception.DAOException;
import model.pojo.ConsultaPojo;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoPojo;
import model.pojo.PacientePojo;
import model.util.DateUtils;
import service.ConsultaService;
import service.excpetion.ServiceException;

/**
 * Clase que implementa los servicios de consultas.
 */
public class ConsultaServiceImplementation implements ConsultaService {

  private final ConsultaDao consultaDao;
  private Map<Integer, ConsultaPojo> consultaPojoMap;


  /**
   * Crea una implementación del servicio de consultas.
   *
   * @param consultaDao DAO de consultas.
   */
  public ConsultaServiceImplementation(final ConsultaDao consultaDao) {
    this.consultaDao = consultaDao;
    this.consultaPojoMap = new TreeMap<>();
  }

  @Override
  public void crearConsulta(final ConsultaPojo consultaPojo,
      final List<MedicoConsultorioPojo> medicoConsultorioPojos) throws ServiceException {
    try {
      this.checkConsulta(consultaPojo, medicoConsultorioPojos);

      this.consultaDao.createConsulta(consultaPojo);
      this.consultaPojoMap.put(consultaPojo.getConsultaId(), consultaPojo);
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public List<ConsultaPojo> obtenerConsultas() throws ServiceException {
    this.checkEmptyConsultaPojoMap();

    return this.consultaPojoMap.values()
        .stream()
        .sorted(Comparator.comparing(ConsultaPojo::getConsultaId))
        .collect(Collectors.toList());
  }

  @Override
  public List<ConsultaPojo> obtenerConsultas(final PacientePojo pacientePojo)
      throws ServiceException {
    this.checkEmptyConsultaPojoMap();

    return this.consultaPojoMap.values()
        .stream()
        .filter(consultaPojo -> consultaPojo.getPacientePojo().getUsuarioId()
            .equals(pacientePojo.getUsuarioId()))
        .sorted(Comparator.comparing(ConsultaPojo::getConsultaId))
        .collect(Collectors.toList());
  }

  @Override
  public List<ConsultaPojo> obtenerConsultas(final MedicoPojo medicoPojo) throws ServiceException {
    this.checkEmptyConsultaPojoMap();

    return this.consultaPojoMap.values()
        .stream()
        .filter(consultaPojo -> consultaPojo.getMedicoPojo().getUsuarioId()
            .equals(medicoPojo.getUsuarioId()))
        .sorted(Comparator.comparing(ConsultaPojo::getConsultaId))
        .collect(Collectors.toList());
  }

  @Override
  public List<String> obtenerNombreColumnasConsulta() {
    return this.consultaDao.getConsultaNombreColumna();
  }

  @Override
  public List<String> obtenerNombreColumnasReporte() {
    return Arrays.asList("Medico", "Cantidad Consultas", "Total Recaudado");
  }

  @Override
  public List<MedicoPojo> obtenerReportes(final Date fechaDesde,
      final Date fechaHasta) throws ServiceException {
    this.checkEmptyConsultaPojoMap();

    final Map<Integer, MedicoPojo> reportePojos = new HashMap<>();

    this.consultaPojoMap.values()
        .stream()
        .filter(consultaPojo -> DateUtils
            .isDateBetween(fechaDesde, fechaHasta, consultaPojo.getFecha()))
        .forEach(consultaPojo -> {
          resetReporte(consultaPojo.getMedicoPojo());
          procesarMedicoReporte(reportePojos, consultaPojo);
        });

    return reportePojos.values()
        .stream()
        .sorted(Comparator.comparing(MedicoPojo::getUsuarioId))
        .collect(Collectors.toList());
  }

  /**
   * Resetea la información de los medicos para el reporte.
   *
   * @param medicoPojo POJO del medico.
   */
  private void resetReporte(final MedicoPojo medicoPojo) {
    medicoPojo.resetCantidadConsultas();
    medicoPojo.resetTotalRecaudado();
  }

  /**
   * Procesa las consultas de los medicos para poder generar un reporte.
   *
   * @param reportePojos Lista del Reporte.
   * @param consultaPojo POJO de la Consulta.
   */
  private void procesarMedicoReporte(final Map<Integer, MedicoPojo> reportePojos,
      final ConsultaPojo consultaPojo) {
    if (!reportePojos.containsKey(consultaPojo.getMedicoPojo().getUsuarioId())) {
      reportePojos.put(consultaPojo.getMedicoPojo().getUsuarioId(), consultaPojo.getMedicoPojo());
    }
    final MedicoPojo currentMedico = reportePojos
        .get(consultaPojo.getMedicoPojo().getUsuarioId());
    currentMedico.increaseCantidadConsultas();
    currentMedico
        .addTotalRecaudado(
            Double.parseDouble(consultaPojo.getPrecioFinal()
                .replace(".", "")
                .replace(",", ".")));
  }

  /**
   * Verifica si se encuentran cargadas las consultas en memoria.
   */
  private void checkEmptyConsultaPojoMap() throws ServiceException {
    if (this.consultaPojoMap.isEmpty()) {
      try {
        this.consultaPojoMap = this.consultaDao.getAllConsultas();
      } catch (final DAOException e) {
        throw new ServiceException(e.getMessage());
      }
    }
  }

  /**
   * Controla que los datos ingresados para la consulta sean validos.
   *
   * @param consultaPojo           POJO de la consulta.
   * @param medicoConsultorioPojos POJOs de los consultorios del medico.
   * @throws ServiceException Si ocurre un error al momento de validar la información.
   */
  private void checkConsulta(final ConsultaPojo consultaPojo,
      final List<MedicoConsultorioPojo> medicoConsultorioPojos) throws ServiceException {
    this.checkFechaAtencion(consultaPojo, medicoConsultorioPojos);
    this.checkDisponibilidadMedico(consultaPojo);
    this.checkDisponibilidadPaciente(consultaPojo);
  }

  /**
   * Controla si la fecha indicada para la consulta se encuentra disponible.
   *
   * @param consultaPojo           POJO de la consulta.
   * @param medicoConsultorioPojos POJOs de los consultorios de un medico.
   * @throws ServiceException Si la fecha indicada no es valida.
   */
  private void checkFechaAtencion(final ConsultaPojo consultaPojo,
      final List<MedicoConsultorioPojo> medicoConsultorioPojos) throws ServiceException {
    if (medicoConsultorioPojos.stream()
        .noneMatch(
            medicoConsultorioPojo -> medicoConsultorioPojo.getConsultorioPojo().getConsultorioId()
                .equals(consultaPojo.getConsultorioPojo().getConsultorioId())
                && medicoConsultorioPojo.getHorarioPojo().getHorarioId()
                .equals(consultaPojo.getHorarioPojo().getHorarioId())
                && DateUtils.isDateBetween(medicoConsultorioPojo.getFechaInicio(),
                medicoConsultorioPojo.getFechaFin(),
                consultaPojo.getFecha()))) {
      throw new ServiceException(
          "La fecha seleccionada para el consultorio no se encuentra disponible.");
    }
  }

  /**
   * Controla la disponibilidad de un medico en esa fecha y horario.
   *
   * @param consultaPojo POJO de la consulta.
   * @throws ServiceException Si el medico ya posee un turno en esa fecha y horario.
   */
  private void checkDisponibilidadMedico(final ConsultaPojo consultaPojo) throws ServiceException {
    if (this.consultaPojoMap.values()
        .stream()
        .anyMatch(actualConsultaPojo -> actualConsultaPojo.getMedicoPojo().getUsuarioId()
            .equals(consultaPojo.getMedicoPojo().getUsuarioId())
            && actualConsultaPojo.getConsultorioPojo().getConsultorioId()
            .equals(consultaPojo.getConsultorioPojo().getConsultorioId())
            && actualConsultaPojo.getFecha().getTime() == consultaPojo.getFecha().getTime()
            && actualConsultaPojo.getHorarioPojo().getHorarioId()
            .equals(consultaPojo.getHorarioPojo().getHorarioId()))) {
      throw new ServiceException("El medico ya posee un turno en ese horario.");
    }
  }

  /**
   * Controla la disponibilidad de un paciente en ese horario.
   *
   * @param consultaPojo POJO de la consulta.
   * @throws ServiceException Si el paciente ya posee un turno en esa fecha y horario.
   */
  private void checkDisponibilidadPaciente(final ConsultaPojo consultaPojo)
      throws ServiceException {
    if (this.consultaPojoMap.values()
        .stream()
        .anyMatch(actualConsultaPojo -> actualConsultaPojo.getPacientePojo().getUsuarioId()
            .equals(consultaPojo.getPacientePojo().getUsuarioId())
            && actualConsultaPojo.getFecha().getTime() == consultaPojo.getFecha().getTime()
            && actualConsultaPojo.getHorarioPojo().getHorarioId()
            .equals(consultaPojo.getHorarioPojo().getHorarioId()))) {
      throw new ServiceException("El paciente ya posee un turno en ese horario.");
    }
  }
}
