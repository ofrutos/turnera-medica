package service.implementation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static util.pojo.MedicoPojoMock.getAllMedicos;
import static util.pojo.MedicoPojoMock.getMedicoUno;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import model.constants.usuario.TipoDocumento;
import model.dao.MedicoDao;
import model.dao.exception.DAOException;
import model.dao.implementation.sqlite.MedicoDaoImplementation;
import model.pojo.MedicoPojo;
import model.pojo.TipoUsuario;
import model.pojo.UsuarioPojo;
import model.util.DateUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import service.MedicoService;
import service.excpetion.ServiceException;

public class MedicoServiceImplementationTest {

  @Rule
  public MockitoRule initRule = MockitoJUnit.rule();

  @Mock
  private final MedicoDao medicoDao = new MedicoDaoImplementation();

  private MedicoService medicoService;

  @Before
  public void setup() {
    this.medicoService = new MedicoServiceImplementation(this.medicoDao);
  }

  @Ignore
  public void crearMedico_Ok() throws DAOException, ServiceException {
    doNothing().when(this.medicoDao).createMedico(isA(MedicoPojo.class));

    final MedicoPojo medicoPojo = new MedicoPojo();

    this.medicoService.crearMedico(medicoPojo);
  }

  @Test(expected = ServiceException.class)
  public void crearMedico_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.medicoDao).createMedico(isA(MedicoPojo.class));

    final MedicoPojo medicoPojo = new MedicoPojo();

    this.medicoService.crearMedico(medicoPojo);
  }

  @Ignore
  public void actualizarMedico_Ok() throws DAOException, ServiceException {
    doNothing().when(this.medicoDao).updateMedico(isA(MedicoPojo.class));

    final MedicoPojo medicoPojo = new MedicoPojo();

    this.medicoService.actualizarMedico(medicoPojo);
  }

  @Test(expected = ServiceException.class)
  public void actualizarMedico_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.medicoDao).updateMedico(isA(MedicoPojo.class));

    final MedicoPojo medicoPojo = new MedicoPojo();

    this.medicoService.actualizarMedico(medicoPojo);
  }

  @Ignore
  public void eliminarMedico_Ok() throws DAOException, ServiceException {
    doNothing().when(this.medicoDao).deleteMedico(isA(MedicoPojo.class));

    final MedicoPojo medicoPojo = new MedicoPojo();

    this.medicoService.eliminarMedico(medicoPojo);
  }

  @Test(expected = ServiceException.class)
  public void eliminarMedico_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.medicoDao).deleteMedico(isA(MedicoPojo.class));

    final MedicoPojo medicoPojo = new MedicoPojo();

    this.medicoService.eliminarMedico(medicoPojo);
  }

  @Ignore
  public void consultarMedico_Ok() throws DAOException, ParseException {
    when(this.medicoDao.getMedico(isA(Integer.class))).thenReturn(getMedicoUno());

    final MedicoPojo expectedMedicoPojo = new MedicoPojo();
    expectedMedicoPojo.setUsuarioId(9996);
    expectedMedicoPojo.setTipoDocumento(TipoDocumento.DNI);
    expectedMedicoPojo.setNumeroDocumento("24567190");
    expectedMedicoPojo.setNombre("Miguel");
    expectedMedicoPojo.setApellido("Saenz");
    expectedMedicoPojo.setFechaNacimiento(DateUtils.getDateFormat().parse("01/01/1990"));
    expectedMedicoPojo.setTipoUsuario(TipoUsuario.MEDICO);
    expectedMedicoPojo.setPrecioConsulta("640");

    final int medicoId = 9996;
    final MedicoPojo actualMedicoPojo = this.medicoService.obtenerMedico(medicoId);

    assertEquals(expectedMedicoPojo, actualMedicoPojo);
  }

  @Test
  public void consultarMedico_Error() throws DAOException {
    when(this.medicoDao.getMedico(isA(Integer.class))).thenThrow(DAOException.class);

    final int medicoId = 1;
    final MedicoPojo actualMedicoPojo = this.medicoService.obtenerMedico(medicoId);

    assertNull(actualMedicoPojo);
  }

  @Test
  public void consultarAllMedicos_Ok() throws DAOException, ServiceException, ParseException {
    when(this.medicoDao.getAllMedicos()).thenReturn(getAllMedicos());

    final List<MedicoPojo> expectedMedicosPojo = new ArrayList<>();

    final MedicoPojo expectedMedicoPojo1 = new MedicoPojo();
    expectedMedicoPojo1.setUsuarioId(9996);
    expectedMedicoPojo1.setTipoDocumento(TipoDocumento.DNI);
    expectedMedicoPojo1.setNumeroDocumento("24567190");
    expectedMedicoPojo1.setNombre("Miguel");
    expectedMedicoPojo1.setApellido("Saenz");
    expectedMedicoPojo1.setFechaNacimiento(DateUtils.getDateFormat().parse("01/01/1990"));
    expectedMedicoPojo1.setTipoUsuario(TipoUsuario.MEDICO);
    expectedMedicoPojo1.setPrecioConsulta("640");

    expectedMedicosPojo.add(expectedMedicoPojo1);

    final MedicoPojo expectedMedicoPojo2 = new MedicoPojo();
    expectedMedicoPojo2.setUsuarioId(9995);
    expectedMedicoPojo2.setTipoDocumento(TipoDocumento.DNI);
    expectedMedicoPojo2.setNumeroDocumento("25513987");
    expectedMedicoPojo2.setNombre("Jorge");
    expectedMedicoPojo2.setApellido("Velazquez");
    expectedMedicoPojo2.setFechaNacimiento(DateUtils.getDateFormat().parse("20/01/1995"));
    expectedMedicoPojo2.setTipoUsuario(TipoUsuario.MEDICO);
    expectedMedicoPojo2.setPrecioConsulta("720");

    expectedMedicosPojo.add(expectedMedicoPojo2);

    expectedMedicosPojo.sort(Comparator.comparing(UsuarioPojo::getUsuarioId));

    final List<MedicoPojo> actualMedicosPojo = this.medicoService.obtenerTodosMedico();

    assertEquals(expectedMedicosPojo, actualMedicosPojo);
  }

  @Test(expected = ServiceException.class)
  public void consultarAllMedicos_Error() throws DAOException, ServiceException {
    when(this.medicoDao.getAllMedicos()).thenThrow(DAOException.class);

    this.medicoService.obtenerTodosMedico();
  }
}
