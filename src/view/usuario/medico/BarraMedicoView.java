package view.usuario.medico;

import javax.swing.JMenuItem;
import view.GeneralBarraView;
import view.constants.ViewLabels;

/**
 * Clase que administra los componentes visuales de la barra de menú de la ventana de los medicos.
 */
public class BarraMedicoView extends GeneralBarraView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private JMenuItem medicoLbl;

  /**
   * Crea la vista de la barra de medico.
   */
  public BarraMedicoView() {
    super();
  }

  /**
   * Inicializa los componentes visuales.
   */
  public void initView() {
    super.initLabels();

    this.medicoLbl = new JMenuItem(ViewLabels.MEDICO.getLabel());
    super.getNuevoLbl().add(this.medicoLbl);
  }

  /**
   * Devuelve el label del medico.
   *
   * @return Label del medico.
   */
  public JMenuItem getMedicoLbl() {
    return this.medicoLbl;
  }
}
