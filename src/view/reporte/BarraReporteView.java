package view.reporte;

import javax.swing.JMenuItem;
import view.GeneralBarraView;
import view.constants.ViewLabels;

/**
 * Clase que administra la barra de la vista de reportes.
 */
public class BarraReporteView extends GeneralBarraView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private JMenuItem rangoLbl;

  /**
   * Crea la vista de la barra de reportes.
   */
  public BarraReporteView() {
    super();
  }

  public void initView() {
    super.initLabels();

    this.rangoLbl = new JMenuItem(ViewLabels.RANGO_FECHAS.getLabel());
    super.getNuevoLbl().add(this.rangoLbl);
  }

  public JMenuItem getRangoLbl() {
    return this.rangoLbl;
  }
}
