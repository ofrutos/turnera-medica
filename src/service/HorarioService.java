package service;

import java.util.List;
import model.pojo.HorarioPojo;
import service.excpetion.ServiceException;

/**
 * Interfaz que administra los servicios de Horarios.
 */
public interface HorarioService {

  /**
   * Obtiene todos los horarios.
   *
   * @return Todos los horarios.
   * @throws ServiceException Si ocurre un error al obtener los horarios.
   */
  List<HorarioPojo> obtenerTodosHorarios() throws ServiceException;
}
