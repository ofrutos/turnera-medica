package model.dao.implementation.sqlite;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import model.constants.table.MedicoTableConstants;
import model.constants.usuario.TipoDocumento;
import model.dao.MedicoDao;
import model.dao.exception.DAOException;
import model.dao.utils.LabelUtils;
import model.jdbc.DBManager;
import model.pojo.MedicoPojo;
import model.pojo.TipoUsuario;
import model.util.DateUtils;
import org.apache.commons.lang3.EnumUtils;

/**
 * Clase que administra los DAO para los médicos en una base SQLite. <br>
 */
public class MedicoDaoImplementation implements MedicoDao {

  /**
   * Declaración de SELECT (All) de la tabla de médicos. <br>
   */
  private static final String SELECT_ALL_MEDICOS = "SELECT * FROM Usuario WHERE Tipo_Usuario = ?";
  /**
   * Obtiene un único médico.
   */
  private static final String SELECT_ONE_MEDICO = "SELECT * FROM Usuario WHERE Usuario_Id = ? AND Tipo_Usuario = ?";
  /**
   * Declaración de INSERT de la tabla de médicos. <br>
   */
  private static final String INSERT_MEDICO = "INSERT INTO Usuario (Tipo_Documento, Numero_Documento, Nombre, Apellido, Fecha_Nacimiento, Tipo_Usuario, Precio_Consulta) VALUES (?, ?, ?, ?, ?, ?, ?)";

  /**
   * Declaración de UPDATE de la tabla de médicos. <br>
   */
  private static final String UPDATE_MEDICO = "UPDATE Usuario SET Tipo_Documento = ?, Numero_Documento = ?, Nombre = ?, Apellido = ?, Fecha_Nacimiento = ?, Tipo_Usuario = ?, Precio_Consulta = ? WHERE Usuario_Id = ?";

  /**
   * Declaración de DELETE de la tabla de médicos. <br>
   */
  private static final String DELETE_MEDICO = "DELETE FROM Usuario WHERE Usuario_Id = ?";

  /**
   * Conexión con la base de datos. <br>
   */
  private Connection connection;

  public void createMedico(MedicoPojo medicoPojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(INSERT_MEDICO);
      statement.setString(1, medicoPojo.getTipoDocumento().name());
      statement.setString(2, medicoPojo.getNumeroDocumento());
      statement.setString(3, medicoPojo.getNombre());
      statement.setString(4, medicoPojo.getApellido());
      statement.setString(5, DateUtils.parseDate(medicoPojo.getFechaNacimiento()));
      statement.setString(6, medicoPojo.getTipoUsuario().name());
      statement.setString(7, medicoPojo.getPrecioConsulta());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows created.");
      }

      ResultSet resultSet = statement.getGeneratedKeys();

      if (resultSet != null && resultSet.next()) {
        medicoPojo.setUsuarioId(resultSet.getInt(1));
      }

      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public void updateMedico(MedicoPojo medicoPojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(UPDATE_MEDICO);
      statement.setString(1, medicoPojo.getTipoDocumento().name());
      statement.setString(2, medicoPojo.getNumeroDocumento());
      statement.setString(3, medicoPojo.getNombre());
      statement.setString(4, medicoPojo.getApellido());
      statement.setString(5, DateUtils.parseDate(medicoPojo.getFechaNacimiento()));
      statement.setString(6, medicoPojo.getTipoUsuario().name());
      statement.setString(7, medicoPojo.getPrecioConsulta());
      statement.setInt(8, medicoPojo.getUsuarioId());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows updated.");
      }

      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public void deleteMedico(MedicoPojo medicoPojo) throws DAOException {
    this.connection = DBManager.connect();
    try {
      PreparedStatement statement = this.connection.prepareStatement(DELETE_MEDICO);
      statement.setInt(1, medicoPojo.getUsuarioId());

      if (statement.executeUpdate() <= 0) {
        throw new SQLException("No rows deleted.");
      }

      this.connection.commit();
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
  }

  public Map<Integer, MedicoPojo> getAllMedicos() throws DAOException {
    Map<Integer, MedicoPojo> medicoPojos = new TreeMap<>();
    this.connection = DBManager.connect();
    try {
      PreparedStatement preparedStatement = this.connection.prepareStatement(SELECT_ALL_MEDICOS);
      preparedStatement.setString(1, TipoUsuario.MEDICO.toString());

      ResultSet resultSet = preparedStatement.executeQuery();
      while (resultSet.next()) {
        medicoPojos.put(resultSet.getInt(1),
            new MedicoPojo(resultSet.getInt(1),
                TipoDocumento.valueOf(resultSet.getString(2)),
                resultSet.getString(3),
                resultSet.getString(4),
                resultSet.getString(5),
                DateUtils.getDateFormat().parse(resultSet.getString(6)),
                TipoUsuario.valueOf(resultSet.getString(7)),
                resultSet.getString(8))
        );
      }
    } catch (final SQLException | ParseException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
    return medicoPojos;
  }

  public List<String> getMedicoNombreColumna() {
    this.connection = DBManager.connect();
    List<String> cabecera = new ArrayList<>();
    try {
      PreparedStatement statement = this.connection.prepareStatement(SELECT_ONE_MEDICO);
      // Solo nos interesa conseguir la cabecera.
      statement.setInt(1, 0);
      statement.setString(1, TipoUsuario.MEDICO.toString());

      ResultSetMetaData meta = statement.getMetaData();
      int cantidadColumnas = statement.getMetaData().getColumnCount();

      for (int columna = 1; columna <= cantidadColumnas; columna++) {
        cabecera.add(meta.getColumnName(columna));
      }

      cabecera = cabecera.stream()
          .filter(this::filtrarColumnasPrivadas)
          .map(LabelUtils::normalizarLabel)
          .collect(Collectors.toList());
    } catch (final SQLException e) {
      DBManager.handleRollback(this.connection);
    } finally {
      DBManager.handleClose(this.connection);
    }

    return cabecera;
  }

  @Override
  public MedicoPojo getMedico(final Integer medicoId) throws DAOException {
    this.connection = DBManager.connect();
    MedicoPojo medicoPojo = null;
    try {
      PreparedStatement preparedStatement = this.connection.prepareStatement(SELECT_ONE_MEDICO);
      preparedStatement.setInt(1, medicoId);
      preparedStatement.setString(2, TipoUsuario.MEDICO.name());

      ResultSet resultSet = preparedStatement.executeQuery();
      if (resultSet.next()) {
        medicoPojo = new MedicoPojo();
        medicoPojo.setUsuarioId(resultSet.getInt(1));
        medicoPojo.setTipoDocumento(TipoDocumento.valueOf(resultSet.getString(2)));
        medicoPojo.setNumeroDocumento(resultSet.getString(3));
        medicoPojo.setNombre(resultSet.getString(4));
        medicoPojo.setApellido(resultSet.getString(5));
        medicoPojo.setFechaNacimiento(DateUtils.getDateFormat().parse(resultSet.getString(6)));
        medicoPojo.setTipoUsuario(TipoUsuario.valueOf(resultSet.getString(7)));
        medicoPojo.setPrecioConsulta(resultSet.getString(8));
      }
    } catch (final SQLException | ParseException e) {
      DBManager.handleRollback(this.connection);
      throw new DAOException(e.getMessage());
    } finally {
      DBManager.handleClose(this.connection);
    }
    return medicoPojo;
  }

  /**
   * Indica las columnas que no deben ser visualizadas.
   *
   * @param columna Nombre de la columna.
   * @return <b>true</b> si la columna debe ser visualizada.<br><b>false</b> si no debe ser
   * visualizada.
   */
  public boolean filtrarColumnasPrivadas(final String columna) {
    return EnumUtils.getEnumIgnoreCase(MedicoTableConstants.class, columna) != null;
  }
}
