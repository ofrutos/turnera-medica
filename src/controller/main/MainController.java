package controller.main;

import controller.InitializableController;
import controller.ViewableController;
import controller.consulta.ConsultaController;
import controller.consultorio.ConsultorioController;
import controller.medicoconsultorio.MedicoConsultorioController;
import controller.reporte.ReporteController;
import controller.usuario.medico.MedicoController;
import controller.usuario.paciente.PacienteController;
import model.dao.ConsultaDao;
import model.dao.ConsultorioDao;
import model.dao.HorarioDao;
import model.dao.MedicoConsultorioDao;
import model.dao.MedicoDao;
import model.dao.PacienteDao;
import model.dao.implementation.sqlite.ConsultaDaoImplementation;
import model.dao.implementation.sqlite.ConsultorioDaoImplementation;
import model.dao.implementation.sqlite.HorarioDaoImplementation;
import model.dao.implementation.sqlite.MedicoConsultorioDaoImplementation;
import model.dao.implementation.sqlite.MedicoDaoImplementation;
import model.dao.implementation.sqlite.PacienteDaoImplementation;
import service.ConsultaService;
import service.ConsultorioService;
import service.HorarioService;
import service.MedicoConsultorioService;
import service.MedicoService;
import service.PacienteService;
import service.implementation.ConsultaServiceImplementation;
import service.implementation.ConsultorioServiceImplementation;
import service.implementation.HorarioServiceImplementation;
import service.implementation.MedicoConsultorioServiceImplementation;
import service.implementation.MedicoServiceImplementation;
import service.implementation.PacienteServiceImplementation;
import view.consulta.BarraConsultaView;
import view.consulta.ConsultaView;
import view.consultorio.BarraConsultorioView;
import view.consultorio.ConsultorioView;
import view.main.MainView;
import view.medicoconsultorio.BarraMedicoConsultorioView;
import view.medicoconsultorio.MedicoConsultorioView;
import view.message.MessageView;
import view.reporte.BarraReporteView;
import view.reporte.ReporteView;
import view.usuario.medico.BarraMedicoView;
import view.usuario.medico.MedicoView;
import view.usuario.paciente.BarraPacienteView;
import view.usuario.paciente.PacienteView;

/**
 * Clase que administra el controlador de la vista del frame principal.
 */
public class MainController implements InitializableController, ViewableController {

  private final MainView mainView;

  private final BarraMainController barraMainController;

  private final MedicoController medicoController;
  private final PacienteController pacienteController;
  private final ConsultorioController consultorioController;
  private final MedicoConsultorioController medicoConsultorioController;
  private final ConsultaController consultaController;
  private final ReporteController reporteController;

  /**
   * Crea el controlador principal.
   */
  public MainController() {
    this.mainView = new MainView();

    final MessageView messageView = new MessageView();

    final MedicoDao medicoDao = new MedicoDaoImplementation();
    final ConsultorioDao consultorioDao = new ConsultorioDaoImplementation();
    final PacienteDao pacienteDao = new PacienteDaoImplementation();
    final HorarioDao horarioDao = new HorarioDaoImplementation();
    final MedicoConsultorioDao medicoConsultorioDao = new MedicoConsultorioDaoImplementation();
    final ConsultaDao consultaDao = new ConsultaDaoImplementation();

    final PacienteService pacienteService = new PacienteServiceImplementation(pacienteDao);
    final MedicoService medicoService = new MedicoServiceImplementation(medicoDao);
    final ConsultorioService consultorioService = new ConsultorioServiceImplementation(
        consultorioDao);
    final HorarioService horarioService = new HorarioServiceImplementation(horarioDao);
    final MedicoConsultorioService medicoConsultorioService = new MedicoConsultorioServiceImplementation(
        medicoConsultorioDao);
    final ConsultaService consultaService = new ConsultaServiceImplementation(consultaDao);

    this.consultaController = new ConsultaController(new ConsultaView(new BarraConsultaView()),
        messageView,
        consultaService,
        pacienteService,
        medicoService,
        medicoConsultorioService);
    this.medicoConsultorioController = new MedicoConsultorioController(
        new MedicoConsultorioView(new BarraMedicoConsultorioView()),
        messageView,
        medicoConsultorioService,
        consultorioService,
        horarioService);
    this.pacienteController = new PacienteController(new PacienteView(new BarraPacienteView()),
        messageView,
        pacienteService,
        this.consultaController);
    this.medicoController = new MedicoController(new MedicoView(new BarraMedicoView()),
        messageView,
        medicoService,
        this.medicoConsultorioController,
        this.consultaController);
    this.consultorioController = new ConsultorioController(
        new ConsultorioView(new BarraConsultorioView()),
        messageView,
        consultorioService);
    this.reporteController = new ReporteController(new ReporteView(new BarraReporteView()),
        messageView,
        consultaService);

    this.barraMainController = new BarraMainController(this.mainView.getBarraMainView());
    this.barraMainController.initController();
  }

  public void initController() {
    this.initButtons();

    this.barraMainController.initController();
    this.pacienteController.initController();
    this.medicoController.initController();
    this.consultorioController.initController();
    this.medicoConsultorioController.initController();
    this.consultaController.initController();
    this.reporteController.initController();
  }

  public void initView() {
    this.mainView.getjFrame().setVisible(true);
  }

  private void initButtons() {
    this.mainView.getBtnMedico().addActionListener(e -> this.medicoController.initView());
    this.mainView.getBtnPaciente().addActionListener(e -> this.pacienteController.initView());
    this.mainView.getBtnConsultorio().addActionListener(e -> this.consultorioController.initView());
    this.mainView.getBtnConsulta().addActionListener(e -> {
      this.consultaController.setMedicoPojo(null);
      this.consultaController.setPacientePojo(null);
      this.consultaController.initView();
    });
    this.mainView.getBtnReporte().addActionListener(e -> this.reporteController.initView());
  }
}
