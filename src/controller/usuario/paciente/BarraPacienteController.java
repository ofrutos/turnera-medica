package controller.usuario.paciente;

import controller.GeneralBarraController;
import controller.InitializableController;
import view.usuario.paciente.BarraPacienteView;

/**
 * Clase que administra el controlador de la barra de menú de la ventana principal de pacientes.
 */
public class BarraPacienteController extends GeneralBarraController implements
    InitializableController {

  private final BarraPacienteView barraPacienteView;
  private final CargaPacienteController cargaPacienteController;

  /**
   * Crea el controlador de la barra de menu de la ventana principal de pacientes.
   *
   * @param barraPacienteView       Vista general de los pacientes.
   * @param cargaPacienteController Controlador de la carga de pacientes.
   */
  public BarraPacienteController(final BarraPacienteView barraPacienteView,
      final CargaPacienteController cargaPacienteController) {
    super(barraPacienteView);
    this.barraPacienteView = barraPacienteView;
    this.cargaPacienteController = cargaPacienteController;
  }

  public void initController() {
    super.initController();
    this.barraPacienteView.getPacienteLbl().addActionListener(e -> this.crearPaciente());
  }

  /**
   * Carga el formulario para crear un nuevo paciente.
   */
  private void crearPaciente() {
    this.cargaPacienteController.setPacientePojo(null);
    this.cargaPacienteController.initView();
  }
}
