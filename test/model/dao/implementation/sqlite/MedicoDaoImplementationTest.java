package model.dao.implementation.sqlite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;
import static util.jdbc.DBManagerMock.borrarMedicos;
import static util.jdbc.DBManagerMock.crearMedicos;
import static util.pojo.MedicoPojoMock.getMedicoDos;
import static util.pojo.MedicoPojoMock.getMedicoTres;
import static util.pojo.MedicoPojoMock.getMedicoUno;

import java.sql.Connection;
import java.text.ParseException;
import model.constants.usuario.TipoDocumento;
import model.dao.exception.DAOException;
import model.jdbc.DBManager;
import model.pojo.MedicoPojo;
import model.pojo.TipoUsuario;
import model.util.DateUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import util.jdbc.DBManagerMock;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DBManager.class)
public class MedicoDaoImplementationTest {

  private MedicoDaoImplementation medicoDaoImplementation;

  @BeforeClass
  public static void preTests() {
    spy(DBManager.class);
    when(DBManager.connect())
        .thenAnswer((Answer<Connection>) invocationOnMock -> DBManagerMock.connect());

    crearMedicos();
  }

  @Before
  public void setup() {
    this.medicoDaoImplementation = new MedicoDaoImplementation();
  }

  @AfterClass
  public static void postTests() {
    borrarMedicos();
  }

  @Test
  public void consultarMedico_Ok() throws DAOException, ParseException {
    final MedicoPojo expectedMedicoPojo = getMedicoUno();
    final MedicoPojo actualMedicoPojo = this.medicoDaoImplementation.getMedico(9996);

    assertEquals(expectedMedicoPojo, actualMedicoPojo);
  }

  @Test
  public void consultarMedico_Error() throws DAOException {
    final MedicoPojo actualMedicoPojo = this.medicoDaoImplementation.getMedico(9000);
    assertNull(actualMedicoPojo);
  }

  @Test
  public void crearMedico_Ok() throws DAOException, ParseException {
    final MedicoPojo expectedMedicoPojo = new MedicoPojo();
    expectedMedicoPojo.setTipoDocumento(TipoDocumento.DNI);
    expectedMedicoPojo.setNumeroDocumento("23451240");
    expectedMedicoPojo.setNombre("Juan");
    expectedMedicoPojo.setApellido("Perez");
    expectedMedicoPojo.setFechaNacimiento(DateUtils.getDateFormat().parse("01/01/1990"));
    expectedMedicoPojo.setPrecioConsulta("360");
    expectedMedicoPojo.setTipoUsuario(TipoUsuario.MEDICO);

    this.medicoDaoImplementation.createMedico(expectedMedicoPojo);
    final MedicoPojo actualMedicoPojo = this.medicoDaoImplementation
        .getMedico(expectedMedicoPojo.getUsuarioId());
    assertEquals(expectedMedicoPojo, actualMedicoPojo);
  }

  @Test
  public void actualizarMedico_Ok() throws DAOException, ParseException {
    final MedicoPojo expectedMedicoPojo = getMedicoDos();
    expectedMedicoPojo.setApellido("Velazques");

    final MedicoPojo currentMedicoPojo = this.medicoDaoImplementation.getMedico(9995);
    currentMedicoPojo.setApellido("Velazques");

    this.medicoDaoImplementation.updateMedico(currentMedicoPojo);
    final MedicoPojo actualMedicoPojo = this.medicoDaoImplementation
        .getMedico(expectedMedicoPojo.getUsuarioId());

    assertEquals(expectedMedicoPojo, actualMedicoPojo);
  }

  @Test(expected = DAOException.class)
  public void actualizarMedico_Error() throws DAOException, ParseException {
    final MedicoPojo expectedMedicoPojo = new MedicoPojo();
    expectedMedicoPojo.setUsuarioId(1001);
    expectedMedicoPojo.setTipoDocumento(TipoDocumento.DNI);
    expectedMedicoPojo.setNumeroDocumento("25513987");
    expectedMedicoPojo.setNombre("Jorge");
    expectedMedicoPojo.setApellido("Velazquez");
    expectedMedicoPojo.setFechaNacimiento(DateUtils.getDateFormat().parse("20/01/1995"));
    expectedMedicoPojo.setTipoUsuario(TipoUsuario.MEDICO);
    expectedMedicoPojo.setPrecioConsulta("720");

    this.medicoDaoImplementation.updateMedico(expectedMedicoPojo);
  }

  @Test
  public void eliminarMedico_Ok() throws DAOException, ParseException {
    final MedicoPojo expectedMedicoPojo = getMedicoTres();

    this.medicoDaoImplementation.deleteMedico(expectedMedicoPojo);
    final MedicoPojo actualMedicoPojo = this.medicoDaoImplementation.getMedico(9994);

    assertNull(actualMedicoPojo);
  }

  @Test(expected = DAOException.class)
  public void eliminarMedico_Error() throws DAOException, ParseException {
    final MedicoPojo expectedMedicoPojo = new MedicoPojo();
    expectedMedicoPojo.setUsuarioId(1001);
    expectedMedicoPojo.setTipoDocumento(TipoDocumento.DNI);
    expectedMedicoPojo.setNumeroDocumento("25513987");
    expectedMedicoPojo.setNombre("Jorge");
    expectedMedicoPojo.setApellido("Velazquez");
    expectedMedicoPojo.setFechaNacimiento(DateUtils.getDateFormat().parse("20/01/1995"));
    expectedMedicoPojo.setTipoUsuario(TipoUsuario.MEDICO);
    expectedMedicoPojo.setPrecioConsulta("720");

    this.medicoDaoImplementation.deleteMedico(expectedMedicoPojo);
  }
}

