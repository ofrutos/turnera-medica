package controller.usuario.paciente;

import controller.GeneralTableController;
import controller.InitializableController;
import controller.consulta.ConsultaController;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import model.constants.table.PacienteTableConstants;
import model.pojo.PacientePojo;
import model.util.DateUtils;
import service.PacienteService;
import service.excpetion.ServiceException;
import view.message.MessageView;
import view.usuario.paciente.PacienteTableControllerPopupMenuView;

/**
 * Controlador de la tabla de pacientes.
 */
public class PacienteTableController extends GeneralTableController implements
    InitializableController {

  private final JTable tablaPaciente;
  /**
   * Servicio de Pacientes.
   */
  private final PacienteService pacienteService;
  private final CargaPacienteController cargaPacienteController;
  private final ConsultaController consultaController;

  /**
   * Crea un controlador para la tabla de pacientes.
   *
   * @param tablaPaciente           Tabla de pacientes.
   * @param pacienteService         Servicio de Pacientes.
   * @param cargaPacienteController Controlador de la carga de pacientes.
   * @param consultaController      Controlador de Consultas.
   * @param messageView             Vista de mensajes.
   */
  public PacienteTableController(final JTable tablaPaciente,
      final PacienteService pacienteService,
      final CargaPacienteController cargaPacienteController,
      final ConsultaController consultaController,
      final MessageView messageView) {
    super(tablaPaciente,
        messageView);

    this.tablaPaciente = tablaPaciente;
    this.pacienteService = pacienteService;
    this.cargaPacienteController = cargaPacienteController;
    this.consultaController = consultaController;
  }

  public void initController() {
    try {
      super.setFilas(pacienteService.obtenerTodosPaciente().toArray());
    } catch (final ServiceException e) {
      super.handleExceptionMessage(e, "los pacientes");
    }
    super.setNombreColumnas(this.pacienteService.obtenerNombreColumnas());

    super.initController();

    this.tablaPaciente.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(final MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2) {
          final JTable jTable = (JTable) mouseEvent.getSource();
          cargaPacienteController.setPacientePojo(getPacientePojoInRow(jTable.getSelectedRow()));
          cargaPacienteController.initView();
        }
        if (SwingUtilities.isRightMouseButton(mouseEvent)) {
          final JTable jTable = (JTable) mouseEvent.getSource();

          final PacienteTableControllerPopupMenuView jPopupMenu = new PacienteTableControllerPopupMenuView();
          jPopupMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());

          jPopupMenu.getConsultasJMenuItem().addActionListener(e -> {
            consultaController.setPacientePojo(getPacientePojoInRow(jTable.getSelectedRow()));
            consultaController.setMedicoPojo(null);

            consultaController.initView();
          });
        }
      }
    });

    this.cargaPacienteController.getCargaPacienteView().addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(final WindowEvent windowEvent) {
        try {
          setFilas(pacienteService.obtenerTodosPaciente().toArray());
        } catch (final ServiceException e) {
          handleExceptionMessage(e, "los pacientes");
        }
        fireTableDataChanged();
        windowEvent.getWindow().dispose();
      }
    });
  }

  @Override
  public Object getValueAt(final int rowIndex, final int columnIndex) {
    return getValueAtColumn(getPacientePojoInRow(rowIndex), columnIndex + 1);
  }

  /**
   * Obtiene el valor de cada columna a partir de su posición en la BD.
   *
   * @param paciente Paciente actual.
   * @param column   Numero de la columna.
   * @return Valor en la columna indicada.
   */
  private Object getValueAtColumn(final PacientePojo paciente, final int column) {
    if (column == PacienteTableConstants.USUARIO_ID.getPosition()) {
      return paciente.getUsuarioId();
    }
    if (column == PacienteTableConstants.TIPO_DOCUMENTO.getPosition()) {
      return paciente.getTipoDocumento();
    }
    if (column == PacienteTableConstants.NUMERO_DOCUMENTO.getPosition()) {
      return paciente.getNumeroDocumento();
    }
    if (column == PacienteTableConstants.NOMBRE.getPosition()) {
      return paciente.getNombre();
    }
    if (column == PacienteTableConstants.APELLIDO.getPosition()) {
      return paciente.getApellido();
    }
    if (column == PacienteTableConstants.FECHA_NACIMIENTO.getPosition()) {
      return DateUtils.parseDate(paciente.getFechaNacimiento());
    }
    return null;
  }

  private PacientePojo getPacientePojoInRow(final int row) {
    return (PacientePojo) super.getFilas()[row];
  }
}
