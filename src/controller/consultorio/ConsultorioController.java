package controller.consultorio;

import controller.InitializableController;
import controller.ViewableController;
import service.ConsultorioService;
import view.consultorio.CargaConsultorioView;
import view.consultorio.ConsultorioView;
import view.message.MessageView;

/**
 * Clase que administra los controladores de la ventana principal de Consultorios.
 */
public class ConsultorioController implements InitializableController, ViewableController {

  private final ConsultorioView consultorioView;
  private final ConsultorioTableController consultorioTableController;
  private final BarraConsultorioController barraConsultorioController;
  private final CargaConsultorioController cargaConsultorioController;

  /**
   * Crea un controlador general de consultorios.
   *
   * @param consultorioView    Vista general de los consultorios.
   * @param messageView        Vista de mensajes.
   * @param consultorioService Servicio de los consultorios.
   */
  public ConsultorioController(final ConsultorioView consultorioView,
      final MessageView messageView,
      final ConsultorioService consultorioService) {
    this.consultorioView = consultorioView;
    this.cargaConsultorioController = new CargaConsultorioController(new CargaConsultorioView(),
        messageView,
        consultorioService);
    this.barraConsultorioController = new BarraConsultorioController(
        this.consultorioView.getBarraConsultorioView(),
        this.cargaConsultorioController);
    this.consultorioTableController = new ConsultorioTableController(
        this.consultorioView.getTablaConsultorio(),
        consultorioService,
        this.cargaConsultorioController,
        messageView);
  }

  public void initController() {
    this.barraConsultorioController.initController();
    this.consultorioTableController.initController();
    this.cargaConsultorioController.initController();
  }

  public void initView() {
    this.consultorioView.getjFrame().setVisible(true);
  }
}
