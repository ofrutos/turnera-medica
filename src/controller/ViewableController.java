package controller;

/**
 * Interfaz de controladores con vistas.
 */
public interface ViewableController {

  /**
   * Inicializa la vista.
   */
  void initView();
}
