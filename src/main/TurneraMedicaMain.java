package main;

import controller.main.MainController;

public class TurneraMedicaMain {

  public static void main(String[] args) {
    final MainController mainController = new MainController();

    mainController.initView();
    mainController.initController();
  }
}
