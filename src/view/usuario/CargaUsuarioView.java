package view.usuario;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import model.constants.usuario.TipoDocumento;
import view.CargaView;
import view.util.DateFocusEvent;
import view.validator.TurneraValidator;

/**
 * Clase que administra la vista de usuario. Esta posee toda la información básica de un usuario.
 */
public class CargaUsuarioView extends CargaView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private final JComboBox<TipoDocumento> tipoDocumentoComboBox;
  private final JFormattedTextField tfNDoc;
  private final JTextField tfNombre;
  private final JTextField tfApellido;
  private final JFormattedTextField tfFechaNacimiento;

  /**
   * Crea la vista de usuario.
   */
  public CargaUsuarioView() {
    setBounds(100, 100, 334, 283);

    this.jPanel = new JPanel();
    this.jPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(this.jPanel, BorderLayout.NORTH);
    GridBagLayout gblPanel = new GridBagLayout();
    gblPanel.columnWidths = new int[]{0, 0, 0};
    gblPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
    gblPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
    gblPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    this.jPanel.setLayout(gblPanel);

    JLabel lblTipoDoc = new JLabel("Tipo Doc:");
    GridBagConstraints gbcLblTipoDoc = new GridBagConstraints();
    gbcLblTipoDoc.fill = GridBagConstraints.VERTICAL;
    gbcLblTipoDoc.anchor = GridBagConstraints.EAST;
    gbcLblTipoDoc.insets = new Insets(0, 0, 5, 5);
    gbcLblTipoDoc.gridx = 0;
    gbcLblTipoDoc.gridy = 0;
    this.jPanel.add(lblTipoDoc, gbcLblTipoDoc);

    tipoDocumentoComboBox = new JComboBox<>(TipoDocumento.values());
    GridBagConstraints gbcTfTipoDoc = new GridBagConstraints();
    gbcTfTipoDoc.fill = GridBagConstraints.HORIZONTAL;
    gbcTfTipoDoc.insets = new Insets(0, 0, 5, 0);
    gbcTfTipoDoc.gridx = 1;
    gbcTfTipoDoc.gridy = 0;
    this.jPanel.add(tipoDocumentoComboBox, gbcTfTipoDoc);

    JLabel lblNDoc = new JLabel("N° Doc:");
    GridBagConstraints gbcLblNDoc = new GridBagConstraints();
    gbcLblNDoc.anchor = GridBagConstraints.EAST;
    gbcLblNDoc.insets = new Insets(0, 0, 5, 5);
    gbcLblNDoc.gridx = 0;
    gbcLblNDoc.gridy = 1;
    this.jPanel.add(lblNDoc, gbcLblNDoc);

    tfNDoc = new JFormattedTextField(TurneraValidator.numberFormatter(1));
    GridBagConstraints gbcTfNDoc = new GridBagConstraints();
    gbcTfNDoc.fill = GridBagConstraints.HORIZONTAL;
    gbcTfNDoc.insets = new Insets(0, 0, 5, 0);
    gbcTfNDoc.gridx = 1;
    gbcTfNDoc.gridy = 1;
    this.jPanel.add(tfNDoc, gbcTfNDoc);

    JLabel lblNombre = new JLabel("Nombre:");
    GridBagConstraints gbcLblNombre = new GridBagConstraints();
    gbcLblNombre.anchor = GridBagConstraints.EAST;
    gbcLblNombre.insets = new Insets(0, 0, 5, 5);
    gbcLblNombre.gridx = 0;
    gbcLblNombre.gridy = 2;
    this.jPanel.add(lblNombre, gbcLblNombre);

    tfNombre = new JTextField(null);
    GridBagConstraints gbcTfNombre = new GridBagConstraints();
    gbcTfNombre.fill = GridBagConstraints.HORIZONTAL;
    gbcTfNombre.insets = new Insets(0, 0, 5, 0);
    gbcTfNombre.gridx = 1;
    gbcTfNombre.gridy = 2;
    this.jPanel.add(tfNombre, gbcTfNombre);

    JLabel lblApellido = new JLabel("Apellido:");
    GridBagConstraints gbcLblApellido = new GridBagConstraints();
    gbcLblApellido.anchor = GridBagConstraints.EAST;
    gbcLblApellido.insets = new Insets(0, 0, 5, 5);
    gbcLblApellido.gridx = 0;
    gbcLblApellido.gridy = 3;
    this.jPanel.add(lblApellido, gbcLblApellido);

    tfApellido = new JTextField();
    GridBagConstraints gbcTfApellido = new GridBagConstraints();
    gbcTfApellido.fill = GridBagConstraints.HORIZONTAL;
    gbcTfApellido.insets = new Insets(0, 0, 5, 0);
    gbcTfApellido.gridx = 1;
    gbcTfApellido.gridy = 3;
    this.jPanel.add(tfApellido, gbcTfApellido);

    JLabel lblFechaNacimiento = new JLabel("Fecha Nacimiento:");
    GridBagConstraints gbcLblFechaNacimiento = new GridBagConstraints();
    gbcLblFechaNacimiento.anchor = GridBagConstraints.EAST;
    gbcLblFechaNacimiento.insets = new Insets(0, 0, 5, 5);
    gbcLblFechaNacimiento.gridx = 0;
    gbcLblFechaNacimiento.gridy = 4;
    this.jPanel.add(lblFechaNacimiento, gbcLblFechaNacimiento);

    this.tfFechaNacimiento = new JFormattedTextField(TurneraValidator.dateFormatter());
    this.tfFechaNacimiento.setFocusLostBehavior(JFormattedTextField.PERSIST);
    this.tfFechaNacimiento.addFocusListener(new DateFocusEvent());
    GridBagConstraints gbcTfFechaNacimiento = new GridBagConstraints();
    gbcTfFechaNacimiento.insets = new Insets(0, 0, 5, 0);
    gbcTfFechaNacimiento.fill = GridBagConstraints.HORIZONTAL;
    gbcTfFechaNacimiento.gridx = 1;
    gbcTfFechaNacimiento.gridy = 4;
    this.jPanel.add(tfFechaNacimiento, gbcTfFechaNacimiento);
  }

  public JComboBox<TipoDocumento> getTipoDocumentoComboBox() {
    return tipoDocumentoComboBox;
  }

  public JTextField getTfNDoc() {
    return tfNDoc;
  }

  public JTextField getTfNombre() {
    return tfNombre;
  }

  public JTextField getTfApellido() {
    return tfApellido;
  }

  public JTextField getTfFechaNacimiento() {
    return tfFechaNacimiento;
  }
}
