package service;

import java.util.List;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoPojo;
import service.excpetion.ServiceException;

/**
 * Interfaz de los servicios de los Consultorios de un Médico.
 */
public interface MedicoConsultorioService {

  /**
   * Obtiene todos los consultorios que tiene un medico.
   *
   * @return Consultorios del medico.
   * @throws ServiceException Si ocurre un error al obtener los consultorios de un medico.
   */
  List<MedicoConsultorioPojo> obtenerConsultorios(final MedicoPojo medicoPojo)
      throws ServiceException;

  /**
   * Crea el consultorio de un medico.
   *
   * @param medicoConsultorioPojo Consultorio a asociar.
   * @throws ServiceException Si ocurre un error al crear el consultorio de un medico.
   */
  void crearConsultorio(final MedicoConsultorioPojo medicoConsultorioPojo) throws ServiceException;

  /**
   * Actualiza el consultorio de un medico.
   *
   * @param medicoConsultorioPojo Consultorio a actualizar.
   * @throws ServiceException Si ocurre un error al actualizar el consultorio de un medico.
   */
  void actualizarConsultorio(final MedicoConsultorioPojo medicoConsultorioPojo)
      throws ServiceException;

  /**
   * Obtiene el nombre de las columnas de la tabla de consultorios de medicos.
   *
   * @return Nombre de las columnas.
   */
  List<String> obtenerNombreColumnas();
}
