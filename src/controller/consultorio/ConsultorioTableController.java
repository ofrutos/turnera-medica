package controller.consultorio;

import controller.GeneralTableController;
import controller.InitializableController;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTable;
import model.constants.table.ConsultorioTableConstants;
import model.pojo.ConsultorioPojo;
import service.ConsultorioService;
import service.excpetion.ServiceException;
import view.message.MessageView;

/**
 * Controlador de la tabla de los consultorios.
 */
public class ConsultorioTableController extends GeneralTableController implements
    InitializableController {

  private final JTable tablaConsultorio;
  /**
   * Servicio de Consultorio.
   */
  private final ConsultorioService consultorioService;
  private final CargaConsultorioController cargaConsultorioController;

  /**
   * Crea un controlador para la tabla de consultorios.
   *
   * @param tablaConsultorio           Tabla de consultorios.
   * @param consultorioService         Servicio de Consultorios.
   * @param cargaConsultorioController Controlador de la carga de consultorios.
   * @param messageView                Vista de mensajes.
   */
  public ConsultorioTableController(final JTable tablaConsultorio,
      final ConsultorioService consultorioService,
      final CargaConsultorioController cargaConsultorioController,
      final MessageView messageView) {
    super(tablaConsultorio,
        messageView);

    this.tablaConsultorio = tablaConsultorio;
    this.consultorioService = consultorioService;
    this.cargaConsultorioController = cargaConsultorioController;
  }

  public void initController() {
    try {
      super.setFilas(this.consultorioService.obtenerTodosConsultorio().toArray());
    } catch (final ServiceException e) {
      super.handleExceptionMessage(e, "los consultorios");
    }
    super.setNombreColumnas(this.consultorioService.obtenerNombreColumnas());

    super.initController();

    this.tablaConsultorio.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(final MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2) {
          final JTable jTable = (JTable) mouseEvent.getSource();
          cargaConsultorioController
              .setConsultorioPojo(getMedicoPojoInRow(jTable.getSelectedRow()));
          cargaConsultorioController.initView();
        }
      }
    });

    this.cargaConsultorioController.getCargaConsultorioView()
        .addWindowListener(new WindowAdapter() {
          @Override
          public void windowClosed(final WindowEvent windowEvent) {
            try {
              setFilas(consultorioService.obtenerTodosConsultorio().toArray());
            } catch (final ServiceException e) {
              handleExceptionMessage(e, "los consultorios");
            }
            fireTableDataChanged();
            windowEvent.getWindow().dispose();
          }
        });
  }

  @Override
  public Object getValueAt(final int rowIndex, final int columnIndex) {
    return getValueAtColumn(getMedicoPojoInRow(rowIndex), columnIndex + 1);
  }

  /**
   * Obtiene el valor de cada columna a partir de su posición en la BD.
   *
   * @param column Numero de la columna.
   * @return Valor en la columna indicada.
   */
  private Object getValueAtColumn(final ConsultorioPojo consultorio, final int column) {
    if (column == ConsultorioTableConstants.CONSULTORIO_ID.getPosition()) {
      return consultorio.getConsultorioId();
    }
    if (column == ConsultorioTableConstants.NOMBRE.getPosition()) {
      return consultorio.getNombre();
    }

    return null;
  }

  private ConsultorioPojo getMedicoPojoInRow(final int row) {
    return (ConsultorioPojo) super.getFilas()[row];
  }
}
