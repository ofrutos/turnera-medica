package view.medicoconsultorio;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import model.pojo.ConsultorioPojo;
import model.pojo.HorarioPojo;
import view.CargaView;
import view.util.DateFocusEvent;
import view.validator.TurneraValidator;

/**
 * Clase que administra la vista de la carga del consultorio de un medico.
 */
public class CargaMedicoConsultorioView extends CargaView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private final JComboBox<ConsultorioPojo> consultorioComboBox;
  private final JFormattedTextField tfFechaInicio;
  private final JFormattedTextField tfFechaFin;
  private final JComboBox<HorarioPojo> horarioComboBox;

  /**
   * Crea la vista de la carga del consultorio de un medico.
   */
  public CargaMedicoConsultorioView() {
    setBounds(100, 100, 334, 283);

    this.jPanel = new JPanel();
    this.jPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    getContentPane().add(this.jPanel, BorderLayout.NORTH);
    GridBagLayout gblPanel = new GridBagLayout();
    gblPanel.columnWidths = new int[]{0, 0, 0};
    gblPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
    gblPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
    gblPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
    this.jPanel.setLayout(gblPanel);

    JLabel lblConsultorio = new JLabel("Consultorio:");
    GridBagConstraints gbcLblConsultorio = new GridBagConstraints();
    gbcLblConsultorio.fill = GridBagConstraints.VERTICAL;
    gbcLblConsultorio.anchor = GridBagConstraints.EAST;
    gbcLblConsultorio.insets = new Insets(0, 0, 5, 5);
    gbcLblConsultorio.gridx = 0;
    gbcLblConsultorio.gridy = 0;
    this.jPanel.add(lblConsultorio, gbcLblConsultorio);

    this.consultorioComboBox = new JComboBox<>();
    GridBagConstraints gbcConsultorioComboBox = new GridBagConstraints();
    gbcConsultorioComboBox.insets = new Insets(0, 0, 5, 0);
    gbcConsultorioComboBox.fill = GridBagConstraints.HORIZONTAL;
    gbcConsultorioComboBox.gridx = 1;
    gbcConsultorioComboBox.gridy = 0;
    this.jPanel.add(consultorioComboBox, gbcConsultorioComboBox);

    JLabel lblFechaInicio = new JLabel("Fecha inicio:");
    GridBagConstraints gbcLblFechaInicio = new GridBagConstraints();
    gbcLblFechaInicio.anchor = GridBagConstraints.EAST;
    gbcLblFechaInicio.insets = new Insets(0, 0, 5, 5);
    gbcLblFechaInicio.gridx = 0;
    gbcLblFechaInicio.gridy = 1;
    this.jPanel.add(lblFechaInicio, gbcLblFechaInicio);

    tfFechaInicio = new JFormattedTextField(TurneraValidator.dateFormatter());
    tfFechaInicio.setFocusLostBehavior(JFormattedTextField.PERSIST);
    tfFechaInicio.addFocusListener(new DateFocusEvent());
    GridBagConstraints gbcTfFechaInicio = new GridBagConstraints();
    gbcTfFechaInicio.fill = GridBagConstraints.HORIZONTAL;
    gbcTfFechaInicio.insets = new Insets(0, 0, 5, 0);
    gbcTfFechaInicio.gridx = 1;
    gbcTfFechaInicio.gridy = 1;
    this.jPanel.add(tfFechaInicio, gbcTfFechaInicio);

    JLabel lblFechaFin = new JLabel("Fecha fin:");
    GridBagConstraints gbcLblFechaFin = new GridBagConstraints();
    gbcLblFechaFin.anchor = GridBagConstraints.EAST;
    gbcLblFechaFin.insets = new Insets(0, 0, 5, 5);
    gbcLblFechaFin.gridx = 0;
    gbcLblFechaFin.gridy = 2;
    this.jPanel.add(lblFechaFin, gbcLblFechaFin);

    tfFechaFin = new JFormattedTextField(TurneraValidator.dateFormatter());
    tfFechaFin.setFocusLostBehavior(JFormattedTextField.PERSIST);
    tfFechaFin.addFocusListener(new DateFocusEvent());
    GridBagConstraints gbcTfFechaFin = new GridBagConstraints();
    gbcTfFechaFin.fill = GridBagConstraints.HORIZONTAL;
    gbcTfFechaFin.insets = new Insets(0, 0, 5, 0);
    gbcTfFechaFin.gridx = 1;
    gbcTfFechaFin.gridy = 2;
    this.jPanel.add(tfFechaFin, gbcTfFechaFin);

    JLabel lblHorario = new JLabel("Horario:");
    GridBagConstraints gbcLblHorario = new GridBagConstraints();
    gbcLblHorario.anchor = GridBagConstraints.EAST;
    gbcLblHorario.insets = new Insets(0, 0, 0, 5);
    gbcLblHorario.gridx = 0;
    gbcLblHorario.gridy = 3;
    this.jPanel.add(lblHorario, gbcLblHorario);

    horarioComboBox = new JComboBox<>();
    GridBagConstraints gbcHorarioComboBox = new GridBagConstraints();
    gbcHorarioComboBox.fill = GridBagConstraints.HORIZONTAL;
    gbcHorarioComboBox.gridx = 1;
    gbcHorarioComboBox.gridy = 3;
    this.jPanel.add(horarioComboBox, gbcHorarioComboBox);
  }

  public JComboBox<ConsultorioPojo> getConsultorioComboBox() {
    return consultorioComboBox;
  }

  public JTextField getTfFechaInicio() {
    return tfFechaInicio;
  }

  public JTextField getTfFechaFin() {
    return tfFechaFin;
  }

  public JComboBox<HorarioPojo> getHorarioComboBox() {
    return horarioComboBox;
  }
}
