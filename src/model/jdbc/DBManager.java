package model.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Administrador de la conexión con la base de datos. <br>
 */
public class DBManager {

  private static final String DB_DRIVER = SQLiteConstants.DB_DRIVER.getConstant();
  private static final String DB_PATH = SQLiteConstants.DB_PATH.getConstant();

  private DBManager() {
    throw new IllegalStateException("Utility class");
  }

  public static Connection connect() {
    try {
      Class.forName("org.sqlite.JDBC");
    } catch (ClassNotFoundException e) {
      System.exit(10);
    }

    Connection connection = null;
    try {
      connection = DriverManager.getConnection(DB_DRIVER + ":" + DB_PATH);
      connection.setAutoCommit(false);
    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(0);
    }
    return connection;
  }

  public static void handleRollback(final Connection connection) {
    try {
      connection.rollback();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public static void handleClose(final Connection connection) {
    try {
      connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
