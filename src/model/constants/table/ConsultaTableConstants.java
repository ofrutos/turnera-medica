package model.constants.table;

public enum ConsultaTableConstants {
  CONSULTA_ID(1),
  PACIENTE(2),
  MEDICO(3),
  CONSULTORIO(4),
  FECHA(5),
  HORARIO(6),
  PRECIO_FINAL(7);

  private final Integer position;

  ConsultaTableConstants(final Integer position) {
    this.position = position;
  }

  public Integer getPosition() {
    return this.position;
  }
}
