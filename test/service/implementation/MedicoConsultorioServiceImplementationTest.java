package service.implementation;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static util.pojo.MedicoConsultorioPojoMock.getMedicoConsultorioDos;

import java.text.ParseException;
import java.util.Collections;
import java.util.List;
import model.dao.MedicoConsultorioDao;
import model.dao.exception.DAOException;
import model.dao.implementation.sqlite.MedicoConsultorioDaoImplementation;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoPojo;
import model.util.DateUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import service.MedicoConsultorioService;
import service.excpetion.ServiceException;

public class MedicoConsultorioServiceImplementationTest {

  @Rule
  public MockitoRule initRule = MockitoJUnit.rule();

  @Mock
  private final MedicoConsultorioDao medicoConsultorioDao = new MedicoConsultorioDaoImplementation();

  private MedicoConsultorioService medicoConsultorioService;

  @Before
  public void setUp() {
    this.medicoConsultorioService = new MedicoConsultorioServiceImplementation(
        this.medicoConsultorioDao);
  }

  @Ignore
  public void crearMedicoConsultorio_Ok() throws DAOException, ServiceException {
    doNothing().when(this.medicoConsultorioDao)
        .createMedicoConsultorio(isA(MedicoConsultorioPojo.class));

    final MedicoConsultorioPojo medicoConsultorioPojo = new MedicoConsultorioPojo();

    this.medicoConsultorioService.crearConsultorio(medicoConsultorioPojo);
  }

  @Test(expected = ServiceException.class)
  public void crearMedicoConsultorio_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.medicoConsultorioDao)
        .createMedicoConsultorio(isA(MedicoConsultorioPojo.class));

    final MedicoConsultorioPojo medicoConsultorioPojo = new MedicoConsultorioPojo();

    this.medicoConsultorioService.crearConsultorio(medicoConsultorioPojo);
  }

  @Ignore
  public void actualizarMedicoConsultorio_Ok() throws DAOException, ServiceException {
    doNothing().when(this.medicoConsultorioDao)
        .updateMedicoConsultorio(isA(MedicoConsultorioPojo.class));

    final MedicoConsultorioPojo medicoConsultorioPojo = new MedicoConsultorioPojo();

    this.medicoConsultorioService.actualizarConsultorio(medicoConsultorioPojo);
  }

  @Test(expected = ServiceException.class)
  public void actualizarMedicoConsultorio_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.medicoConsultorioDao)
        .updateMedicoConsultorio(isA(MedicoConsultorioPojo.class));

    final MedicoConsultorioPojo medicoConsultorioPojo = new MedicoConsultorioPojo();

    this.medicoConsultorioService.actualizarConsultorio(medicoConsultorioPojo);
  }

  @Ignore
  public void obtenerMedicoConsultorio_Ok() throws ServiceException, DAOException, ParseException {
    when(this.medicoConsultorioDao.getAllMedicoConsultorio()).thenReturn(getMedicoConsultorioDos());

    final MedicoConsultorioPojo medicoConsultorioPojo = new MedicoConsultorioPojo();
    medicoConsultorioPojo.getMedicoPojo().setUsuarioId(9995);
    medicoConsultorioPojo.getConsultorioPojo().setConsultorioId(103);
    medicoConsultorioPojo.setFechaInicio(DateUtils.getDateFormat().parse("01/01/2010"));
    medicoConsultorioPojo.setFechaFin(DateUtils.getDateFormat().parse(""));
//    medicoConsultorioPojo.setHorarioId(23);

    final List<MedicoConsultorioPojo> expectedMedicoConsultorioPojos = Collections
        .singletonList(medicoConsultorioPojo);

    final MedicoPojo medicoPojo = new MedicoPojo();

    final List<MedicoConsultorioPojo> actualMedicoConsultorioPojos = this.medicoConsultorioService
        .obtenerConsultorios(medicoPojo);

    assertEquals(expectedMedicoConsultorioPojos, actualMedicoConsultorioPojos);
  }

  @Test(expected = ServiceException.class)
  public void obtenerMedicoConsultorio_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.medicoConsultorioDao).getAllMedicoConsultorio();

    final MedicoPojo medicoPojo = new MedicoPojo();

    this.medicoConsultorioService.obtenerConsultorios(medicoPojo);
  }
}
