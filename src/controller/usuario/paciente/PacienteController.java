package controller.usuario.paciente;

import controller.InitializableController;
import controller.ViewableController;
import controller.consulta.ConsultaController;
import service.PacienteService;
import view.message.MessageView;
import view.usuario.paciente.CargaPacienteView;
import view.usuario.paciente.PacienteView;

/**
 * Clase que administra los controladores de la ventana principal de Pacientes.
 */
public class PacienteController implements InitializableController, ViewableController {

  private final PacienteView pacienteView;
  private final PacienteTableController pacienteTableController;
  private final BarraPacienteController barraPacienteController;
  private final CargaPacienteController cargaPacienteController;

  /**
   * Crea un controlador general de pacientes.
   *
   * @param pacienteView       Vista general de los pacientes.
   * @param messageView        Vista de mensajes.
   * @param pacienteService    Servicio de los Pacientes.
   * @param consultaController Controlador de Consultas.
   */
  public PacienteController(final PacienteView pacienteView,
      final MessageView messageView,
      final PacienteService pacienteService,
      final ConsultaController consultaController) {
    this.pacienteView = pacienteView;
    this.cargaPacienteController = new CargaPacienteController(new CargaPacienteView(),
        messageView,
        pacienteService);
    this.barraPacienteController = new BarraPacienteController(
        this.pacienteView.getBarraPacienteView(),
        this.cargaPacienteController);
    this.pacienteTableController = new PacienteTableController(this.pacienteView.getTablaPaciente(),
        pacienteService,
        this.cargaPacienteController,
        consultaController,
        messageView);
  }

  public void initController() {
    this.barraPacienteController.initController();
    this.pacienteTableController.initController();
    this.cargaPacienteController.initController();
  }

  public void initView() {
    this.pacienteView.getjFrame().setVisible(true);
  }
}
