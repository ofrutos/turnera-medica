package controller.usuario.medico;

import controller.GeneralTableController;
import controller.InitializableController;
import controller.consulta.ConsultaController;
import controller.medicoconsultorio.MedicoConsultorioController;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import model.constants.table.MedicoTableConstants;
import model.pojo.MedicoPojo;
import model.util.DateUtils;
import service.MedicoService;
import service.excpetion.ServiceException;
import view.message.MessageView;
import view.usuario.medico.MedicoTableControllerPopupMenuView;

/**
 * Controlados de la tabla de medicos.
 */
public class MedicoTableController extends GeneralTableController implements
    InitializableController {

  private final JTable tablaMedico;
  private final MedicoService medicoService;
  private final CargaMedicoController cargaMedicoController;
  private final MedicoConsultorioController medicoConsultorioController;
  private final ConsultaController consultaController;

  /**
   * Crea una controlador para la tabla de medicos.
   *
   * @param tablaMedico                 Tabla de medicos.
   * @param medicoService               Servicio de Medicos.
   * @param medicoConsultorioController Controlador de la carga de medicos.
   */
  public MedicoTableController(final JTable tablaMedico,
      final MedicoService medicoService,
      final CargaMedicoController cargaMedicoController,
      final MedicoConsultorioController medicoConsultorioController,
      final ConsultaController consultaController,
      final MessageView messageView) {
    super(tablaMedico,
        messageView);

    this.tablaMedico = tablaMedico;
    this.medicoService = medicoService;
    this.cargaMedicoController = cargaMedicoController;
    this.medicoConsultorioController = medicoConsultorioController;
    this.consultaController = consultaController;
  }

  public void initController() {
    try {
      super.setFilas(this.medicoService.obtenerTodosMedico().toArray());
    } catch (final ServiceException e) {
      super.handleExceptionMessage(e, "los medicos");
    }
    super.setNombreColumnas(this.medicoService.obtenerNombreColumnas());

    super.initController();

    this.tablaMedico.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(final MouseEvent mouseEvent) {
        if (mouseEvent.getClickCount() == 2) {
          final JTable jTable = (JTable) mouseEvent.getSource();
          cargaMedicoController.setPacientePojo(getMedicoPojoInRow(jTable.getSelectedRow()));
          cargaMedicoController.initView();
        }
        if (SwingUtilities.isRightMouseButton(mouseEvent)) {
          final JTable jTable = (JTable) mouseEvent.getSource();

          final MedicoTableControllerPopupMenuView jPopupMenu = new MedicoTableControllerPopupMenuView();
          jPopupMenu.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());

          jPopupMenu.getConsultoriosJMenuItem().addActionListener(e -> {
            medicoConsultorioController.setMedicoPojo(getMedicoPojoInRow(jTable.getSelectedRow()));

            medicoConsultorioController.initView();
          });

          jPopupMenu.getConsultasJMenuItem().addActionListener(e -> {
            consultaController.setMedicoPojo(getMedicoPojoInRow(jTable.getSelectedRow()));
            consultaController.setPacientePojo(null);

            consultaController.initView();
          });
        }
      }
    });

    this.cargaMedicoController.getCargaMedicoView().addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(final WindowEvent windowEvent) {
        try {
          setFilas(medicoService.obtenerTodosMedico().toArray());
        } catch (final ServiceException e) {
          handleExceptionMessage(e, "los medicos");
        }
        fireTableDataChanged();
        windowEvent.getWindow().dispose();
      }
    });
  }

  @Override
  public Object getValueAt(final int rowIndex, final int columnIndex) {
    return getValueAtColumn(getMedicoPojoInRow(rowIndex), columnIndex + 1);
  }

  /**
   * Obtiene el valor de cada columna a partir de su posición en la BD.
   *
   * @param medico Medico actual.
   * @param column Numero de la columna.
   * @return Valor en la columna indicada.
   */
  private Object getValueAtColumn(final MedicoPojo medico, final int column) {
    if (column == MedicoTableConstants.USUARIO_ID.getPosition()) {
      return medico.getUsuarioId();
    }
    if (column == MedicoTableConstants.TIPO_DOCUMENTO.getPosition()) {
      return medico.getTipoDocumento();
    }
    if (column == MedicoTableConstants.NUMERO_DOCUMENTO.getPosition()) {
      return medico.getNumeroDocumento();
    }
    if (column == MedicoTableConstants.NOMBRE.getPosition()) {
      return medico.getNombre();
    }
    if (column == MedicoTableConstants.APELLIDO.getPosition()) {
      return medico.getApellido();
    }
    if (column == MedicoTableConstants.FECHA_NACIMIENTO.getPosition()) {
      return DateUtils.parseDate(medico.getFechaNacimiento());
    }
    if (column == MedicoTableConstants.PRECIO_CONSULTA.getPosition()) {
      return medico.getPrecioConsulta();
    }

    return null;
  }

  private MedicoPojo getMedicoPojoInRow(final int row) {
    return (MedicoPojo) super.getFilas()[row];
  }
}
