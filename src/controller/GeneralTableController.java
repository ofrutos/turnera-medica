package controller;

import java.util.List;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import view.constants.ViewLabels;
import view.message.MessageView;

/**
 * Clase que administra el controlador general de las tablas.
 */
public abstract class GeneralTableController extends AbstractTableModel implements
    InitializableController {

  private final JTable jTable;
  private final MessageView messageView;
  /**
   * Filas de la BD.
   */
  private Object[] filas;
  /**
   * Nombre de las columnas.
   */
  private List<String> nombreColumnas;

  /**
   * Crea un controlador general de las tablas.
   *
   * @param jTable      Tabla.
   * @param messageView Vista de mensajes.
   */
  public GeneralTableController(final JTable jTable,
      final MessageView messageView) {
    this.jTable = jTable;
    this.messageView = messageView;
  }

  @Override
  public void initController() {
    this.jTable.setModel(this);
  }

  @Override
  public String getColumnName(final int col) {
    return this.nombreColumnas.get(col);
  }

  @Override
  public Class getColumnClass(final int col) {
    return this.nombreColumnas.get(col).getClass();
  }

  @Override
  public int getRowCount() {
    return this.filas.length;
  }

  @Override
  public int getColumnCount() {
    return this.nombreColumnas.size();
  }

  public Object[] getFilas() {
    return this.filas;
  }

  /**
   * Establece las filas de la tabla.
   *
   * @param filas Filas de la tabla.
   */
  public void setFilas(final Object[] filas) {
    this.filas = filas;
  }

  /**
   * Establece el nombre de las columnas de la tabla.
   *
   * @param nombreColumnas Nombre de las columnas.
   */
  public void setNombreColumnas(final List<String> nombreColumnas) {
    this.nombreColumnas = nombreColumnas;
  }

  /**
   * Muestra un mensaje de excepción.
   *
   * @param exception Excepción.
   * @param origen    Origen de la excepción.
   */
  protected void handleExceptionMessage(final Exception exception,
      final String origen) {
    this.messageView.setTitle(ViewLabels.MENSAJE_ERROR.getLabel());
    this.messageView
        .setMensaje(
            "Ha ocurrido un error al obtener " + origen + ": " + exception.getMessage());
    this.messageView.setVisible(true);
  }
}
