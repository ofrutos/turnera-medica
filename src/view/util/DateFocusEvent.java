package view.util;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.ParseException;
import javax.swing.JFormattedTextField;

/**
 * Clase que administra el evento de perdida de foco en campos de fechas.
 */
public class DateFocusEvent extends FocusAdapter {

  @Override
  public void focusLost(final FocusEvent focusEvent) {
    final JFormattedTextField textField = (JFormattedTextField) focusEvent.getSource();
    if (textField.getText() == null || textField.getText().isEmpty()) {
      textField.setText("");
    } else {
      try {
        textField.setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);
        textField.commitEdit();
      } catch (final ParseException parseException) {
        textField.setText("");
      } finally {
        textField.setFocusLostBehavior(JFormattedTextField.PERSIST);
      }
    }
  }
}
