package controller.reporte;

import controller.CargaController;
import java.text.ParseException;
import java.util.Date;
import model.util.DateUtils;
import service.excpetion.ServiceException;
import view.constants.ViewLabels;
import view.message.MessageView;
import view.reporte.CargaReporteView;

/**
 * Clase que administra el controlador de carga de reportes.
 */
public class CargaReporteController extends CargaController {

  private final CargaReporteView cargaReporteView;

  private Date fechaDesde;
  private Date fechaHasta;

  /**
   * Crea un controlador para la carga de reportes.
   *
   * @param cargaReporteView Vista de la carga de reportes.
   * @param messageView      Vista de mensajes.
   */
  public CargaReporteController(final CargaReporteView cargaReporteView,
      final MessageView messageView) {
    super(cargaReporteView,
        messageView);

    this.cargaReporteView = cargaReporteView;

    this.fechaDesde = DateUtils.getMinimumDate();
    this.fechaHasta = DateUtils.getMaximumDate();
  }

  @Override
  public void initView() {
    this.cargaReporteView.setVisible(true);
  }

  @Override
  protected boolean controlarCarga() {
    return false;
  }

  @Override
  protected Object getDataInView() throws ParseException {
    Date fechaHastaView = DateUtils.getDateFormat()
        .parse(this.cargaReporteView.getTfFechaDesde().getText());
    if (fechaHastaView.equals(DateUtils.getMaximumDate())) {
      fechaHastaView = DateUtils.getMinimumDate();
    }

    this.fechaDesde = fechaHastaView;
    this.fechaHasta = DateUtils.getDateFormat()
        .parse(this.cargaReporteView.getTfFechaHasta().getText());

    return null;
  }

  /**
   * Establece el rango de fechas a mostrar.
   */
  @Override
  protected void update() {
    try {
      if (!DateUtils.isRangoFechasValida(this.fechaDesde, this.fechaHasta)) {
        throw new ServiceException("El rango de fechas ingresado no es valido.");
      } else {
        this.getDataInView();
        this.cargaReporteView.dispose();
      }
    } catch (final ServiceException | ParseException e) {
      super.showMessage(ViewLabels.MENSAJE_ERROR.getLabel(), e.getMessage());
    }
  }

  /**
   * Devuelve la vista de la carga de reportes.
   *
   * @return Vista de la carga de reportes.
   */
  public CargaReporteView getCargaReporteView() {
    return this.cargaReporteView;
  }

  /**
   * Devuelve la fecha desde cargada en la carga.
   *
   * @return Fecha desde.
   */
  public Date getFechaDesde() {
    return fechaDesde;
  }

  /**
   * Devuelve la fecha hasta cargada en la carga.
   *
   * @return Fecha hasta.
   */
  public Date getFechaHasta() {
    return fechaHasta;
  }
}
