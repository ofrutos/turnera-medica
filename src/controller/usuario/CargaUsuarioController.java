package controller.usuario;

import controller.CargaController;
import java.text.ParseException;
import model.constants.usuario.TipoDocumento;
import model.pojo.UsuarioPojo;
import model.util.DateUtils;
import view.constants.ViewLabels;
import view.message.MessageView;
import view.usuario.CargaUsuarioView;

/**
 * Clase que administra la carga de un usuario.
 */
public abstract class CargaUsuarioController extends CargaController<UsuarioPojo> {

  private final CargaUsuarioView cargaUsuarioView;
  private UsuarioPojo usuarioPojo;

  /**
   * Crea la carga de un usuario.
   *
   * @param cargaUsuarioView Vista de la carga de un usuario.
   */
  public CargaUsuarioController(final CargaUsuarioView cargaUsuarioView,
      final MessageView messageView) {
    super(cargaUsuarioView,
        messageView);

    this.cargaUsuarioView = cargaUsuarioView;
  }

  @Override
  public void initView() {
    if (this.usuarioPojo != null) {
      this.cargaUsuarioView.getTfNDoc().setText(this.usuarioPojo.getNumeroDocumento());
      this.cargaUsuarioView.getTipoDocumentoComboBox().getModel()
          .setSelectedItem(this.usuarioPojo.getTipoDocumento().name());
      this.cargaUsuarioView.getTfNombre().setText(this.usuarioPojo.getNombre());
      this.cargaUsuarioView.getTfApellido().setText(this.usuarioPojo.getApellido());
      this.cargaUsuarioView.getTfFechaNacimiento()
          .setText(DateUtils.parseDate(this.usuarioPojo.getFechaNacimiento()));
    } else {
      this.cargaUsuarioView.getTfNDoc().setText(null);
      this.cargaUsuarioView.getTfNombre().setText(null);
      this.cargaUsuarioView.getTfApellido().setText(null);
      this.cargaUsuarioView.getTfFechaNacimiento().setText(null);
    }
    this.cargaUsuarioView.setVisible(true);
  }

  protected void setUsuarioPojo(final UsuarioPojo usuarioPojo) {
    this.usuarioPojo = usuarioPojo;
  }

  /**
   * Obtiene el POJO del usuario en la vista.
   *
   * @return POJO del Usuario.
   * @throws ParseException En caso de que la fecha ingresada sea invalida.
   */
  @Override
  protected UsuarioPojo getDataInView() throws ParseException {
    return new UsuarioPojo(this.usuarioPojo == null ? null : this.usuarioPojo.getUsuarioId(),
        TipoDocumento.valueOf(
            this.cargaUsuarioView.getTipoDocumentoComboBox().getModel().getSelectedItem()
                .toString()),
        this.cargaUsuarioView.getTfNDoc().getText(),
        this.cargaUsuarioView.getTfNombre().getText(),
        this.cargaUsuarioView.getTfApellido().getText(),
        DateUtils.getDateFormat().parse(this.cargaUsuarioView.getTfFechaNacimiento().getText()),
        null);
  }

  @Override
  protected boolean controlarCarga() {
    if (this.cargaUsuarioView.getTfNDoc().getText().isEmpty()) {
      super.showMessage(ViewLabels.MENSAJE_ADVERTENCIA.getLabel(),
          "Se debe ingresar un número de documento.");
      return false;
    }
    if (this.cargaUsuarioView.getTfNombre().getText().isEmpty()) {
      super.showMessage(ViewLabels.MENSAJE_ADVERTENCIA.getLabel(),
          "Se debe ingresar un nombre.");
      return false;
    }
    if (this.cargaUsuarioView.getTfApellido().getText().isEmpty()) {
      super.showMessage(ViewLabels.MENSAJE_ADVERTENCIA.getLabel(),
          "Se debe ingresar un apellido.");
      return false;
    }
    if (this.cargaUsuarioView.getTfFechaNacimiento().getText().isEmpty()) {
      super.showMessage(ViewLabels.MENSAJE_ADVERTENCIA.getLabel(),
          "Se debe ingresar una fecha de nacimiento.");
      return false;
    }
    return true;
  }
}
