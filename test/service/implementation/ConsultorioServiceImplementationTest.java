package service.implementation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static util.pojo.ConsultorioPojoMock.getAllConsultorios;
import static util.pojo.ConsultorioPojoMock.getConsultorioUno;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import model.dao.ConsultorioDao;
import model.dao.exception.DAOException;
import model.dao.implementation.sqlite.ConsultorioDaoImplementation;
import model.pojo.ConsultorioPojo;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import service.ConsultorioService;
import service.excpetion.ServiceException;

public class ConsultorioServiceImplementationTest {

  @Rule
  public MockitoRule initRule = MockitoJUnit.rule();

  @Mock
  private final ConsultorioDao consultorioDao = new ConsultorioDaoImplementation();

  private ConsultorioService consultorioService;

  @Before
  public void setup() {
    this.consultorioService = new ConsultorioServiceImplementation(this.consultorioDao);
  }

  @Ignore
  public void crearConsultorio_Ok() throws DAOException, ServiceException {
    doNothing().when(this.consultorioDao).createConsultorio(isA(ConsultorioPojo.class));

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();

    this.consultorioService.crearConsultorio(consultorioPojo);
  }

  @Test(expected = ServiceException.class)
  public void crearConsultorio_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.consultorioDao)
        .createConsultorio(isA(ConsultorioPojo.class));

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();

    this.consultorioService.crearConsultorio(consultorioPojo);
  }

  @Ignore
  public void actualizarConsultorio_Ok() throws DAOException, ServiceException {
    doNothing().when(this.consultorioDao).updateConsultorio(isA(ConsultorioPojo.class));

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();

    this.consultorioService.actualizarConsultorio(consultorioPojo);
  }

  @Test(expected = ServiceException.class)
  public void actualizarConsultorio_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.consultorioDao)
        .updateConsultorio(isA(ConsultorioPojo.class));

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();

    this.consultorioService.actualizarConsultorio(consultorioPojo);
  }

  @Ignore
  public void eliminarConsultorio_Ok() throws DAOException, ServiceException {
    doNothing().when(this.consultorioDao).deleteConsultorio(isA(ConsultorioPojo.class));

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();

    this.consultorioService.eliminarConsultorio(consultorioPojo);
  }

  @Test(expected = ServiceException.class)
  public void eliminarConsultorio_Error() throws DAOException, ServiceException {
    doThrow(DAOException.class).when(this.consultorioDao)
        .deleteConsultorio(isA(ConsultorioPojo.class));

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();

    this.consultorioService.eliminarConsultorio(consultorioPojo);
  }

  @Ignore
  public void consultarConsultorio_Ok() throws DAOException {
    when(this.consultorioDao.getConsultorio(isA(Integer.class))).thenReturn(getConsultorioUno());

    final ConsultorioPojo expectedConsultorioPojo = new ConsultorioPojo();
    expectedConsultorioPojo.setConsultorioId(101);
    expectedConsultorioPojo.setNombre("Consultorio 1");

    final int consultorioId = 101;
    final ConsultorioPojo actualConsultorioPojo = this.consultorioService
        .obtenerConsultorio(consultorioId);

    assertEquals(expectedConsultorioPojo, actualConsultorioPojo);
  }

  @Test
  public void consultarConsultorio_Error() {
    final int consultorioId = 1;
    final ConsultorioPojo actualConsultorioPojo = this.consultorioService
        .obtenerConsultorio(consultorioId);

    assertNull(actualConsultorioPojo);
  }

  @Test
  public void consultarAllConsultorio_Ok() throws DAOException, ServiceException {
    when(this.consultorioDao.getAllConsultorio()).thenReturn(getAllConsultorios());

    final List<ConsultorioPojo> expectedConsultoriosPojo = new ArrayList<>();

    final ConsultorioPojo expectedConsultorioPojo1 = new ConsultorioPojo();
    expectedConsultorioPojo1.setConsultorioId(101);
    expectedConsultorioPojo1.setNombre("Consultorio 1");

    expectedConsultoriosPojo.add(expectedConsultorioPojo1);

    final ConsultorioPojo expectedConsultorioPojo2 = new ConsultorioPojo();
    expectedConsultorioPojo2.setConsultorioId(102);
    expectedConsultorioPojo2.setNombre("Consultorio 2");

    expectedConsultoriosPojo.add(expectedConsultorioPojo2);

    expectedConsultoriosPojo.sort(Comparator.comparing(ConsultorioPojo::getConsultorioId));

    final List<ConsultorioPojo> actualConsultoriosPojo = this.consultorioService
        .obtenerTodosConsultorio();

    assertEquals(expectedConsultoriosPojo, actualConsultoriosPojo);
  }

  @Test(expected = ServiceException.class)
  public void consultarAllConsultorio_Error() throws DAOException, ServiceException {
    when(this.consultorioDao.getAllConsultorio()).thenThrow(DAOException.class);

    final List<ConsultorioPojo> actualConsultoriosPojo = this.consultorioService
        .obtenerTodosConsultorio();

    assertNull(actualConsultoriosPojo);
  }
}
