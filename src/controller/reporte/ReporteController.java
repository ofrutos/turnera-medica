package controller.reporte;

import controller.InitializableController;
import controller.ViewableController;
import service.ConsultaService;
import view.message.MessageView;
import view.reporte.CargaReporteView;
import view.reporte.ReporteView;

/**
 * Clase que administra el controlador de Reportes.
 */
public class ReporteController implements InitializableController, ViewableController {

  private final ReporteView reporteView;
  private final ReporteTableController reporteTableController;
  private final BarraReporteController barraReporteController;
  private final CargaReporteController cargaReporteController;

  /**
   * Crea un controlador de Reportes.
   *
   * @param reporteView     Vista de Reportes.
   * @param messageView     Vista de mensajes.
   * @param consultaService Servicio de Consulta.
   */
  public ReporteController(final ReporteView reporteView,
      final MessageView messageView,
      final ConsultaService consultaService) {
    this.reporteView = reporteView;
    this.cargaReporteController = new CargaReporteController(new CargaReporteView(),
        messageView);
    this.barraReporteController = new BarraReporteController(this.reporteView.getBarraReporteView(),
        this.cargaReporteController);
    this.reporteTableController = new ReporteTableController(reporteView.getTablaReporte(),
        consultaService,
        this.cargaReporteController,
        messageView);
  }

  @Override
  public void initController() {
    this.reporteTableController.initController();
    this.barraReporteController.initController();
    this.cargaReporteController.initController();
  }

  @Override
  public void initView() {
    this.reporteTableController.initView();
    this.reporteView.getjFrame().setVisible(true);
  }
}
