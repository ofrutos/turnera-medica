package view.usuario.paciente;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
 * Clase que administra el popup menu de la tabla de pacientes.
 */
public class PacienteTableControllerPopupMenuView extends JPopupMenu {

  private final JMenuItem consultasJMenuItem;


  /**
   * Crea el popup menu de la tabla de pacientes.
   */
  public PacienteTableControllerPopupMenuView() {
    super();

    this.consultasJMenuItem = new JMenuItem("Consultas");
    add(this.consultasJMenuItem);
  }

  public JMenuItem getConsultasJMenuItem() {
    return consultasJMenuItem;
  }
}
