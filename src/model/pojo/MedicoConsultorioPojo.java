package model.pojo;

import java.util.Date;
import java.util.Objects;

public class MedicoConsultorioPojo {

  private final MedicoConsultorioPojoKey medicoConsultorioPojoKey;
  private MedicoPojo medicoPojo;
  private ConsultorioPojo consultorioPojo;
  private Date fechaFin;
  private HorarioPojo horarioPojo;

  public MedicoConsultorioPojo() {
    this.medicoConsultorioPojoKey = new MedicoConsultorioPojoKey();
  }

  public MedicoConsultorioPojo(final Integer usuarioId,
      final Integer consultorioId,
      final String nombreConsultorio,
      final Date fechaInicio,
      final Date fechaFin,
      final Integer horarioId,
      final String horaInicio,
      final String horaFin) {
    this.medicoConsultorioPojoKey = new MedicoConsultorioPojoKey(usuarioId,
        consultorioId,
        fechaInicio,
        horarioId);
    this.medicoPojo = new MedicoPojo();
    this.medicoPojo.setUsuarioId(usuarioId);
    this.consultorioPojo = new ConsultorioPojo(consultorioId, nombreConsultorio);
    this.fechaFin = fechaFin;
    this.horarioPojo = new HorarioPojo(horarioId, horaInicio, horaFin);
  }

  public MedicoPojo getMedicoPojo() {
    return medicoPojo;
  }

  public void setMedicoPojo(MedicoPojo medicoPojo) {
    this.medicoPojo = medicoPojo;
    this.medicoConsultorioPojoKey.setUsuarioId(medicoPojo.getUsuarioId());
  }

  public ConsultorioPojo getConsultorioPojo() {
    return consultorioPojo;
  }

  public void setConsultorioPojo(ConsultorioPojo consultorioPojo) {
    this.consultorioPojo = consultorioPojo;
    this.medicoConsultorioPojoKey.setConsultorioId(consultorioPojo.getConsultorioId());
  }

  public Date getFechaInicio() {
    return this.medicoConsultorioPojoKey.getFechaInicio();
  }

  public void setFechaInicio(final Date fechaInicio) {
    this.medicoConsultorioPojoKey.setFechaInicio(fechaInicio);
  }

  public Date getFechaFin() {
    return fechaFin;
  }

  public void setFechaFin(final Date fechaFin) {
    this.fechaFin = fechaFin;
  }

  public HorarioPojo getHorarioPojo() {
    return horarioPojo;
  }

  public void setHorarioPojo(HorarioPojo horarioPojo) {
    this.horarioPojo = horarioPojo;
    this.medicoConsultorioPojoKey.setHorarioId(horarioPojo.getHorarioId());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    MedicoConsultorioPojo that = (MedicoConsultorioPojo) o;
    return medicoConsultorioPojoKey.equals(that.medicoConsultorioPojoKey) &&
        Objects.equals(fechaFin, that.fechaFin);
  }

  @Override
  public int hashCode() {
    return Objects.hash(medicoConsultorioPojoKey, fechaFin);
  }

  public MedicoConsultorioPojoKey getMedicoConsultorioPojoKey() {
    return medicoConsultorioPojoKey;
  }
}
