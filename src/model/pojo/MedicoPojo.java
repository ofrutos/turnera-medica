package model.pojo;

import java.util.Date;
import java.util.Objects;
import model.constants.usuario.TipoDocumento;

/**
 * Clase que administra la información de un médico.
 */
public class MedicoPojo extends UsuarioPojo {

  private String precioConsulta;
  private int cantidadConsultas = 0;
  private double totalRecaudado = 0;

  /**
   * Crea un medico.
   */
  public MedicoPojo() {
    super();
  }

  /**
   * Crea un medico a partir de un usuario.
   *
   * @param usuarioPojo POJO del usuario.
   */
  public MedicoPojo(final UsuarioPojo usuarioPojo) {
    super(usuarioPojo);
  }

  /**
   * Crea un medico.
   *
   * @param usuarioId       ID del usuario.
   * @param tipoDocumento   Tipo de documento.
   * @param numeroDocumento Numero del documento.
   * @param nombre          Nombre del usuario.
   * @param apellido        Apellido del usuario.
   * @param fechaNacimiento Fecha de nacimiento.
   * @param tipoUsuario     Tipo de usuario.
   * @param precioConsulta  Precio de la consulta.
   */
  public MedicoPojo(final Integer usuarioId,
      final TipoDocumento tipoDocumento,
      final String numeroDocumento,
      final String nombre,
      final String apellido,
      final Date fechaNacimiento,
      final TipoUsuario tipoUsuario,
      final String precioConsulta) {
    super(usuarioId,
        tipoDocumento,
        numeroDocumento,
        nombre,
        apellido,
        fechaNacimiento,
        tipoUsuario);
    this.precioConsulta = precioConsulta;
  }

  public String getPrecioConsulta() {
    return precioConsulta;
  }

  public void setPrecioConsulta(final String precioConsulta) {
    this.precioConsulta = precioConsulta;
  }

  /**
   * Aumenta la cantidad de consultas realizadas.
   */
  public void increaseCantidadConsultas() {
    this.cantidadConsultas++;
  }

  /**
   * Resetea la cantidad de consultas.
   */
  public void resetCantidadConsultas() {
    this.cantidadConsultas = 0;
  }

  /**
   * Suma lo recaudado al total.
   *
   * @param recaudado Recaudado en la consulta.
   */
  public void addTotalRecaudado(final double recaudado) {
    this.totalRecaudado += recaudado;
  }

  /**
   * Resetea lo recaudado.
   */
  public void resetTotalRecaudado() {
    this.totalRecaudado = 0;
  }

  public int getCantidadConsultas() {
    return cantidadConsultas;
  }

  public double getTotalRecaudado() {
    return totalRecaudado;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    if (!super.equals(o)) {
      return false;
    }
    MedicoPojo that = (MedicoPojo) o;
    return Objects.equals(precioConsulta, that.precioConsulta);
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode(), precioConsulta);
  }

  @Override
  public String toString() {
    return super.toString() + " (" + this.precioConsulta + ")";
  }
}
