# Tema 8: Turnera Medica

## Funcionalidad básica

### Médicos
-[x] Administrar médicos. 
-[X] Cada médico es un usuario del sistema, pudiendo consultar los turnos que tiene para una fecha determinada.
-[x] Cada médico cobra cierta cantidad de dinero por su consulta.

### Pacientes 
-[x] Administrar pacientes.

### Consultas
-[X] Administrar turnos - fecha y hora. 
-[x] No se puede tomar un turno con un mismo médico a una misma hora. 
-[X] El médico debe elegirse de una lista, al igual que el paciente.

### Reportes
-[x] Se debe poder obtener una lista de cuánto ha cobrado un médico, por cuántas consultas (turnos) entre dos fechas.

## Adicionales

### Pacientes
-[X] Los pacientes también son usuarios, pudiendo consultar cuándo tienen que asistir a una consulta.

### Médicos
-[x] Administrar lugares de atención (consultorios) con la consecuencia de que un médico debe atender en un cierto lugar entre una y otra fecha.
-[X] El turno debe tomarse en un cierto consultorio (es solo una restricción más al turno).

### Reportes
-[X] Listar los médicos y su recaudación entre dos fechas.

## Bonus points

### Obras Sociales
-[ ] Manejar obras sociales.
-[ ] Si un paciente tiene la misma obra social que la que atiende un médico, se le hace un descuento del 50% en la consulta.

### Consultas
-[ ] Mostrar un grilla mensual o semanal con turnos (como si se mostrara un calendario)
