package view.medicoconsultorio;

import javax.swing.JTable;
import view.GeneralView;

/**
 * Clase que administra la vista de los consultorios de los medicos.
 */
public class MedicoConsultorioView extends GeneralView {

  private final BarraMedicoConsultorioView barraMedicoConsultorioView;

  /**
   * Crea la vista principal de los consultorios de los medicos.
   *
   * @param barraMedicoConsultorioView Barra de la vista de los consultorios del medico.
   */
  public MedicoConsultorioView(final BarraMedicoConsultorioView barraMedicoConsultorioView) {
    super(barraMedicoConsultorioView);

    this.barraMedicoConsultorioView = barraMedicoConsultorioView;
    this.barraMedicoConsultorioView.initView();
  }

  /**
   * Devuelve la barra de la vista de los consultorios del medico.
   *
   * @return Barra de la vista de los consultorios del medico.
   */
  public BarraMedicoConsultorioView getBarraMedicoView() {
    return this.barraMedicoConsultorioView;
  }

  /**
   * Devuelve la tabla de los consultorios del medico.
   *
   * @return Tabla de los los consultorios del medico.
   */
  public JTable getTablaMedicoConsultorio() {
    return super.getjTable();
  }
}
