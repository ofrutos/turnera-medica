package service;

import java.util.List;
import model.pojo.MedicoPojo;
import service.excpetion.ServiceException;

/**
 * Interfaz de los servicios de Médicos.
 */
public interface MedicoService {

  /**
   * Crea un medico.
   *
   * @param medicoPojo POJO del medico.
   * @throws ServiceException Si ocurre un error al crear al medico.
   */
  void crearMedico(final MedicoPojo medicoPojo) throws ServiceException;

  /**
   * Actualiza un medico.
   *
   * @param medicoPojo POJO del medico.
   * @throws ServiceException Si ocurre un error al actualizar al medico.
   */
  void actualizarMedico(final MedicoPojo medicoPojo) throws ServiceException;

  /**
   * Elimina un medico.
   *
   * @param medicoPojo POJO del medico.
   * @throws ServiceException Si ocurre un error al eliminar al medico.
   */
  void eliminarMedico(final MedicoPojo medicoPojo) throws ServiceException;

  /**
   * Obtiene un medico.
   *
   * @param medicoId POJO del medico.
   * @return El medico indicado. En caso de no existir devuelve <b>null</b>
   */
  MedicoPojo obtenerMedico(final int medicoId);

  /**
   * Obtiene un medico.
   *
   * @return Devuelve todos los medicos.
   * @throws ServiceException Si ocurre un error al obtener a los medicos.
   */
  List<MedicoPojo> obtenerTodosMedico() throws ServiceException;

  /**
   * Obtiene el nombre de las columnas de la tabla de medicos.
   *
   * @return Nombre de las columnas.
   */
  List<String> obtenerNombreColumnas();
}
