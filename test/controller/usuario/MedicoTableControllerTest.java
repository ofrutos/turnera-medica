package controller.usuario;

import static org.junit.Assert.assertEquals;

import controller.medicoconsultorio.MedicoConsultorioController;
import controller.usuario.medico.CargaMedicoController;
import controller.usuario.medico.MedicoTableController;
import java.util.Arrays;
import java.util.List;
import javax.swing.JTable;
import model.dao.implementation.sqlite.ConsultorioDaoImplementation;
import model.dao.implementation.sqlite.HorarioDaoImplementation;
import model.dao.implementation.sqlite.MedicoConsultorioDaoImplementation;
import model.dao.implementation.sqlite.MedicoDaoImplementation;
import org.junit.Before;
import org.junit.Test;
import service.ConsultorioService;
import service.HorarioService;
import service.MedicoConsultorioService;
import service.MedicoService;
import service.implementation.ConsultorioServiceImplementation;
import service.implementation.HorarioServiceImplementation;
import service.implementation.MedicoConsultorioServiceImplementation;
import service.implementation.MedicoServiceImplementation;
import view.medicoconsultorio.BarraMedicoConsultorioView;
import view.medicoconsultorio.MedicoConsultorioView;
import view.message.MessageView;
import view.usuario.medico.CargaMedicoView;

public class MedicoTableControllerTest {

  private MedicoTableController medicoTableController;

  @Before
  public void setUp() {
    final MedicoService medicoService = new MedicoServiceImplementation(
        new MedicoDaoImplementation());
    final CargaMedicoController cargaMedicoController = new CargaMedicoController(
        new CargaMedicoView(),
        new MessageView(),
        medicoService);
    final MedicoConsultorioService medicoConsultorioService = new MedicoConsultorioServiceImplementation(
        new MedicoConsultorioDaoImplementation());
    final ConsultorioService consultorioService = new ConsultorioServiceImplementation(
        new ConsultorioDaoImplementation());
    final HorarioService horarioService = new HorarioServiceImplementation(
        new HorarioDaoImplementation());
    final MedicoConsultorioController medicoConsultorioController = new MedicoConsultorioController(
        new MedicoConsultorioView(new BarraMedicoConsultorioView()),
        new MessageView(),
        medicoConsultorioService,
        consultorioService,
        horarioService);
    this.medicoTableController = new MedicoTableController(new JTable(),
        medicoService,
        cargaMedicoController,
        medicoConsultorioController,
        null,
        new MessageView());

    this.medicoTableController.initController();
  }

  @Test
  public void getColumnCountTest() {
    assertEquals(7, this.medicoTableController.getColumnCount());
  }

  @Test
  public void getColumnNameTest() {
    List<String> nombreColumnas = Arrays
        .asList("Usuario Id",
            "Tipo Documento",
            "Numero Documento",
            "Nombre",
            "Apellido",
            "Fecha Nacimiento",
            "Precio Consulta");

    for (int columna = 0; columna < 7; columna++) {
      assertEquals(nombreColumnas.get(columna),
          this.medicoTableController.getColumnName(columna));
    }
  }
}
