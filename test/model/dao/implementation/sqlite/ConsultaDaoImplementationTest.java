package model.dao.implementation.sqlite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;
import static util.jdbc.DBManagerMock.borrarConsultas;
import static util.jdbc.DBManagerMock.borrarConsultorios;
import static util.jdbc.DBManagerMock.borrarMedicos;
import static util.jdbc.DBManagerMock.borrarPacientes;
import static util.jdbc.DBManagerMock.crearConsultas;
import static util.jdbc.DBManagerMock.crearConsultorios;
import static util.jdbc.DBManagerMock.crearMedicos;
import static util.jdbc.DBManagerMock.crearPacientes;
import static util.pojo.ConsultaPojoMock.getConsultaCinco;
import static util.pojo.ConsultaPojoMock.getConsultaUno;

import java.sql.Connection;
import java.text.ParseException;
import model.dao.exception.DAOException;
import model.jdbc.DBManager;
import model.pojo.ConsultaPojo;
import model.pojo.ConsultorioPojo;
import model.pojo.HorarioPojo;
import model.pojo.MedicoPojo;
import model.pojo.PacientePojo;
import model.util.DateUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import util.jdbc.DBManagerMock;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DBManager.class)
public class ConsultaDaoImplementationTest {

  private ConsultaDaoImplementation consultaDaoImplementation;

  @BeforeClass
  public static void preTests() {
    spy(DBManager.class);
    when(DBManager.connect())
        .thenAnswer((Answer<Connection>) invocationOnMock -> DBManagerMock.connect());

    crearPacientes();
    crearMedicos();
    crearConsultorios();
    crearConsultas();
  }

  @Before
  public void setup() {
    this.consultaDaoImplementation = new ConsultaDaoImplementation();
  }

  @AfterClass
  public static void postTests() {
    borrarConsultas();
    borrarPacientes();
    borrarMedicos();
    borrarConsultorios();
  }

  @Ignore
  public void consultarConsulta_Ok() throws ParseException {
    final ConsultaPojo expectedConsultorioPojo = getConsultaUno();
    final ConsultaPojo actualConsultaPojo = this.consultaDaoImplementation
        .getConsulta(2999);

    assertEquals(expectedConsultorioPojo, actualConsultaPojo);
  }

  @Test
  public void consultarConsulta_Fail() {
    final ConsultaPojo actualConsultaPojo = this.consultaDaoImplementation
        .getConsulta(1001);

    assertNull(actualConsultaPojo);
  }

  @Ignore
  public void crearConsulta_Ok() throws DAOException, ParseException {
    final PacientePojo pacientePojo = new PacientePojo();
    pacientePojo.setUsuarioId(9998);

    final MedicoPojo medicoPojo = new MedicoPojo();
    medicoPojo.setUsuarioId(9995);

    final ConsultorioPojo consultorioPojo = new ConsultorioPojo();
    consultorioPojo.setConsultorioId(102);

    final HorarioPojo horarioPojo = new HorarioPojo(36, "", "");

    final ConsultaPojo expectedConsultaPojo = new ConsultaPojo();
    expectedConsultaPojo.setPacientePojo(pacientePojo);
    expectedConsultaPojo.setMedicoPojo(medicoPojo);
    expectedConsultaPojo.setConsultorioPojo(consultorioPojo);
    expectedConsultaPojo.setFecha(DateUtils.getDateFormat().parse("08/09/2020"));
    expectedConsultaPojo.setHorarioPojo(horarioPojo);
    expectedConsultaPojo.setPrecioBase("640");
    expectedConsultaPojo.setDescuento(0);
    expectedConsultaPojo.setPrecioFinal("640");

    this.consultaDaoImplementation.createConsulta(expectedConsultaPojo);

    final ConsultaPojo actualConsultaPojo = this.consultaDaoImplementation
        .getConsulta(expectedConsultaPojo.getConsultaId());
    assertEquals(expectedConsultaPojo, actualConsultaPojo);
  }

  @Test
  public void eliminarConsulta_Ok() throws DAOException, ParseException {
    final ConsultaPojo expectedConsultaPojo = getConsultaCinco();

    this.consultaDaoImplementation.deleteConsulta(expectedConsultaPojo);

    final ConsultaPojo actualConsultaPojo = this.consultaDaoImplementation
        .getConsulta(expectedConsultaPojo.getConsultaId());
    assertNull(actualConsultaPojo);
  }

  @Test(expected = DAOException.class)
  public void eliminarConsulta_Error() throws DAOException {
    final ConsultaPojo expectedConsultaPojo = new ConsultaPojo();
    expectedConsultaPojo.setConsultaId(1001);

    this.consultaDaoImplementation.deleteConsulta(expectedConsultaPojo);
  }
}
