package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import view.constants.ViewLabels;

/**
 * Clase que administra la ventana de una carga de datos.
 */
public class CargaView extends JDialog {

  private final JButton okButton;
  private final JButton cancelButton;
  protected JPanel jPanel;

  /**
   * Crea la vista de la carga de datos.
   */
  public CargaView() {
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    getContentPane().setLayout(new BorderLayout());
    setModal(true);

    JPanel buttonPane = new JPanel();
    buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));

    getContentPane().add(buttonPane, BorderLayout.SOUTH);

    this.okButton = new JButton(ViewLabels.OK.getLabel());
    this.okButton.setActionCommand(ViewLabels.OK.getLabel());
    buttonPane.add(this.okButton);
    getRootPane().setDefaultButton(this.okButton);

    this.cancelButton = new JButton(ViewLabels.CANCELAR.getLabel());
    this.cancelButton.setActionCommand(ViewLabels.CANCELAR.getLabel());
    buttonPane.add(this.cancelButton);
  }

  public JButton getOkButton() {
    return this.okButton;
  }

  public JButton getCancelButton() {
    return this.cancelButton;
  }
}
