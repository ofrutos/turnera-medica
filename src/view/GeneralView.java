package view;

import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * Clase que administra la vista general de los componentes.
 */
public class GeneralView extends JFrame {

  private final JTable jTable;
  protected JFrame jFrame;

  /**
   * Crea una vista general.
   *
   * @param generalBarraView Barra general de la vista.
   */
  public GeneralView(final GeneralBarraView generalBarraView) {
    setDefaultCloseOperation(EXIT_ON_CLOSE);

    this.jFrame = new JFrame();
    this.jFrame.setBounds(100, 100, 563, 406);
    jFrame.getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

    this.jFrame.setJMenuBar(generalBarraView);

    this.jTable = new JTable();
    JScrollPane scrollPaneTablaPaciente = new JScrollPane(this.jTable);
    jFrame.getContentPane().add(scrollPaneTablaPaciente);
  }

  public JFrame getjFrame() {
    return this.jFrame;
  }

  public JTable getjTable() {
    return this.jTable;
  }
}
