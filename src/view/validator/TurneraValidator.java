package view.validator;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.text.NumberFormatter;
import model.util.DateUtils;

/**
 * Clase que administra los validadores.
 */
public class TurneraValidator {

  private TurneraValidator() {
    throw new IllegalStateException("Utility class");
  }

  /**
   * Crea un formateador de números.
   *
   * @param minimum Valor mínimo a utilizar
   * @return Formateador de números.
   */
  public static NumberFormatter numberFormatter(final int minimum) {
    final NumberFormat numberFormat = NumberFormat.getInstance(new Locale("da", "DK"));
    numberFormat.setGroupingUsed(true);

    final NumberFormatter numberFormatter = new NumberFormatter(numberFormat);
    numberFormatter.setMinimum(minimum);
    numberFormatter.setMaximum(Integer.MAX_VALUE);
    numberFormatter.setAllowsInvalid(true);

    return numberFormatter;
  }

  public static DateFormat dateFormatter() {
    final DateFormat dateFormat = DateUtils.getDateFormat();
    dateFormat.setLenient(true);

    return dateFormat;
  }
}
