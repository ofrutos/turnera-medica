package controller;

import view.GeneralBarraView;

/**
 * Clase que administra el control de la barra general del programa.
 */
public class GeneralBarraController implements InitializableController {

  private final GeneralBarraView generalBarraView;

  /**
   * Crea el controlador de la barra general.
   *
   * @param generalBarraView Vista de la barra general.
   */
  public GeneralBarraController(final GeneralBarraView generalBarraView) {
    this.generalBarraView = generalBarraView;
  }

  @Override
  public void initController() {
    this.generalBarraView.getSalirLbl().addActionListener(e -> this.salir());
  }

  /**
   * Sale del sistema.
   */
  private void salir() {
    System.exit(0);
  }
}
