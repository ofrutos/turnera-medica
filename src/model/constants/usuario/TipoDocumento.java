package model.constants.usuario;

/**
 * Clase que administra las constantes de tipo de documento.
 */
public enum TipoDocumento {
  DNI,
  LC,
  LE
}
