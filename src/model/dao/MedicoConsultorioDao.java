package model.dao;

import java.util.List;
import java.util.Map;
import model.dao.exception.DAOException;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoConsultorioPojoKey;

/**
 * Interfaz que administra todas las declaraciones para el DAO de un consultorio de un medico.
 */
public interface MedicoConsultorioDao {

  /**
   * Crea una consultorio de médico.
   *
   * @param medicoConsultorioPojo Consultorio de médico a crear. <br>
   * @throws DAOException Si ocurre un error al crear el consultorio del medico.
   */
  void createMedicoConsultorio(final MedicoConsultorioPojo medicoConsultorioPojo)
      throws DAOException;

  /**
   * Actualiza un consultorio de médico.
   *
   * @param medicoConsultorioPojo Consultorio de médico a actualizar.
   * @throws DAOException Si ocurre un error al actualizar el consultorio del medico.
   */
  void updateMedicoConsultorio(final MedicoConsultorioPojo medicoConsultorioPojo)
      throws DAOException;

  /**
   * Obtiene todas los consultorios de los médicos.
   *
   * @return Todas los consultorios de los médicos.
   * @throws DAOException Si ocurre un error al obtener los consultorios del medico.
   */
  Map<MedicoConsultorioPojoKey, MedicoConsultorioPojo> getAllMedicoConsultorio()
      throws DAOException;

  /**
   * Obtiene el nombre de las columnas de la tabla de MedicoConsultorio.
   *
   * @return Nombre de las columnas.
   */
  List<String> getMedicoConsultorioNombreColumna();
}
