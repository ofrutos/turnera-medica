package controller.usuario.medico;

import controller.usuario.CargaUsuarioController;
import java.text.ParseException;
import model.pojo.MedicoPojo;
import model.pojo.TipoUsuario;
import service.MedicoService;
import service.excpetion.ServiceException;
import view.constants.ViewLabels;
import view.message.MessageView;
import view.usuario.medico.CargaMedicoView;

/**
 * Clase que administra el controlador de alta y modificación de medicos.
 */
public class CargaMedicoController extends CargaUsuarioController {

  private final CargaMedicoView cargaMedicoView;
  private final MedicoService medicoService;
  private MedicoPojo medicoPojo;

  /**
   * Crea el controlador de la carga de medico.
   *
   * @param cargaMedicoView Vista de la carga de medicos.
   * @param medicoService   Servicio de Medico.
   */
  public CargaMedicoController(final CargaMedicoView cargaMedicoView,
      final MessageView messageView,
      final MedicoService medicoService) {
    super(cargaMedicoView,
        messageView);

    this.cargaMedicoView = cargaMedicoView;
    this.medicoService = medicoService;
  }

  public void initView() {
    if (this.medicoPojo != null) {
      this.cargaMedicoView.setTitle("Actualizando paciente " + this.medicoPojo.getUsuarioId());

      this.cargaMedicoView.getTfPrecioConsulta().setText(this.medicoPojo.getPrecioConsulta());
    } else {
      this.cargaMedicoView.setTitle("Creando medico");

      this.cargaMedicoView.getTfPrecioConsulta().setText(null);
    }

    super.initView();
  }

  @Override
  protected boolean controlarCarga() {
    if (super.controlarCarga()) {
      if (this.cargaMedicoView.getTfPrecioConsulta().getText().isEmpty()) {
        super.showMessage(ViewLabels.MENSAJE_ADVERTENCIA.getLabel(),
            "Se debe ingresar un precio de consulta.");
        return false;
      }
      return true;
    }
    return false;
  }

  /**
   * Actualiza la información del usuario. Si el usuario no existe en el sistema, lo genera.
   */
  @Override
  protected void update() {
    if (this.controlarCarga()) {
      try {
        String mensaje;
        if (this.medicoPojo == null) {
          this.medicoService.crearMedico(this.getDataInView());
          mensaje = "Se ha creado al medico exitosamente.";
        } else {
          this.medicoService.actualizarMedico(this.getDataInView());
          mensaje = "Se ha actualizado el paciente exitosamente.";
        }
        super.showMessage(ViewLabels.MENSAJE_EXITO.getLabel(), mensaje);
        this.cargaMedicoView.dispose();
      } catch (final ServiceException | ParseException e) {
        super.showMessage(ViewLabels.MENSAJE_ERROR.getLabel(), e.getMessage());
      }
    }
  }

  /**
   * Obtiene el POJO del medico en la carga.
   *
   * @return POJO del Medico.
   * @throws ParseException En caso de que la fecha no haya sido ingresada correctamente.
   */
  @Override
  protected MedicoPojo getDataInView() throws ParseException {
    final MedicoPojo medicoPojoInView = new MedicoPojo(super.getDataInView());
    medicoPojoInView.setTipoUsuario(TipoUsuario.MEDICO);
    medicoPojoInView.setPrecioConsulta(this.cargaMedicoView.getTfPrecioConsulta().getText());

    return medicoPojoInView;
  }

  /**
   * Establece el medico a visualizar en la carga.
   *
   * @param medicoPojo POJO del medico.
   */
  public void setPacientePojo(final MedicoPojo medicoPojo) {
    super.setUsuarioPojo(medicoPojo);
    this.medicoPojo = medicoPojo;
  }

  /**
   * Devuelve la vista de la carga de medico.
   *
   * @return Vista de la carga de medico.
   */
  public CargaMedicoView getCargaMedicoView() {
    return this.cargaMedicoView;
  }
}
