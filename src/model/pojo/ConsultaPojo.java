package model.pojo;

import java.util.Date;
import java.util.Objects;

/**
 * Clase que administra el POJO de una consulta.
 */
public class ConsultaPojo {

  private Integer consultaId;
  private PacientePojo pacientePojo;
  private MedicoPojo medicoPojo;
  private ConsultorioPojo consultorioPojo;
  private Date fecha;
  private HorarioPojo horarioPojo;
  private String precioBase;
  private Integer descuento;
  private String precioFinal;

  public ConsultaPojo() {
  }

  /**
   * Crea una consulta
   *
   * @param consultaId      ID de la consulta.
   * @param pacientePojo    POJO del paciente.
   * @param medicoPojo      POJO del medico.
   * @param consultorioPojo POJO del consultorio.
   * @param fecha           Fecha de la consulta.
   * @param horarioPojo     POJO de la hora.
   * @param precioBase      Precio base de la consulta.
   * @param descuento       Descuento de la consulta.
   * @param precioFinal     Precio final de la consulta.
   */
  public ConsultaPojo(final Integer consultaId,
      final PacientePojo pacientePojo,
      final MedicoPojo medicoPojo,
      final ConsultorioPojo consultorioPojo,
      final Date fecha,
      final HorarioPojo horarioPojo,
      final String precioBase,
      final Integer descuento,
      final String precioFinal) {
    this.consultaId = consultaId;
    this.pacientePojo = pacientePojo;
    this.medicoPojo = medicoPojo;
    this.consultorioPojo = consultorioPojo;
    this.fecha = fecha;
    this.horarioPojo = horarioPojo;
    this.precioBase = precioBase;
    this.descuento = descuento;
    this.precioFinal = precioFinal;
  }

  public Integer getConsultaId() {
    return consultaId;
  }

  public void setConsultaId(Integer consultaId) {
    this.consultaId = consultaId;
  }

  public PacientePojo getPacientePojo() {
    return pacientePojo;
  }

  public void setPacientePojo(PacientePojo pacientePojo) {
    this.pacientePojo = pacientePojo;
  }

  public MedicoPojo getMedicoPojo() {
    return medicoPojo;
  }

  public void setMedicoPojo(MedicoPojo medicoPojo) {
    this.medicoPojo = medicoPojo;
  }

  public ConsultorioPojo getConsultorioPojo() {
    return consultorioPojo;
  }

  public void setConsultorioPojo(ConsultorioPojo consultorioPojo) {
    this.consultorioPojo = consultorioPojo;
  }

  public Date getFecha() {
    return fecha;
  }

  public void setFecha(Date fecha) {
    this.fecha = fecha;
  }

  public HorarioPojo getHorarioPojo() {
    return horarioPojo;
  }

  public void setHorarioPojo(HorarioPojo horarioPojo) {
    this.horarioPojo = horarioPojo;
  }

  public String getPrecioBase() {
    return precioBase;
  }

  public void setPrecioBase(String precioBase) {
    this.precioBase = precioBase;
  }

  public Integer getDescuento() {
    return descuento;
  }

  public void setDescuento(Integer descuento) {
    this.descuento = descuento;
  }

  public String getPrecioFinal() {
    return precioFinal;
  }

  public void setPrecioFinal(String precioFinal) {
    this.precioFinal = precioFinal;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConsultaPojo that = (ConsultaPojo) o;
    return Objects.equals(consultaId, that.consultaId) &&
        Objects.equals(pacientePojo, that.pacientePojo) &&
        Objects.equals(medicoPojo, that.medicoPojo) &&
        Objects.equals(consultorioPojo, that.consultorioPojo) &&
        Objects.equals(fecha, that.fecha) &&
        Objects.equals(horarioPojo, that.horarioPojo) &&
        Objects.equals(precioBase, that.precioBase) &&
        Objects.equals(descuento, that.descuento) &&
        Objects.equals(precioFinal, that.precioFinal);
  }

  @Override
  public int hashCode() {
    return Objects
        .hash(consultaId, pacientePojo, medicoPojo, consultorioPojo, fecha, horarioPojo,
            precioBase, descuento, precioFinal);
  }
}
