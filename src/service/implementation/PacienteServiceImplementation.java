package service.implementation;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import model.dao.PacienteDao;
import model.dao.exception.DAOException;
import model.pojo.PacientePojo;
import model.pojo.UsuarioPojo;
import service.PacienteService;
import service.excpetion.ServiceException;

/**
 * Clase que implementa los servicios de pacientes.
 */
public class PacienteServiceImplementation implements PacienteService {

  private final PacienteDao pacienteDao;
  private Map<Integer, PacientePojo> pacientePojoMap;


  /**
   * Crea una implementación del servicio de Pacientes.
   *
   * @param pacienteDao DAO de paciente.
   */
  public PacienteServiceImplementation(final PacienteDao pacienteDao) {
    this.pacienteDao = pacienteDao;
    this.pacientePojoMap = new TreeMap<>();
  }

  @Override
  public void crearPaciente(final PacientePojo pacientePojo) throws ServiceException {
    try {
      this.checkUnicoDocumento(pacientePojo);

      this.pacienteDao.createPaciente(pacientePojo);
      this.pacientePojoMap.put(pacientePojo.getUsuarioId(), pacientePojo);
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public void actualizarPaciente(final PacientePojo pacientePojo) throws ServiceException {
    try {
      this.checkUnicoDocumento(pacientePojo);

      this.pacienteDao.updatePaciente(pacientePojo);
      this.pacientePojoMap.put(pacientePojo.getUsuarioId(), pacientePojo);
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public void eliminarPaciente(final PacientePojo pacientePojo) throws ServiceException {
    try {
      this.pacienteDao.deletePaciente(pacientePojo);
      this.pacientePojoMap.remove(pacientePojo.getUsuarioId());
    } catch (final DAOException e) {
      throw new ServiceException(e.getMessage());
    }
  }

  @Override
  public PacientePojo obtenerPaciente(final int pacienteId) {
    try {
      return this.pacienteDao.getPaciente(pacienteId);
    } catch (final DAOException e) {
      return null;
    }
  }

  @Override
  public List<PacientePojo> obtenerTodosPaciente() throws ServiceException {
    if (this.pacientePojoMap.isEmpty()) {
      try {
        this.pacientePojoMap = this.pacienteDao.getAllPacientes();
      } catch (final DAOException e) {
        throw new ServiceException(e.getMessage());
      }
    }
    return this.pacientePojoMap.values()
        .stream()
        .sorted(Comparator.comparing(UsuarioPojo::getUsuarioId))
        .collect(Collectors.toList());
  }

  @Override
  public List<String> obtenerNombreColumnas() {
    return this.pacienteDao.getPacienteNombreColumna();
  }

  /**
   * Controla si el paciente no posee el mismo documento que otro paciente.
   *
   * @param pacientePojo POJO del paciente.
   * @throws ServiceException Si el paciente posee el mismo documento que otro paciente.
   */
  private void checkUnicoDocumento(final PacientePojo pacientePojo) throws ServiceException {
    if (this.pacientePojoMap.values()
        .stream()
        .anyMatch(currentPacientePojo ->
            !currentPacientePojo.getUsuarioId().equals(pacientePojo.getUsuarioId()) &&
                currentPacientePojo.getTipoDocumento().equals(pacientePojo.getTipoDocumento())
                && currentPacientePojo.getNumeroDocumento()
                .equals(pacientePojo.getNumeroDocumento()))) {
      throw new ServiceException("El DNI ingresado ya se encuentra registrado para un paciente.");
    }
  }
}
