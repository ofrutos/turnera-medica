package view.usuario.paciente;

import javax.swing.JMenuItem;
import view.GeneralBarraView;
import view.constants.ViewLabels;

/**
 * Clase que administra los componentes visuales de la barra de menú de la ventana de los
 * pacientes.
 */
public class BarraPacienteView extends GeneralBarraView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private JMenuItem pacienteLbl;

  /**
   * Crea la vista de la barra de pacientes.
   */
  public BarraPacienteView() {
    super();
  }

  /**
   * Inicializa los componentes visuales.
   */
  public void initView() {
    super.initLabels();

    this.pacienteLbl = new JMenuItem(ViewLabels.PACIENTE.getLabel());
    super.getNuevoLbl().add(this.pacienteLbl);
  }

  /**
   * Devuelve el label de Pacientes.
   *
   * @return Label de Pacientes.
   */
  public JMenuItem getPacienteLbl() {
    return this.pacienteLbl;
  }
}
