package controller.consulta;

import controller.GeneralBarraController;
import controller.InitializableController;
import view.consulta.BarraConsultaView;

/**
 * Clase que administra el controlador de la barra de la vista de consultas.
 */
public class BarraConsultaController extends GeneralBarraController implements
    InitializableController {

  private final BarraConsultaView barraConsultaView;
  private final CargaConsultaController cargaConsultaController;

  /**
   * Crea el controlador de la barra de menu de la ventana de consultas.
   *
   * @param barraConsultaView       Vista de la barra de la ventana de consultas.
   * @param cargaConsultaController Controlador de la carga de consultas.
   */
  public BarraConsultaController(final BarraConsultaView barraConsultaView,
      final CargaConsultaController cargaConsultaController) {
    super(barraConsultaView);

    this.barraConsultaView = barraConsultaView;
    this.cargaConsultaController = cargaConsultaController;
  }

  public void initController() {
    super.initController();
    this.barraConsultaView.getConsultaLbl().addActionListener(e -> this.crearConsulta());
  }

  /**
   * Carga el formulario para crear una nueva consulta.
   */
  private void crearConsulta() {
    this.cargaConsultaController.setConsultaPojo(null);
    this.cargaConsultaController.initView();
  }
}
