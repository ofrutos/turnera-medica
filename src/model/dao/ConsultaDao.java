package model.dao;

import java.util.List;
import java.util.Map;
import model.dao.exception.DAOException;
import model.pojo.ConsultaPojo;

/**
 * Interfaz que administra todas las declaraciones para el DAO de una consulta.
 */
public interface ConsultaDao {

  /**
   * Crea una consulta.
   *
   * @param consultaPojo Consulta a crear.
   * @throws DAOException Si no se pudo crear una consulta u ocurrió un error en la base de datos.
   */
  void createConsulta(final ConsultaPojo consultaPojo) throws DAOException;

  /**
   * Actualiza una consulta.
   *
   * @param consultaPojo Consulta a actualizar.
   * @throws DAOException Si no se pudo actualizar una consulta u ocurrió un error en la base de
   *                      datos.
   */
  void updateConsulta(final ConsultaPojo consultaPojo) throws DAOException;

  /**
   * Elimina una consulta.
   *
   * @param consultaPojo Consulta a eliminar.
   * @throws DAOException Si no se pudo eliminar una consulta u ocurrió un error en la base de
   *                      datos.
   */
  void deleteConsulta(final ConsultaPojo consultaPojo) throws DAOException;

  /**
   * Obtiene todas las consultas.
   *
   * @return Todas las consultas.
   * @throws DAOException Si no se pudo obtener las consultas u ocurrió un error en la base de
   *                      datos.
   */
  Map<Integer, ConsultaPojo> getAllConsultas() throws DAOException;

  /**
   * Obtiene una consulta.
   *
   * @param consultaId Id de la consulta.
   * @return Consulta.
   */
  ConsultaPojo getConsulta(final int consultaId);

  /**
   * Obtiene los labels de la tabla de consultas.
   *
   * @return Label de la tabla de consultas.
   */
  List<String> getConsultaNombreColumna();
}
