package model.constants.table;

public enum PacienteTableConstants {
  USUARIO_ID(1),
  TIPO_DOCUMENTO(2),
  NUMERO_DOCUMENTO(3),
  NOMBRE(4),
  APELLIDO(5),
  FECHA_NACIMIENTO(6);

  private final Integer position;

  PacienteTableConstants(final Integer position) {
    this.position = position;
  }

  public Integer getPosition() {
    return this.position;
  }
}
