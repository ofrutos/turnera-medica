package controller.usuario.medico;

import controller.GeneralBarraController;
import controller.InitializableController;
import view.usuario.medico.BarraMedicoView;

/**
 * Clase que administra el controlador de la barra de menú de la ventana principal de medicos.
 */
public class BarraMedicoController extends GeneralBarraController implements
    InitializableController {

  private final BarraMedicoView barraMedicoView;
  private final CargaMedicoController cargaMedicoController;

  /**
   * Crea el controlador de la barra de menu de la ventana principal de medicos.
   *
   * @param barraMedicoView       Vista general de los medicos.
   * @param cargaMedicoController Controlador de la carga de medicos.
   */
  public BarraMedicoController(final BarraMedicoView barraMedicoView,
      final CargaMedicoController cargaMedicoController) {
    super(barraMedicoView);
    this.barraMedicoView = barraMedicoView;
    this.cargaMedicoController = cargaMedicoController;
  }

  /**
   * Inicializa los controladores.
   */
  public void initController() {
    super.initController();
    this.barraMedicoView.getMedicoLbl().addActionListener(e -> this.crearMedico());
  }

  /**
   * Carga el formulario para crear un nuevo médico.
   */
  private void crearMedico() {
    this.cargaMedicoController.setPacientePojo(null);
    this.cargaMedicoController.initView();
  }
}
