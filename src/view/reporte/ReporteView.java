package view.reporte;

import javax.swing.JTable;
import view.GeneralView;
import view.constants.ViewLabels;

/**
 * Clase de la vista general de reportes.
 */
public class ReporteView extends GeneralView {

  private final BarraReporteView barraReporteView;

  /**
   * Crea la vista general de reportes.
   *
   * @param barraReporteView Barra de la vista de reportes.
   */
  public ReporteView(final BarraReporteView barraReporteView) {
    super(barraReporteView);

    this.barraReporteView = barraReporteView;
    this.barraReporteView.initView();

    this.jFrame.setTitle(ViewLabels.TITULO_REPORTE.getLabel());
  }

  /**
   * Devuelve la barra de la vista de reportes.
   *
   * @return Barra de la vista de reportes.
   */
  public BarraReporteView getBarraReporteView() {
    return this.barraReporteView;
  }

  /**
   * Devuelve la tabla de reportes.
   *
   * @return Tabla de reportes.
   */
  public JTable getTablaReporte() {
    return super.getjTable();
  }
}
