package model.pojo;

import java.util.Objects;

public class ConsultorioPojo {

  private Integer consultorioId;
  private String nombre;

  public ConsultorioPojo() {

  }

  public ConsultorioPojo(final Integer consultorioId, final String nombre) {
    this.consultorioId = consultorioId;
    this.nombre = nombre;
  }

  public Integer getConsultorioId() {
    return consultorioId;
  }

  public void setConsultorioId(Integer consultorioId) {
    this.consultorioId = consultorioId;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ConsultorioPojo that = (ConsultorioPojo) o;
    return Objects.equals(consultorioId, that.consultorioId) &&
        Objects.equals(nombre, that.nombre);
  }

  @Override
  public int hashCode() {
    return Objects.hash(consultorioId, nombre);
  }

  @Override
  public String toString() {
    return this.consultorioId + " - " + this.nombre;
  }
}
