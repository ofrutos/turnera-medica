package model.dao.implementation.sqlite;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;
import static util.jdbc.DBManagerMock.borrarPacientes;
import static util.jdbc.DBManagerMock.crearPacientes;
import static util.pojo.PacientePojoMock.getPacienteDos;
import static util.pojo.PacientePojoMock.getPacienteTres;
import static util.pojo.PacientePojoMock.getPacienteUno;

import java.sql.Connection;
import java.text.ParseException;
import model.constants.usuario.TipoDocumento;
import model.dao.exception.DAOException;
import model.jdbc.DBManager;
import model.pojo.PacientePojo;
import model.pojo.TipoUsuario;
import model.util.DateUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import util.jdbc.DBManagerMock;

@RunWith(PowerMockRunner.class)
@PrepareForTest(DBManager.class)
public class PacienteDaoImplementationTest {

  private PacienteDaoImplementation pacienteDaoImplementation;

  @BeforeClass
  public static void preTests() {
    spy(DBManager.class);
    when(DBManager.connect())
        .thenAnswer((Answer<Connection>) invocationOnMock -> DBManagerMock.connect());

    crearPacientes();
  }

  @Before
  public void setup() {
    this.pacienteDaoImplementation = new PacienteDaoImplementation();
  }

  @AfterClass
  public static void postTests() {
    borrarPacientes();
  }

  @Test
  public void consultarPaciente_Ok() throws DAOException, ParseException {
    final PacientePojo expectedPacientePojo = getPacienteUno();
    final PacientePojo actualPacientePojo = this.pacienteDaoImplementation.getPaciente(9999);

    assertEquals(expectedPacientePojo, actualPacientePojo);
  }

  @Test
  public void consultarPaciente_Error() throws DAOException {
    final PacientePojo actualPacientePojo = this.pacienteDaoImplementation.getPaciente(9000);
    assertNull(actualPacientePojo);
  }

  @Test
  public void crearPaciente_Ok() throws DAOException, ParseException {
    PacientePojo expectedPacientePojo = new PacientePojo();
    expectedPacientePojo.setTipoDocumento(TipoDocumento.DNI);
    expectedPacientePojo.setNumeroDocumento("23451240");
    expectedPacientePojo.setNombre("Juan");
    expectedPacientePojo.setApellido("Perez");
    expectedPacientePojo.setFechaNacimiento(DateUtils.getDateFormat().parse("01/01/1990"));
    expectedPacientePojo.setTipoUsuario(TipoUsuario.PACIENTE);

    this.pacienteDaoImplementation.createPaciente(expectedPacientePojo);
    PacientePojo actualPaciente = this.pacienteDaoImplementation
        .getPaciente(expectedPacientePojo.getUsuarioId());
    assertEquals(expectedPacientePojo, actualPaciente);
  }

  @Test
  public void actualizarPaciente_Ok() throws DAOException, ParseException {
    final PacientePojo expectedPacientePojo = getPacienteDos();
    expectedPacientePojo.setApellido("Molinares");

    final PacientePojo currentPacientePojo = this.pacienteDaoImplementation.getPaciente(9997);
    currentPacientePojo.setApellido("Molinares");

    this.pacienteDaoImplementation.updatePaciente(currentPacientePojo);
    final PacientePojo actualPacientePojo = this.pacienteDaoImplementation
        .getPaciente(expectedPacientePojo.getUsuarioId());

    assertEquals(expectedPacientePojo, actualPacientePojo);
  }

  @Test(expected = DAOException.class)
  public void actualizarPaciente_Error() throws DAOException, ParseException {
    final PacientePojo expectedPacientePojo = new PacientePojo();
    expectedPacientePojo.setUsuarioId(1001);
    expectedPacientePojo.setTipoDocumento(TipoDocumento.DNI);
    expectedPacientePojo.setNumeroDocumento("26145124");
    expectedPacientePojo.setNombre("Romina");
    expectedPacientePojo.setApellido("Molina");
    expectedPacientePojo.setFechaNacimiento(DateUtils.getDateFormat().parse("10/10/1985"));
    expectedPacientePojo.setTipoUsuario(TipoUsuario.PACIENTE);

    this.pacienteDaoImplementation.updatePaciente(expectedPacientePojo);
  }

  @Test
  public void eliminarPaciente_Ok() throws DAOException, ParseException {
    final PacientePojo expectedPacientePojo = getPacienteTres();

    this.pacienteDaoImplementation.deletePaciente(expectedPacientePojo);
    final PacientePojo actualPacientePojo = this.pacienteDaoImplementation.getPaciente(9998);

    assertNull(actualPacientePojo);
  }

  @Test(expected = DAOException.class)
  public void eliminarPaciente_Error() throws DAOException, ParseException {
    final PacientePojo expectedPacientePojo = new PacientePojo();
    expectedPacientePojo.setUsuarioId(1001);
    expectedPacientePojo.setTipoDocumento(TipoDocumento.DNI);
    expectedPacientePojo.setNumeroDocumento("26145124");
    expectedPacientePojo.setNombre("Romina");
    expectedPacientePojo.setApellido("Molina");
    expectedPacientePojo.setFechaNacimiento(DateUtils.getDateFormat().parse("10/10/1985"));
    expectedPacientePojo.setTipoUsuario(TipoUsuario.PACIENTE);

    this.pacienteDaoImplementation.deletePaciente(expectedPacientePojo);
  }
}

