package service;

import java.util.Date;
import java.util.List;
import model.pojo.ConsultaPojo;
import model.pojo.MedicoConsultorioPojo;
import model.pojo.MedicoPojo;
import model.pojo.PacientePojo;
import service.excpetion.ServiceException;

/**
 * Interfaz de los servicios de Consultas.
 */
public interface ConsultaService {

  /**
   * Crea una consulta.
   *
   * @param consultaPojo           POJO de la consulta.
   * @param medicoConsultorioPojos POJOs de los consultorios de un medico.
   * @throws ServiceException Si ocurrió un error al crear la consulta o un error de la base de
   *                          datos.
   */
  void crearConsulta(final ConsultaPojo consultaPojo,
      List<MedicoConsultorioPojo> medicoConsultorioPojos) throws ServiceException;

  /**
   * Obtiene todas las consultas.
   *
   * @return Consultas del paciente.
   * @throws ServiceException Si ocurrió un error al obtener las consultas o un error de la base de
   *                          datos.
   */
  List<ConsultaPojo> obtenerConsultas() throws ServiceException;

  /**
   * Obtiene todas las consultas de un paciente.
   *
   * @param pacientePojo POJO del paciente.
   * @return Consultas del paciente.
   * @throws ServiceException Si ocurrió un error al obtener las consultas o un error de la base de
   *                          datos.
   */
  List<ConsultaPojo> obtenerConsultas(final PacientePojo pacientePojo) throws ServiceException;

  /**
   * Obtiene todas las consultas de un medico.
   *
   * @param medicoPojo POJO del medico.
   * @return Consultas del medico.
   * @throws ServiceException Si ocurrió un error al obtener las consultas o un error de la base de
   *                          datos.
   */
  List<ConsultaPojo> obtenerConsultas(final MedicoPojo medicoPojo) throws ServiceException;

  /**
   * Obtiene el nombre de las columnas de la tabla de consultas.
   *
   * @return Nombre de las columnas.
   */
  List<String> obtenerNombreColumnasConsulta();

  /**
   * Obtiene el nombre de las columnas para el reporte.
   *
   * @return Nombre de las columnas.
   */
  List<String> obtenerNombreColumnasReporte();

  /**
   * Obtiene reportes de los medicos.
   *
   * @param fechaDesde Fecha desde.
   * @param fechaHasta Fecha hasta.
   * @return Reporte de los medicos.
   * @throws ServiceException Si ocurrió un error al obtener el reporte o un error de la base de
   *                          datos.
   */
  List<MedicoPojo> obtenerReportes(final Date fechaDesde,
      final Date fechaHasta) throws ServiceException;
}
