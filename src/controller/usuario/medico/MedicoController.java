package controller.usuario.medico;

import controller.InitializableController;
import controller.ViewableController;
import controller.consulta.ConsultaController;
import controller.medicoconsultorio.MedicoConsultorioController;
import service.MedicoService;
import view.message.MessageView;
import view.usuario.medico.CargaMedicoView;
import view.usuario.medico.MedicoView;

/**
 * Clase que administra los controladores de la ventana principal de Medicos.
 */
public class MedicoController implements InitializableController, ViewableController {

  private final MedicoView medicoView;
  private final MedicoTableController medicoTableController;
  private final BarraMedicoController barraMedicoController;
  private final CargaMedicoController cargaMedicoController;

  /**
   * Crea un controlador general de medicos.
   *
   * @param medicoView                  Vista general de los medicos.
   * @param messageView                 Vista de mensajes.
   * @param medicoService               Servicio de los Medicos.
   * @param medicoConsultorioController Controlador de la carga de Consultorios de Medicos.
   * @param consultaController          Controlador de Consultas.
   */
  public MedicoController(final MedicoView medicoView,
      final MessageView messageView,
      final MedicoService medicoService,
      final MedicoConsultorioController medicoConsultorioController,
      final ConsultaController consultaController) {
    this.medicoView = medicoView;
    this.cargaMedicoController = new CargaMedicoController(new CargaMedicoView(),
        messageView,
        medicoService);
    this.barraMedicoController = new BarraMedicoController(this.medicoView.getBarraMedicoView(),
        this.cargaMedicoController);
    this.medicoTableController = new MedicoTableController(this.medicoView.getTablaMedico(),
        medicoService,
        this.cargaMedicoController,
        medicoConsultorioController,
        consultaController,
        messageView);
  }

  public void initController() {
    this.barraMedicoController.initController();
    this.medicoTableController.initController();
    this.cargaMedicoController.initController();
  }

  public void initView() {
    this.medicoView.getjFrame().setVisible(true);
  }
}
