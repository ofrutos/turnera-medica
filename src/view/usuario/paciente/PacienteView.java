package view.usuario.paciente;

import javax.swing.JTable;
import view.constants.ViewLabels;
import view.usuario.UsuarioView;

/**
 * Clase de la vista principal de pacientes.
 */
public class PacienteView extends UsuarioView {

  /**
   *
   */
  private static final long serialVersionUID = 1L;
  private final BarraPacienteView barraPacienteView;

  /**
   * Crea la vista principal de pacientes.
   *
   * @param barraPacienteView Barra de la vista de pacientes.
   */
  public PacienteView(final BarraPacienteView barraPacienteView) {
    super(barraPacienteView);

    this.barraPacienteView = barraPacienteView;
    this.barraPacienteView.initView();

    this.jFrame.setTitle(ViewLabels.TITULO_PACIENTES.getLabel());
  }

  /**
   * Devuelve la barra de la vista de pacientes.
   *
   * @return Barra de la vista de pacientes.
   */
  public BarraPacienteView getBarraPacienteView() {
    return this.barraPacienteView;
  }

  /**
   * Devuelve la tabla de pacientes.
   *
   * @return Tabla de pacientes.
   */
  public JTable getTablaPaciente() {
    return super.getjTable();
  }
}
