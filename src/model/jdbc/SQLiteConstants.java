package model.jdbc;

/**
 * Constantes para la conexión con SQLite. <br>
 */
public enum SQLiteConstants {
  /**
   * Driver de la base de datos. <br>
   */
  DB_DRIVER("jdbc:sqlite"),
  /**
   * Path de la base de datos. <br>
   */
  DB_PATH("turnera_medica.db"),
  /**
   * Path de la base de datos de prueba. <br>
   */
  DB_PATH_TEST("turnera_medica_test.db");

  private final String constant;

  SQLiteConstants(String constant) {
    this.constant = constant;
  }

  public String getConstant() {
    return this.constant;
  }
}
